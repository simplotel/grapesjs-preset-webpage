export const initiateCKeditor = function initiateCKeditor(textArea) {
    $(textArea).ckeditor({
        paramInternalLinks: internalLinks,
        customConfig : 'ckeditor_customConfig.js'
    });
    return CKEDITOR.instances[textArea.id];
}

export const textEditor =  (editor, config = {}) => {
    const domc = editor.DomComponents;

    function getDisableAllDataAttrs() {
        return 'data-gjs-copyable="false" data-gjs-draggable="false" data-gjs-droppable="false" data-gjs-editable="false" data-gjs-highlightable="false" data-gjs-hoverable="false" data-gjs-layerable="false" data-gjs-removable="false" data-gjs-resizable="false" data-gjs-selectable="false" data-gjs-stylable="false"';
    }

    function getHtmlElement(name, props, attrs) {
        var innerItem = document.createElement(name);
        for (var key in props) {
            innerItem[key] = props[key];
        }
        for (var key in attrs) {
            innerItem.setAttribute(key, attrs[key]);
        }
        return innerItem;
    }


    return function showTextPopup(modal, model, title) {
        var content = '<div style="text-align: center;padding: 20px;min-height: 180px;"><i class="fa fa-refresh fa-spin fa-3x fa-fw" aria-hidden="true"></i></div>';
        const pfx = editor.getConfig('stylePrefix');
        const currElem = editor.getSelected().getEl(); 
        var textArea = '';

        /*function initiateCKeditor() {
            $(textArea).ckeditor({
                paramInternalLinks: internalLinks,
                customConfig : 'ckeditor_customConfig.js'
            });
            var ckeditorInstance = CKEDITOR.instances[textArea.id];
            ckeditorInstance.setData(editor.getSelected().attributes.type === "widget-variable-imagetext-slider" ? editor.getSelected().getAttributes()['data-heading_text'] : currElem.innerHTML);
        }*/

        function handleSave() {
            var ckeditorInstance = CKEDITOR.instances[textArea.id];
            var data = ckeditorInstance.getData();
            if(model === 'variable-head') {
                let attr = editor.getSelected().getAttributes();
                attr['data-heading_text'] = data;
                editor.getSelected().setAttributes(attr);
            } else {
                /*
                    Passing { fromDisable : true} as options args to avoid breaking of GJS ComponentTextView.js
                    TODO: Update GJS library to avoid this bug
                */
                model.set("content", data, { fromDisable : true});
            }
            modal.close();
            editor.getSelected().view.render();
            ckeditorInstance.destroy(true);
        }

        modal.onceOpen(function () {
            var modalHtml = getHtmlElement('table', {}, { style: 'width: 100%;' });
            var parentItem = '', childItem = '';
            var attrs = {
                'id': currElem.id + '__text',
                'name': 'ckeditor_textarea'
            };

            parentItem = getHtmlElement('td', {}, { style: 'padding-top: 10px;' });
            childItem = parentItem;
            parentItem = getHtmlElement('tr', {}, {});
            
            textArea = getHtmlElement('textarea', {}, attrs);
            childItem.appendChild(textArea);

            childItem.appendChild(getHtmlElement('button', { 'innerHTML': 'Ok', 'className': `${pfx}btn-prim`, 'onclick': handleSave }, { style: 'float: right;' }));
            parentItem.appendChild(childItem);
            modalHtml.appendChild(parentItem);
            modal.setContent(modalHtml);
            
            const ckeditorInstance = initiateCKeditor(textArea);
            ckeditorInstance.setData(editor.getSelected().attributes.type === "widget-variable-imagetext-slider" ? editor.getSelected().getAttributes()['data-heading_text'] : currElem.innerHTML);
        });
        modal.open({ title, content }).onceClose(function () {
            console.log('Text editor modal closed');
        });
    }
}