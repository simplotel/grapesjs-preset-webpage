import summaryWidget from "./GroupWidgets";
import widgetVariant from "./WidgetVariants";
import { textEditor } from "./TextEditor";
import cmdItems from "./../commands";
import { cmdEditSortDelete, allSliders, cmdOpenTextGroupConfig } from "./../consts.js";

/**
 * @param {String} - icon
 * @param {String} - cmd
 * Use this function with call, apply or bind to add toolbar item
 */

function addToolbarItem(icon, title, cmd) {
    if (cmd) {
        const toolbar = this.get("toolbar");
        const toolbarExists = toolbar.some(
            (item) => item.command === cmd
        );
        if (!toolbarExists) {
            toolbar.push({
                attributes: {
                    class: icon,
                    title: title
                },
                command: cmd
            });
            this.set("toolbar", toolbar);
            this.emitUpdate("toolbar");
        }
    }
}

/**
 * @param {String} - cmd
 * Use this function with call, apply or bind to add toolbar item
 */

function removeToolbarItem(cmd) {
    if (cmd) {
        const toolbar = this.get("toolbar");
        const newToolbar = (toolbar ? toolbar : []).filter(
            (item) => item.command !== cmd
        );
        if (newToolbar.length) {
            this.set("toolbar", newToolbar);
            this.emitUpdate("toolbar");
        }
    }
}

/**
 * @param {String} - icon
 * @param {String} - icon
 * @param {Boolean} - flag
 * Add or remove item based on flag
 */

function updateToolbar(icon, title, cmd, flag) {
    if (flag) {
        addToolbarItem.call(this, icon, title, cmd);
    } else {
        removeToolbarItem.call(this, cmd);
    }
}

export default (editor, config = {}) => {
    widgetVariant(editor, config);

    const domc = editor.DomComponents;
    const defaultType = domc.getType("default");
    const tableType = domc.getType("table");
    const defaultModel = defaultType.model;
    const defaultView = defaultType.view;
    const imageType = domc.getType("image");
    const videoType = domc.getType("video");

    const baseInitToolbar = imageType.model.prototype.initToolbar;
    const countdownTimer = domc.getType("countdown");

    // Default traits for widget crop
    const widgetCropTrait = {
        type: "checkbox",
        valueTrue: "true",
        valueFalse: "false",
        name: "data-widget_crop",
        label: "Widget Level Cropping",
    }
    const bookingFormOnWidget = {
        type: "checkbox",
        valueTrue: "bookingFormWidget",
        valueFalse: "",
        name: "data-booking",
        label: "Booking-form",
    }

    // Widget Crop State
    const widgetCropState = {
        "widget-heroimage-slider": true,
        "widget-image-slider": true,
        "widget-imagetext-slider": true,
        "widget-img-txt-link": true
    }

    // Trait option to add link in images
    var hCompDefaultLink = Object.assign({}, imageType.model.prototype.defaults);
    hCompDefaultLink.type = "image";
    hCompDefaultLink.traits.push({
        type: "text",
        placeholder: "http://",
        name: "data-href",
        label: "HREF VALUE",
    });
    domc.addType("image", {
        model: imageType.model.extend({
            defaults: hCompDefaultLink,
        }),
        view: imageType.view.extend({
            updateVals() {
                var attrs = this.model.getAttributes();
                attrs["data-href"] = this.model.attributes["data-href"];
                this.model.setAttributes(attrs);
                this.updateScript();
            },

            init() {
                this.listenTo(this.model, "change:data-href", this.updateVals);
                const attrs = this.model.getAttributes();
                if (attrs["data-href"]) {
                    this.model.set("data-href", attrs["data-href"]);
                }
            },
        }),
    });

    // Trait option for full width
    var hCompDefaultWidth = Object.assign({},
        defaultType.model.prototype.defaults
    );
    hCompDefaultWidth.traits.push({
        type: "checkbox",
        valueTrue: "fullWidthWidget",
        valueFalse: "",
        name: "data-style",
        label: "Full-Width",
    });
    domc.addType("default", {
        model: defaultType.model.extend({
            defaults: hCompDefaultWidth,
        }),
        view: defaultType.view.extend({
            updateVals() {
                var attrs = this.model.getAttributes();
                attrs["data-style"] = this.model.attributes["data-style"];
                this.model.setAttributes(attrs);
                this.updateScript();
            },

            init() {
                this.listenTo(this.model, "change:data-style", this.updateVals);
                const attrs = this.model.getAttributes();
                if (attrs["data-style"]) {
                    this.model.set("data-style", attrs["data-style"]);
                }
            },
        }),
    });


    // countdown timer component code fix to load previously saved settings
    domc.addType("countdown", {
        model: countdownTimer.model,
        view: countdownTimer.view.extend({
            updateVals() {
                var attrs = this.model.getAttributes();
                attrs["data-startfrom"] = this.model.attributes.startfrom;
                attrs["data-endtext"] = this.model.attributes.endText;
                this.model.setAttributes(attrs);
                this.updateScript();
            },
            init() {
                this.listenTo(
                    this.model,
                    "change:startfrom change:endText",
                    this.updateVals
                );
                const comps = this.model.get("components");
                const c = config.countdownOpts;
                const pfx = c.countdownClsPfx;

                // Add a basic countdown template if it's not yet initialized
                if (!comps.length) {
                    comps.reset();
                    comps.add(`
            <span data-js="countdown" class="${pfx}-cont">
              <div class="${pfx}-block">
                <div data-js="countdown-day" class="${pfx}-digit"></div>
                <div class="${pfx}-label">${c.labelDays}</div>
              </div>
              <div class="${pfx}-block">
                <div data-js="countdown-hour" class="${pfx}-digit"></div>
                <div class="${pfx}-label">${c.labelHours}</div>
              </div>
              <div class="${pfx}-block">
                <div data-js="countdown-minute" class="${pfx}-digit"></div>
                <div class="${pfx}-label">${c.labelMinutes}</div>
              </div>
              <div class="${pfx}-block">
                <div data-js="countdown-second" class="${pfx}-digit"></div>
                <div class="${pfx}-label">${c.labelSeconds}</div>
              </div>
            </span>
            <span data-js="countdown-endtext" class="${pfx}-endtext"></span>
          `);
                }
                const attrs = this.model.getAttributes();

                if (attrs["data-startfrom"]) {
                    this.model.set("startfrom", attrs["data-startfrom"]);
                }

                if (attrs["data-endtext"]) {
                    this.model.set("endText", attrs["data-endtext"]);
                }
            },
        }),
    });

    /**
     * @param {Function} - fn
     * @return {Boolean} - Based on callback function
     * Optimized iterator which returns a boolean value on success
     */

    Array.prototype.iterateWithFlag = function(fn) {
        for (let i = 0; i < this.length; i++) {
            let res = fn.call(this, this[i], i);
            if (res === true) {
                return true;
            }
        }

        return false;
    };

    /**
     * @param {Function} - updateWidgetCrop
     * Function will add event listeners for widget crop on initiation of widget type.
     */

    function widgetCropInit(updateWidgetCropParam) {
        this.listenTo(this.model, "change:data-widget_crop", this.updateVals);
        this.listenTo(this.model, "change:attributes:data-widget_crop", this.updateWidgetCrop);
        const attrs = this.model.getAttributes();
        updateWidgetCropParam(attrs["data-widget_crop"]);
        if (attrs["data-widget_crop"]) {
            this.model.set("data-widget_crop", attrs["data-widget_crop"]);
        }
    }

    /**
     * @param {Function} - updateWidgetCrop
     * Function will listen to change event for widget crop.
     */

    function widgetCropUpdateVals(updateWidgetCrop) {
        const attrs = this.model.getAttributes();
        attrs["data-widget_crop"] = this.model.attributes["data-widget_crop"];
        updateWidgetCrop(attrs["data-widget_crop"]);
        this.model.setAttributes(attrs);
    }

    /**
     * @param {Function} - updateWidgetCrop
     * Function will listen to change event for widget crop and it will also display the changes.
     */
    function widgetCropUpdateToolbar(updateWidgetCrop) {
        widgetCropInitToolbar.call(this, updateWidgetCrop, true);
    }

    /**
     * @param {Function} - updateWidgetCrop
     * @param {Object} - ref: It will be the model or component
     * Function will update toolbar on initiation of the widget type.
     */

    function widgetCropInitToolbar(updateWidgetCrop, ref) {
        const _this = ref ? this.model : this;
        const attrs = _this.getAttributes();
        let isWidgetCrop = updateWidgetCrop(attrs["data-widget_crop"]);
        updateToolbar.call(_this, "fa fa-pencil", "Crop", config.imageEditCommandId, isWidgetCrop);
    }

    /**
     * @param {Object} - ele
     * @param {String} - widgetName
     * This function will check for the name of compoenent's parent
     */

    function checkParent(ele, widgetName) {
        if (!ele) {
            return false
        } else if (ele.getName().toLowerCase() === widgetName.toLowerCase()) {
            return true
        }
        return checkParent(ele.parent(), widgetName);
    }

    /**
     * @param {String} - widgetName
     * @return {Function} - returns updated value for crop state.
     * Function will update the widget's crop state.
     */

    function updateWidgetCropVals(widgetName) {
        return (flag) => {
            widgetCropState[widgetName] = flag === "true" ? true : false;
            return widgetCropState[widgetName];
        }
    }

    /**
     * @param {String} - widgetName
     * @return {Object} - returns the object of necessary functions for widget crop.
     */

    function getWidgetCropFunctions(widgetName) {
        return {
            updateVals() {
                widgetCropUpdateVals.call(this, updateWidgetCropVals(widgetName));
            },

            updateWidgetCrop() {
                setTimeout(() => {
                    widgetCropUpdateToolbar.call(this, updateWidgetCropVals(widgetName));
                }, 0);
            },
        }
    }

    /**
     * @param {String} - widgetName
     */
    function listenToRender(widgetName) {

        function updateToolbarOfChildImages(comp, flag) {
            if (comp.getName().toLowerCase() === widgetName.toLowerCase()) {
                const allImages = comp.find('img');
                allImages.forEach(image => {
                    updateToolbar.call(image, "fa fa-pencil", "Crop", config.imageEditCommandId, flag);
                });
            }
        }

        editor.on('component:mount', (comp) => {
            updateToolbarOfChildImages(comp, !widgetCropState[widgetName.toLowerCase()])
        });

        editor.on("component:add", comp => {
            if (comp.getName() === "Image" && checkParent(comp, widgetName)) {
                updateToolbar.call(comp, "fa fa-pencil", "Crop", config.imageEditCommandId, !widgetCropState[widgetName.toLowerCase()]);
            }
        });

        editor.on("component:update:attributes", comp => {
            updateToolbarOfChildImages(comp, comp.getAttributes()["data-widget_crop"] === "false");
        })
    }


    // simplotel CMS customizations
    if (config.showAssetsMethod) {
        function getSIUPopupOnSelectCallback(model, widgetType, wrapperClass) {
            /**
             * @param {Node} - ele
             * @param {Object} - attrs
             * @return {Node} - modified DOM element
             * Function is adding or updating attributes on the DOM element
             */

            function updateAttributes(ele, attrs) {
                if (!ele) {
                    return;
                }
                for (let atr of Object.keys(attrs)) {
                    ele.setAttribute(atr, attrs[atr] && attrs[atr] !== 'undefined' ? attrs[atr] : "");
                }

                return ele;
            }

            /**
             * @param {Object} - newAttrs
             * @param {String} - asset
             * @param {String} - mediaType
             * @param {Object} - extraAttrs
             * @return {Object} - new attributes with new configuration.
             */

            function updateOldMediaAttributes(newAttrs, asset, mediaType, extraAttrs) {
                let attrObj = {
                    ...newAttrs,
                    "class": extraAttrs.class,
                    "src": asset,
                    "data-category": mediaType,
                    "data-gjs-type": mediaType,
                    "data-simp-content": mediaType,
                };

                if (mediaType === 'video') {
                    attrObj = {
                        ...attrObj,
                        "data-autoplay": "no",
                        "data-controls": "yes",
                        "data-muted": "yes",
                        "poster": newAttrs["data-poster"],
                        "controls": true,
                        "muted": true
                    }
                }

                return attrObj;
            }

            /**
             * @param {Node} - node
             * @param {Array} - attrs
             * Function is removing unwanted attributes from DOM element.
             */

            function removeUnwantedAttributes(node, attrs = []) {
                attrs.forEach(function(attr) {
                    let allEle = node.querySelectorAll("*");
                    allEle.forEach(function(ele) {
                        ele.removeAttribute(attr);
                    });
                });
            }

            /**
             * @param {Node} - node
             * Function is removing unwanted functionality from node.
             */

            function addExtraActionAttributes(node) {
                if(config.showOlderPhoenix) return;
                let allEle = node.querySelectorAll("*");
                allEle.forEach(function(ele) {
                    ele.setAttribute("data-gjs-copyable", false);
                    ele.setAttribute("data-gjs-draggable", false);
                    ele.setAttribute("data-gjs-removable", false);
                    ele.setAttribute("data-gjs-resizable", false);
                    ele.setAttribute("data-gjs-droppable", false);
                });
                node.setAttribute("data-gjs-copyable", false);
                node.setAttribute("data-gjs-draggable", false);
                node.setAttribute("data-gjs-removable", false);
                node.setAttribute("data-gjs-resizable", false);
                node.setAttribute("data-gjs-droppable", false);
            }

            /**
             * @param {Node} - ele
             * @return {Object} - all the attributes which are present on ele
             */

            function getAllAttributes(ele) {
                let attrs = {};
                Array.from(ele.attributes).forEach(attr => {
                    attrs[attr.name] = attr.value;
                });
                return attrs;
            }

            /**
             * @param {Object} - attrs
             * @return {Node} - mediaTag ex. <img />, <video></video>
             * Function will create media tag based on attributes mainly "data-category".
             */

            function createNewMediaTag(attrs) {
                let mediaTag = document.createElement(attrs["data-category"] === "video" ? "video" : "img");
                updateAttributes(mediaTag, attrs);
                return mediaTag;
            }

            /**
             * @param {Node} - dummyEle
             * @param {Object} - attrs
             * @return {Node} - media block
             * Function will create media block for sliders
             */

            function createSliderMediaBlock(dummyEle, attrs) {
                let newMediaEle = createNewMediaTag(attrs);
                let img = dummyEle.querySelector('img');
                let video = dummyEle.querySelector('video');

                if (img) {
                    if (img.parentElement.nodeName === 'A') {
                        img.parentElement.parentElement.replaceChild(newMediaEle, img.parentElement);
                    } else {
                        img.parentElement.replaceChild(newMediaEle, img);
                    }
                } else if (video) {
                    video.parentElement.replaceChild(newMediaEle, video);
                }

                let textWrapper = dummyEle.querySelector('.text-content-wrapper');
                if (textWrapper) {
                    textWrapper.querySelector('p').innerHTML = `Lorem ipsum dolor sit amet, consectetur adipiscing elit`;
                }
                return dummyEle;
            }

            /**
             * @param {Node} - textContent
             * @param {Object} - args
             * @param {Array} - asset
             * @return {Node} - Mapped HTML Code
             * Function will map all the attributes to media blocks and will return a dummy element with all HTML code.
             */
            function createAllMediaBlocks(textContent, args, asset) {
                const _helperEle = document.createElement("div");
                args.forEach(function(mediaAttrs, idx) {
                    const mediaType = mediaAttrs["data-type"];
                    const clonedTextContent = textContent.cloneNode(true);

                    const extraAttrs = getAllAttributes(getAvailableMediaTag(clonedTextContent));
                    const newAttrs = updateOldMediaAttributes(mediaAttrs, asset[idx], mediaType, extraAttrs);

                    const newTextContent = createSliderMediaBlock(clonedTextContent, newAttrs);
                    removeUnwantedAttributes(newTextContent, ["id", "autoplay"]);

                    //updateClonedDataWithNew(mediaType, clonedTextContent, mediaAttrs, true);
                    _helperEle.appendChild(newTextContent);
                });
                addExtraActionAttributes(_helperEle);

                return _helperEle;
            }

            /**
             * @param {Node} - textContent
             * @return {Node || null} - media tag
             * Function will return available media tag from media block.
             */

            function getAvailableMediaTag(textContent) {
                const img = textContent.querySelector('img');
                const video = textContent.querySelector('video');

                if (img) {
                    return img;
                } else if (video) {
                    return video;
                }

                return null;
            }

            /**
             * @param {String} - wrapperClass
             * @param {Object} - WIDGET_DATA
             * @param {Node} - selectedEle
             * @return {Node} - selected media block
             * Function will selected media block, later which can be used to create more blocks.
             */
            function getTextContentBlock(wrapperClass, WIDGET_DATA, selectedEle) {
                let textContent = null;
                const isValid = WIDGET_DATA.allWidgets.iterateWithFlag(function(data) {
                    if (wrapperClass === data.widget && data.selector) {
                        textContent = selectedEle.querySelector(data.selector);
                        return true;
                    }
                });

                if (!isValid) {
                    textContent = selectedEle.querySelector(WIDGET_DATA.default.selector);
                }

                return textContent;
            }

            return function(asset, args, altAsset, altArgs) {
                if (widgetType === "poster") {
                    return asset;
                }

                const selComp = editor.getSelected();
                const selectedEle = selComp.getEl();

                let isMultiple = Array.isArray(args);
                const mediaType = !isMultiple ? args["data-type"] : "";
                const closestWidget = selComp.closest("[data-simp-type]");

                let isClosestWidgetSlider = false;
                if (allSliders.find((widgetName) => closestWidget.getName().toLowerCase() === widgetName)) {
                    isClosestWidgetSlider = true;
                }

                const allAvailableImages = selComp.getName().toLowerCase() === "video" && isClosestWidgetSlider ? closestWidget.find("img") : selComp.find("img");
                let attrsOfFirstImage = allAvailableImages.length ? allAvailableImages[0].getAttributes() : (isMultiple ? args[0] : args);

                // Using this dummy state to call cropping functions
                const dumbState = {
                    data: {
                        selectedEle: {},
                    },
                    state: {},
                    PRECISION_VALUE: 3,
                };

                /**
                 * @param {Number} - ratio
                 * @param {Object} - attrs
                 * @return {Object} - cropped values
                 * Function returns a cropped data the image.
                 */

                function getCropDataByDevice(ratio, attrs) {
                    let newValues = cmdItems.getOnSelectCallback(ratio, () => {}, false)(attrs);
                    newValues = {
                        ...newValues,
                        width: newValues.x2 - newValues.x1,
                        height: newValues.y2 - newValues.y1,
                        ratio: ratio,
                        orgUrl: attrs['data-orgimgsrc'],
                    }
                    const urls = cmdItems.constructNewUrl.call(dumbState, config, null, newValues, true);
                    return {
                        cropValues: newValues,
                        urls: urls,
                    }
                }

                function cropMediaWithGivenData() {
                    return (isMultiple ? args : [args]).map((imgData) => {
                        const mediaType = imgData["data-type"];
                        const desktopRatio = attrsOfFirstImage["data-imgaspect"];
                        const tabletRatio = attrsOfFirstImage["data-tabaspect"];
                        const mobileRatio = attrsOfFirstImage["data-mobaspect"];

                        const desktopCropData = getCropDataByDevice(parseFloat(desktopRatio), imgData);
                        const tabletCropData = getCropDataByDevice(parseFloat(tabletRatio), imgData);
                        const mobileCropData = getCropDataByDevice(parseFloat(mobileRatio), imgData);

                        const newImgData = {
                            ...(selComp.getName() === "Image" ? selComp.getAttributes() : {}),
                            ...imgData,
                            "data-cropimgx": desktopCropData.cropValues.x1,
                            "data-cropimgy": desktopCropData.cropValues.y1,
                            "data-cropimgx2": desktopCropData.cropValues.x2,
                            "data-cropimgy2": desktopCropData.cropValues.y2,
                            "data-imgaspect": desktopRatio,
                            "data-tabaspect": tabletRatio,
                            "data-mobaspect": mobileRatio,
                            "src": mediaType === "video" ? imgData["data-orgimgsrc"] : desktopCropData.urls.desktop,
                            "data-tabimgsrc": mediaType === "video" ? imgData["data-orgimgsrc"] : tabletCropData.urls.tablet,
                            "data-mobimgsrc": mediaType === "video" ? imgData["data-orgimgsrc"] : mobileCropData.urls.mobile,
                        }

                        return newImgData;
                    });
                }

                /**
                 * Modifying args (parameter of the callback) as per cropped aspect ratio of the previous image.
                 */

                if (selComp.getName() === "Image") {
                    let selAttrs = selComp.getAttributes();
                    attrsOfFirstImage = {
                        ...selAttrs,
                        ...attrsOfFirstImage,
                        "data-imgaspect": selAttrs["data-imgaspect"],
                        "data-tabaspect": selAttrs["data-tabaspect"],
                        "data-mobaspect": selAttrs["data-mobaspect"],
                    }
                }

                args = cropMediaWithGivenData();

                if (!isMultiple) {
                    args = args.length ? args[0] : args;

                    let selParent = selComp.parent();

                    args = updateOldMediaAttributes(args, args["src"], mediaType, selComp.getAttributes());

                    const newMediaTag = createNewMediaTag(args);
                    removeUnwantedAttributes(newMediaTag, ["id", "autoplay"]);
                    addExtraActionAttributes(newMediaTag);

                    /** 
                     * Updating new media element in DOM and GrapesJS object
                     * Checking for anchor tag, If image is wrapper inside anchor tag then we need to replace anchor tag with media tag
                     */
                    if (selParent.getName() === 'A') {
                        selParent.parent().components(newMediaTag.outerHTML);
                    } else {
                        selParent.components(newMediaTag.outerHTML);
                    }

                } else if (widgetType === "image-slider") {

                    /**
                     * @readonly {Object} - DEFAUTL_MEDIA_STRING
                     * It has a default name of the media files.
                     */

                    const DEFAULT_MEDIA_STRING = {
                        video: "copy_1.mp4",
                        image: "ph-img.png",
                    };

                    /**
                    * @readonly {Object} - WIDGET_DATA
                    * It has a data of the widgets which needs extra modifications.
                    * innerEle represents a class of inner DOM element 
                     in the selected component to add newly selected data in it.
                     */

                    const WIDGET_DATA = {
                        default: {
                            selector: ".text-content",
                        },
                        allWidgets: [{
                                widget: ".wt-variable-imagetext-slider-container",
                                selector: ".imagetext-container",
                                innerEle: "wt-variable-imagetext-slider",
                            },
                            {
                                widget: ".jssor-carousel-container",
                                innerEle: "sliders-jssor",
                                selector: ".item",
                            },
                            {
                                widget: ".wt-img-txt-link",
                                innerEle: "img-content",
                            }
                        ],
                    };

                    /**
                     * @return {Boolean} - To identify first slider media block
                     */
                    function checkIsFirst(textContent) {
                        const mediaTag = getAvailableMediaTag(textContent)
                        const sourceOfMedia = mediaTag ?
                            mediaTag.getAttribute("src") :
                            "";
                        return sourceOfMedia.includes(DEFAULT_MEDIA_STRING.image) || sourceOfMedia.includes(DEFAULT_MEDIA_STRING.video) || sourceOfMedia === "";
                    }

                    /**
                     * @param {Object} - component is the grapesJs Object
                     * Updating DOM and GrapesJs with new Data;
                     */
                    function updateComponent(component) {
                        const textContent = getTextContentBlock(wrapperClass, WIDGET_DATA, selectedEle);
                        asset = args.map((arg) => {
                            return arg["src"];
                        })
                        const _helperEle = createAllMediaBlocks(textContent, args, asset);

                        if (checkIsFirst(textContent)) {
                            component.components(_helperEle.innerHTML);
                        } else {
                            component.components().add(_helperEle.innerHTML);
                        }
                    }

                    /**
                     * @param {Object} - comObj
                     * @param {String} - selector
                     * @return {Boolean} - based on task
                     */

                    function getAndUpdateChildComponent(comObj, selector) {
                        return comObj.components().models.iterateWithFlag(function(comp) {
                            let attr = comp.getAttributes();
                            if (attr.class === selector || attr.class.includes(selector)) {
                                updateComponent(comp);
                                return true;
                            } else if (comp.components().models.length > 0) {
                                return getAndUpdateChildComponent(comp, selector);
                            }
                        });
                    }

                    /**
                     * @return {Boolean} - based on task
                     * Updating widget which has extra modifications.
                     */

                    function updateModifiedComponent() {
                        return WIDGET_DATA.allWidgets.iterateWithFlag(function(data) {
                            if (!data.innerEle) {
                                return false;
                            }
                            if (wrapperClass === data.widget) {
                                return getAndUpdateChildComponent(selComp, data.innerEle);
                            }
                        });
                    }

                    const isDone = updateModifiedComponent();
                    /**
                     * Updating selected component
                     */
                    if (!isDone) {
                        updateComponent(selComp);
                    }
                    selComp.view.render();
                }
            };
        }

        domc.addType("image", {
            model: imageType.model.extend({
                init() {
                    this.on("change:attributes:data-mediashow", this.handleMediaShowChange);
                    this.removeTrait('alt');
                },
                handleMediaShowChange: function handleMediaChange(component, value) {
                    component.view.$el.parent().data("mediashow", value);
                },
                initToolbar(...args) {
                    baseInitToolbar.apply(this, args);
                    addToolbarItem.call(this, "fa fa-pencil", "Crop", config.imageEditCommandId);
                },
            }),
            view: imageType.view.extend({
                // Bind events
                events: {
                    // If you want to bind the event to children elements
                    // 'click .someChildrenClass': 'methodName',
                    dblclick: "handleClick",
                },
                handleClick: function(e) {
                    e.stopPropagation();
                    var attrs = this.model.getAttributes();
                    var aspectRatio = parseFloat(attrs["data-imgaspect"]) ?
                        parseFloat(attrs["data-imgaspect"]) :
                        1.777;

                    config.showAssetsMethod(
                        this,
                        getSIUPopupOnSelectCallback(this.model),
                        aspectRatio
                    );
                },
            }),
        });

        domc.addType("video", {
            extend: "default",
            model: {
                defaults: {
                    traits: [{
                            label: "Poster",
                            name: "poster",
                            type: "poster"
                        },
                        {
                            label: "Controls",
                            name: "controls",
                            type: "checkbox",
                            changeProp: 1,
                        },
                        {
                            label: "Mute",
                            name: "muted",
                            type: "checkbox",
                            changeProp: 1
                        },
                        {
                            label: "Autoplay",
                            name: "autoplay",
                            type: "checkbox",
                            changeProp: 1,
                        },
                    ],
                    attributes: {
                        poster: "",
                        onloadstart: "",
                    },
                    tagName: "video",
                    controls: true,
                    muted: true,
                    autoplay: true,
                },
                init() {
                    this.on("change:attributes:data-mediashow", this.handleMediaShowChange);
                    this.on("change:controls", this.handleControl);
                    this.on("change:muted", this.handleMuted);
                    this.on("change:autoplay", this.handleAutoplay);
                    //  this.on('change:poster', this.handlePoster);

                    var attrs = this.getAttributes();
                    this.set("controls", attrs["data-controls"] == "yes" ? true : false);
                    this.set("muted", attrs["data-muted"] == "yes" ? true : false);
                    this.set("autoplay", attrs["data-autoplay"] == "yes" ? true : false);
                },
                handleMediaShowChange: function handleMediaChange(component, value) {
                    component.view.$el.parent().data("mediashow", value);
                },
                handleControl: function handleControl(component, value) {
                    var attrs = this.getAttributes();
                    if (attrs["controls"] === value) {
                        return;
                    }
                    attrs["controls"] = value;
                    attrs["data-controls"] = value ? "yes" : "no";
                    this.setAttributes(attrs);
                },
                handleMuted: function handleMuted(component, value) {
                    var attrs = this.getAttributes();
                    if (attrs["muted"] === value) {
                        return;
                    }
                    attrs["muted"] = value;
                    attrs["data-muted"] = value ? "yes" : "no";
                    this.setAttributes(attrs);

                    // Bypass the browser's security restriction on muting the built-in video and audio player.
                    if (component.view) {
                        if (value) {
                            component.view.el.muted = true;
                        } else {
                            component.view.el.muted = false;
                        }
                    }
                },
                handleAutoplay: function handleAutoplay(component, value) {
                    var attrs = this.getAttributes();
                    if (attrs["autoplay"] === value) {
                        return;
                    }
                    attrs["autoplay"] = value;
                    attrs["data-autoplay"] = value ? "yes" : "no";
                    this.setAttributes(attrs);

                    // Bypass the browser's security restriction on autoplaying the built-in video and audio player.
                    if (component.view) {
                        if (value) {
                            component.view.el.autoplay = true;
                        } else {
                            component.view.el.autoplay = false;
                        }
                    }
                },
                handlePoster: function handlePoster(component, value) {},
            },
            view: {
                tagName: "video",
                events: {
                    dblclick: "handleClick",
                },
                handleClick: function(e) {
                    e.stopPropagation();
                    var attrs = this.model.getAttributes();
                    var aspectRatio = parseFloat(attrs["data-imgaspect"]) ?
                        parseFloat(attrs["data-imgaspect"]) :
                        1.777;

                    config.showAssetsMethod(
                        this,
                        getSIUPopupOnSelectCallback(this.model),
                        aspectRatio
                    );
                },
                // Set mute & autoplay from data attributes on render.
                onRender({ el }) {
                    if (el.dataset.muted === 'yes') {
                        el.muted = true;
                    }
                    if (el.dataset.autoplay === 'yes') {
                        el.autoplay = true;
                    }
                }
            },
        });

        const oneImageTypeName = "widget-one-image";
        const oneImage = domc.getType(oneImageTypeName);
        var compDefaults = Object.assign({}, oneImage.model.prototype.defaults);
        compDefaults.type = oneImageTypeName;
        compDefaults.tagName = "div";


        domc.addType(oneImageTypeName, {
            model: oneImage.model.extend({
                defaults: {
                    ...compDefaults,
                    traits: [
                        ...compDefaults.traits,
                        {
                            ...bookingFormOnWidget
                        }
                    ]
                },
            }),
        });

        // to remove a Drag Delete Clone toolbars for the Cell/Child elements of a widget

        function removeDragDeleteCloneToolbar(currentInstance) {
            currentInstance.get("components").models.forEach(ele => {
                updateToolbar.call(ele, "","", "tlb-move", false);
                updateToolbar.call(ele, "","", "tlb-delete", false);
                updateToolbar.call(ele, "","", "tlb-clone", false);
            });
        }

        // generic function for all the 4 container widgets
        
        function containerWidgets(widgetType) {
            const containerWidgetTypeName = widgetType;
            const containerWidget = domc.getType(containerWidgetTypeName);
            var compDefaults = Object.assign({}, containerWidget.model.prototype.defaults);
            compDefaults.type = containerWidgetTypeName;
            compDefaults.tagName = "div";

            const containerWidgetInitToolbar = containerWidget.model.prototype.initToolbar;

            domc.addType(containerWidgetTypeName, {
                model: containerWidget.model.extend({
                    defaults: {
                        ...compDefaults,
                        traits: [
                            ...compDefaults.traits,
                        ]
                    },
                    initToolbar(...args) {
                        containerWidgetInitToolbar.apply(this, args);
                        removeDragDeleteCloneToolbar(this);
                    },
                }),
            });
        }

        containerWidgets("widget-column-1");
        containerWidgets("widget-column-2");
        containerWidgets("widget-column-3");
        containerWidgets("widget-column-3-7");

        const imageSliderTypeName = "widget-image-slider";
        const imageSlider = domc.getType(imageSliderTypeName);
        var compDefaults = Object.assign({}, imageSlider.model.prototype.defaults);
        compDefaults.type = imageSliderTypeName;
        compDefaults.tagName = "div";

        const imageSliderInitToolbar = imageSlider.model.prototype.initToolbar;

        domc.addType(imageSliderTypeName, {
            model: imageSlider.model.extend({
                defaults: {
                    ...compDefaults,
                    traits: [
                        ...compDefaults.traits,
                        {
                            ...bookingFormOnWidget
                        }
                    ]
                },
                initToolbar(...args) {
                    imageSliderInitToolbar.apply(this, args);
                    widgetCropInitToolbar.call(this, updateWidgetCropVals(imageSliderTypeName));
                    updateToolbar.call(this, "fa fa-sliders", "Rearrange/Delete Images or Videos", cmdEditSortDelete, true);
                },
            }),
            view: imageSlider.view.extend({
                // Bind events
                events: {
                    // If you want to bind the event to children elements
                    // 'click .someChildrenClass': 'methodName',
                    dblclick: "handleClick",
                    'click .js-add-slide': "handleClick"
                },
                onRender({ el }) {
                    const buttonWrapper = document.createElement('div');
                    buttonWrapper.setAttribute("class", "add-slide-wrapper");
                    const addSlideButton = document.createElement('button');
                    addSlideButton.setAttribute("class", "js-add-slide");
                    addSlideButton.innerText = "+";
                    buttonWrapper.appendChild(addSlideButton);
                    const jssorSlider = el.querySelector(".sliders-jssor.slider");
                    if(jssorSlider){
                        jssorSlider.appendChild(buttonWrapper);
                    }
                },
                handleClick: function(e) {
                    var attrs = this.model.getAttributes();
                    var aspectRatio = parseFloat(attrs["data-imgaspect"]) ?
                        parseFloat(attrs["data-imgaspect"]) :
                        1.777;
                    if (!$(e.target).is(".item")) {
                        config.showAssetsMethod(
                            this,
                            getSIUPopupOnSelectCallback(
                                this.model,
                                "image-slider",
                                ".jssor-carousel-container"
                            ),
                            aspectRatio,
                            null
                        );
                    }
                },
                init() {
                    widgetCropInit.call(this, updateWidgetCropVals(imageSliderTypeName));
                },
                ...getWidgetCropFunctions(imageSliderTypeName)
            }),
        });

        //listenToRender(imageSliderTypeName);

        const heroImageSliderTypeName = "widget-heroimage-slider";
        const heroImageSlider = domc.getType(heroImageSliderTypeName);
        var compDefaults = Object.assign({},
            heroImageSlider.model.prototype.defaults
        );
        compDefaults.type = heroImageSliderTypeName;
        compDefaults.tagName = "div";

        const heroImageSliderInitToolbar = heroImageSlider.model.prototype.initToolbar;

        domc.addType(heroImageSliderTypeName, {
            model: heroImageSlider.model.extend({
                defaults: {
                    ...compDefaults,
                    traits: [
                        ...compDefaults.traits,
                        {
                            ...bookingFormOnWidget
                        }
                    ]
                },
                initToolbar(...args) {
                    heroImageSliderInitToolbar.apply(this, args);
                    widgetCropInitToolbar.call(this, updateWidgetCropVals(heroImageSliderTypeName));
                    updateToolbar.call(this, "fa fa-sliders", "Rearrange/Delete Images or Videos", cmdEditSortDelete, true);
                },
            }),
            view: heroImageSlider.view.extend({
                // Bind events
                events: {
                    dblclick: "handleClick",
                    'click .js-add-slide': "handleClick"
                },
                onRender({ el }) {
                  const buttonWrapper = document.createElement("div");
                  buttonWrapper.setAttribute("class", "add-slide-wrapper");
                  const addSlideButton = document.createElement("button");
                  addSlideButton.setAttribute("class", "js-add-slide");
                  addSlideButton.innerText = "+";
                  buttonWrapper.appendChild(addSlideButton);
                  const jssorSlider = el.querySelector(".sliders-jssor.slider");
                  if(jssorSlider){
                     jssorSlider.appendChild(buttonWrapper);
                  }
                },
                handleClick: function(e) {
                    var attrs = this.model.getAttributes();
                    var aspectRatio = parseFloat(attrs["data-imgaspect"]) ?
                        parseFloat(attrs["data-imgaspect"]) :
                        1.777;
                    if (!$(e.target).is(".item")) {
                        config.showAssetsMethod(
                            this,
                            getSIUPopupOnSelectCallback(
                                this.model,
                                "image-slider",
                                ".jssor-carousel-container"
                            ),
                            aspectRatio,
                            null
                        );
                    }
                },
                init() {
                    widgetCropInit.call(this, updateWidgetCropVals(heroImageSliderTypeName));
                },
                ...getWidgetCropFunctions(heroImageSliderTypeName)
            }),
        });

        //listenToRender(heroImageSliderTypeName);

        const imageTextSliderTypeName = "widget-imagetext-slider";
        const imageTextSlider = domc.getType(imageTextSliderTypeName);
        var compDefaults = Object.assign({},
            imageTextSlider.model.prototype.defaults
        );
        compDefaults.type = imageTextSliderTypeName;
        compDefaults.tagName = "div";

        const imageTextSliderInitToolbar = imageTextSlider.model.prototype.initToolbar;

        domc.addType(imageTextSliderTypeName, {
            model: imageTextSlider.model.extend({
                defaults: {
                    ...compDefaults,
                    traits: [
                        ...compDefaults.traits,
                        {
                            type: 'select',
                            label: 'Type',
                            name: 'data-variation',
                            options: [
                                { id: 'widget_imagetext_slider', name: 'Slider' },
                                { id: 'widget_card_layout', name: 'Cards' },
                                { id: 'widget_collage_layout', name: 'Collage' }
                            ]
                        },
                        {
                            type: 'select',
                            name: 'data-images_in_row',
                            label: 'Images in row',
                            options: [
                                { id: '1', name: '1' },
                                { id: '2', name: '2' },
                                { id: '3', name: '3' },
                                { id: '4', name: '4' },
                                { id: '5', name: '5' },
                                { id: '6', name: '6' },
                            ]
                        },
                        {
                            type: 'checkbox',
                            valueTrue: 'true',
                            valueFalse: 'false',
                            name: 'data-center_aligned',
                            label: 'Center aligned',
                        },
                        {
                            type: 'checkbox',
                            valueTrue: 'true',
                            valueFalse: 'false',
                            name: 'data-mobile_dots',
                            label: 'Show dots in mobile',
                        },
                        {
                            type: 'checkbox',
                            valueTrue: 'true',
                            valueFalse: 'false',
                            name: 'data-desktop_dots',
                            label: 'Show dots in desktop',
                        },
                    ],
                },

                initToolbar(...args) {
                    imageTextSliderInitToolbar.apply(this, args);
                    widgetCropInitToolbar.call(this, updateWidgetCropVals(imageTextSliderTypeName));
                    updateToolbar.call(this, "fa fa-sliders", "Rearrange/Delete Images or Videos", cmdEditSortDelete, true);
                    if(config.showOlderPhoenix) return;
                    this.get("components").models.forEach(ele => {
                        updateToolbar.call(ele, "","", "tlb-move", false);
                        updateToolbar.call(ele, "","", "tlb-delete", false);
                        updateToolbar.call(ele, "","", "tlb-clone", false);
                    })
                },
            }),
            view: imageTextSlider.view.extend({

                // Bind events
                events: {
                    // If you want to bind the event to children elements
                    // 'click .someChildrenClass': 'methodName',
                    dblclick: "handleClick",
                    'click .js-add-slide': "handleClick"
                },
                onRender({ el }) {
                    const buttonWrapper = document.createElement('div');
                    buttonWrapper.setAttribute("class", "add-slide-wrapper");
                    const addSlideButton = document.createElement('button');
                    addSlideButton.setAttribute("class", "js-add-slide");
                    addSlideButton.innerText = "+";
                    buttonWrapper.appendChild(addSlideButton);
                    el.appendChild(buttonWrapper);
                },
                handleClick: function(e) {
                    var attrs = this.model.getAttributes();
                    var aspectRatio = parseFloat(attrs["data-imgaspect"]) ?
                        parseFloat(attrs["data-imgaspect"]) :
                        1.777;
                    if (!$(e.target).is(".text-content") &&
                        !$(e.target).parents().hasClass("text-content")
                    ) {
                        config.showAssetsMethod(
                            this,
                            getSIUPopupOnSelectCallback(
                                this.model,
                                "image-slider",
                                ".wt-image-text-slider"
                            ),
                            aspectRatio,
                            null
                        );
                    }
                },
                init() {
                    widgetCropInit.call(this, updateWidgetCropVals(imageTextSliderTypeName));
                },
                ...getWidgetCropFunctions(imageTextSliderTypeName)
            }),
        });

        //listenToRender(imageTextSliderTypeName);

        //for img text link
        const imageTextLinkTypeName = "widget-img-txt-link";
        const imageTextLink = domc.getType(imageTextLinkTypeName);
        var compDefaults = Object.assign({},
            imageTextLink.model.prototype.defaults
        );
        compDefaults.type = imageTextLinkTypeName;
        compDefaults.tagName = "div";

        const imageTextLinkInitToolbar = imageTextLink.model.prototype.initToolbar;

        domc.addType(imageTextLinkTypeName, {
            model: imageTextLink.model.extend({
                defaults: {
                    ...compDefaults,
                    traits: [
                        ...compDefaults.traits,
                    ]
                },
                initToolbar(...args) {
                    imageTextLinkInitToolbar.apply(this, args);
                    widgetCropInitToolbar.call(this, updateWidgetCropVals(imageTextLinkTypeName));
                    updateToolbar.call(this, "fa fa-sliders", "Rearrange/Delete Images or Videos", cmdEditSortDelete, true);
                },
            }),
            view: imageTextLink.view.extend({
                // Bind events
                events: {
                    // If you want to bind the event to children elements
                    // 'click .someChildrenClass': 'methodName',
                    dblclick: "handleClick",
                    'click .js-add-slide': "handleClick"
                },
                onRender({ el }) {
                    const buttonWrapper = document.createElement('div');
                    buttonWrapper.setAttribute("class", "add-slide-wrapper");
                    const addSlideButton = document.createElement('button');
                    addSlideButton.setAttribute("class", "js-add-slide");
                    addSlideButton.innerText = "+";
                    buttonWrapper.appendChild(addSlideButton);
                    const imageContent = el.querySelector(".col-md-8.col-sm-8.img-content");
                    if(imageContent){
                        imageContent.appendChild(buttonWrapper);
                    }
                },
                handleClick: function(e) {
                    var attrs = this.model.getAttributes();
                    var aspectRatio = parseFloat(attrs["data-imgaspect"]) ?
                        parseFloat(attrs["data-imgaspect"]) :
                        1.777;
                    if (!$(e.target).is(".text-content") &&
                        !$(e.target).parents().hasClass("text-content")
                    ) {
                        config.showAssetsMethod(
                            this,
                            getSIUPopupOnSelectCallback(
                                this.model,
                                "image-slider",
                                ".wt-img-txt-link"
                            ),
                            aspectRatio,
                            null
                        );
                    }
                },
                init() {
                    widgetCropInit.call(this, updateWidgetCropVals(imageTextLinkTypeName));
                },
                ...getWidgetCropFunctions(imageTextLinkTypeName)
            }),
        });

        //listenToRender(imageTextLinkTypeName);

        // for variable image text slider
        const variableImageTextSliderTypeName = "widget-variable-imagetext-slider";
        const variableImageTextSlider = domc.getType(variableImageTextSliderTypeName);
        var compDefaults = Object.assign({},
            variableImageTextSlider.model.prototype.defaults
        );
        compDefaults.type = variableImageTextSliderTypeName;
        compDefaults.tagName = "div";

        const variableImageTextSliderToolbar = variableImageTextSlider.model.prototype.initToolbar;

        domc.addType(variableImageTextSliderTypeName, {
            model: variableImageTextSlider.model.extend({
                defaults: compDefaults,
                initToolbar(...args) {
                    variableImageTextSliderToolbar.apply(this, args);
                    updateToolbar.call(this, "fa fa-sliders", "Rearrange/Delete Images or Videos", cmdEditSortDelete, true);
                },
            }),
            view: variableImageTextSlider.view.extend({
                // Bind events
                events: {
                    // If you want to bind the event to children elements
                    // 'click .someChildrenClass': 'methodName',
                    dblclick: "handleClick",
                    'click .js-add-slide': "handleClick"
                },
                onRender({ el }) {
                    const buttonWrapper = document.createElement('div');
                    buttonWrapper.setAttribute("class", "add-slide-wrapper");
                    const addSlideButton = document.createElement('button');
                    addSlideButton.setAttribute("class", "js-add-slide");
                    addSlideButton.innerText = "+";
                    buttonWrapper.appendChild(addSlideButton);
                    const imageTextSlider = el.querySelector(".wt-variable-imagetext-slider");
                    if(imageTextSlider){
                        imageTextSlider.appendChild(buttonWrapper);
                    }
                },
                handleClick: function(e) {
                    var attrs = this.model.getAttributes();
                    var aspectRatio = parseFloat(attrs["data-imgaspect"]) ?
                        parseFloat(attrs["data-imgaspect"]) :
                        1.777;
                    if (!$(e.target).is(".text-content") &&
                        !$(e.target).parents().hasClass("text-content")
                    ) {
                        config.showAssetsMethod(
                            this,
                            getSIUPopupOnSelectCallback(
                                this.model,
                                "image-slider",
                                ".wt-variable-imagetext-slider-container"
                            ),
                            aspectRatio,
                            null
                        );
                    }
                },
            }),
        });

        //end
    }

    // Component and functionality for Text-Group-Widget

    const textGroupTypeName = "widget-text-group";
    const textGroup = domc.getType(textGroupTypeName);
    var compDefaults = Object.assign({}, textGroup.model.prototype.defaults);
    compDefaults.type = textGroupTypeName;
    compDefaults.tagName = "div";
    const textGroupInitToolbar = textGroup.model.prototype.initToolbar;

    domc.addType(textGroupTypeName, {
        model: textGroup.model.extend({
            defaults: {
                ...compDefaults,
                traits: [
                    ...compDefaults.traits,
                    {
                        type: 'select',
                        label: 'Desktop Type',
                        name: 'data-desktopwidgettype',
                        options: [
                            { id: 'widget_text_card_layout', name: 'Cards' },
                            { id: 'widget_text_slider', name: 'Slider' },
                        ]
                    },
                    {
                        type: 'select',
                        label: 'Mobile Type',
                        name: 'data-mobilewidgettype',
                        options: [
                            { id: 'widget_text_card_layout', name: 'Cards' },
                            { id: 'widget_text_slider', name: 'Slider' },
                        ]
                    },
                    {
                        type: 'select',
                        name: 'data-textsinrow',
                        label: 'Texts in row',
                        options: [
                            { id: '1', name: '1' },
                            { id: '2', name: '2' },
                            { id: '3', name: '3' },
                            { id: '4', name: '4' },
                        ]
                    },
                    {
                        type: 'select',
                        name: 'data-textalignment',
                        label: 'Alignment',
                        options: [
                            { id: 'flex-start', name: 'Left' },
                            { id: 'center', name: 'Center' },
                            { id: 'flex-end', name: 'Right' },
                        ]
                    },
                ]
            },
            init() {
                this.listenTo(this, 'change:attributes:data-textsinrow', this.changeRowSize)
            },
            changeRowSize() {
                const attrs = this.getAttributes();
                const textInRow = parseInt(attrs['data-textsinrow'], 10);
                const textInRowNew = parseInt((12/textInRow), 10);
                const textBlockArray = this.find(".text-content");
                if(textBlockArray && textBlockArray.length) {
                    textBlockArray.forEach((ele,i) => {
                        ele.setClass([`col-sm-${textInRowNew}`, `col-md-${textInRowNew}`, `text-block-${i+1}`, `text-content`]);
                    });
                }
            },
            initToolbar(...args) {
                textGroupInitToolbar.apply(this, args);
                updateToolbar.call(this, "fa fa-gear", "Configure Text Widget", cmdOpenTextGroupConfig, true);
                updateToolbar.call(this, "fa fa-sliders", "Rearrange/Delete Text Block", cmdEditSortDelete, true);
            },
        }),
        view: textGroup.view.extend({
            events: {
                    dblclick: "handleClick",
                    'click .js-add-text-group': "onAddNewGroup"
                },
            onAddNewGroup: function(e){
                const attrs = this.model.view.model.getAttributes();
                const noOfGroups = parseInt(attrs['data-noofgroup'], 10) + 1;
                this.model.view.model.setAttributes({
                    ...attrs,
                    "data-noofgroup": noOfGroups,
                });
                const contentBlock = this.model.view.model.find(".content-block");
                if(contentBlock && !contentBlock.length) return;
                const textBlocks = contentBlock[0].find(".text-content");
                if(textBlocks && !textBlocks.length) return;
                const lastClonedTextBlock = textBlocks[textBlocks.length - 1].clone();
                contentBlock[0].append(lastClonedTextBlock);
            },
            onRender({ el }) {
                    const buttonWrapper = document.createElement('div');
                    buttonWrapper.setAttribute("class", "add-new-group-wrapper");
                    const addSlideButton = document.createElement('button');
                    addSlideButton.setAttribute("class", "js-add-text-group");
                    addSlideButton.innerText = "+";
                    buttonWrapper.appendChild(addSlideButton);
                    const contentBlock = el.querySelector(".content-block");
                    if(contentBlock){
                        contentBlock.appendChild(buttonWrapper);
                    }
                },
        })
    });

    summaryWidget(editor, config);
    const showTextPopup = textEditor(editor, config);

    const textType = domc.getType("text");

    domc.addType("text", {
        view: textType.view.extend({
            events: {
                dblclick: "handleClick",
            },
            handleClick: function(e) {
                if(this.model && this.model.attributes && !this.model.attributes.editable){
                    return;
                }
                showTextPopup(editor.Modal, this.model, "Text Editor");
                e.stopPropagation();
            },
        }),
    });

    // img txt link
    const imageComp = domc.getType("image");
    var hCompDefaultImgLink = Object.assign({},
        imageComp.model.prototype.defaults
    );
    hCompDefaultImgLink.type = "image";
    hCompDefaultImgLink.tagName = "img";
    hCompDefaultImgLink.traits.push({
        type: "text",
        name: "data-imglink",
        label: "Enter image title",
    });
    domc.addType("image", {
        model: imageComp.model.extend({
            defaults: hCompDefaultImgLink,
        }),
        view: imageComp.view.extend({
            updateVals() {
                var attrs = this.model.getAttributes();
                attrs["data-imglink"] = this.model.attributes["data-imglink"];
                this.model.setAttributes(attrs);
                this.updateScript();
            },

            init() {
                this.listenTo(this.model, "change:data-href", this.updateVals);
                const attrs = this.model.getAttributes();
                if (attrs["data-imglink"]) {
                    this.model.set("data-imglink", attrs["data-imglink"]);
                }
            },
        }),
    });

    editor.TraitManager.addType("poster", {
        evenCapture: ["input"],
        createInput({
            trait,
            component
        }) {
            const el = document.createElement("div");
            el.className = "poster-parent";
            el.innerHTML = `<input type="text" class="poster" value="${component.view.el.getAttribute("poster")}" />
            <button class="btn btn-primary clear-btn" title="Clear Poster">Clear</button>`;

            function assignPosterVal(asset = "") {
                var attrs = trait.target.getAttributes();
                trait.target.setAttributes({
                    ...attrs,
                    poster: asset,
                    "data-poster": asset,
                });
            }

            el.addEventListener("click", function(evt) {
                config.showAssetsMethod({
                        property: "poster"
                    },
                    function(asset) {
                        el.querySelector(".poster").value = asset;
                        assignPosterVal(asset);
                    },
                    1.77
                );
            });

            const delBtn = el.querySelector(".clear-btn");
            delBtn.addEventListener("click", function(evt) {
                evt.stopPropagation();
                assignPosterVal();
                el.querySelector(".poster").value = "";
            });

            return el;
        },
        onEvent({
            elInput,
            component,
            event
        }) {
            let tracks = event.target.value;
            component.addAttributes({
                tracks
            });
        },
    });
    domc.addType("track", {
        view: {
            tagName: "track",
            attributes: {
                crossorigin: "anonymous"
            },
        },
    });
};