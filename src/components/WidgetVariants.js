import { cmdCloneWidget } from "../consts";

export default (editor, config = {}) => {
    const domc = editor.DomComponents;
    const defaultType = domc.getType('default');

    var compNames = [];
    config.WidgetsToShowVariants.forEach(widget_id => {
        compNames.push(widget_id.replace(/_/g, "-"));
    });

    compNames.forEach(compName => {
        var compDefaults = Object.assign({}, defaultType.model.prototype.defaults);
        compDefaults.type = compName;
        compDefaults.tagName = 'div';
    
        domc.addType(compName, {
            model: defaultType.model.extend({
                defaults: compDefaults
            }),
            view: defaultType.view.extend({
                // Bind events
                events: {
                    // If you want to bind the event to children elements
                    // 'click .someChildrenClass': 'methodName',
                    dblclick: 'handleClick'
                },
                render() {
                    this.renderAttributes();
                    if (this.modelOpt.temporary) return this;
                    this.renderChildren();
                    this.updateScript();
                    this.postRender();
                    var attributes = this.model.getAttributes();
                    attributes['data-gjs-type'] = compName;
                    attributes['data-gjs-droppable'] = 'false';
                    this.model.setAttributes(attributes);
                    return this;
                },
                handleClick: function (e) {
                    const modal = editor.Modal;
                    var model = this.model;
                }
            })
        });
        
    });

    editor.on('component:selected', comp => {
        if (comp && comp.attributes) {
            if (compNames.includes(comp.attributes.type)) {
                let hasButtonBool = false;
                let cloneButtonIndex = -1;
                let isComponentCopyableToOtherPage = true;
                const parent = comp.parent();
                // If parent is copyable then don't show custom clone option
                if(parent && parent.attributes && parent.attributes.copyable){
                    isComponentCopyableToOtherPage = false;
                }
                const componentAttributes = comp.attributes && comp.attributes.attributes;
                const widgetVariantId =
                  componentAttributes &&
                  (componentAttributes["data-simpwidget-variant"] ||
                    componentAttributes["data-simp-type-variant"] ||
                    componentAttributes["data-simp-container-widget-variant"]);
                if(widgetVariantId){
                    isComponentCopyableToOtherPage = isComponentCopyableToOtherPage && true;
                }
                var toolbar = comp.get('toolbar');
                for (let i = 0; i < toolbar.length; i++) {
                    if (toolbar[i].command === 'show-widget-variants') {
                        hasButtonBool = true;
                    }
                    if (toolbar[i].command === "tlb-clone") {
                        cloneButtonIndex = i;
                    }
                }

                if (!hasButtonBool) {
                    toolbar.push({
                        attributes: { class: 'fa fa-cubes', title: 'Select Widget Vaiants' },
                        command: 'show-widget-variants'
                    });
                }
                if (
                  cloneButtonIndex !== -1 &&
                  toolbar[cloneButtonIndex].command &&
                  isComponentCopyableToOtherPage
                ) {
                  toolbar[cloneButtonIndex].command = cmdCloneWidget;
                }
                comp.set('toolbar', toolbar);
                comp.emitUpdate('toolbar');
            }
        }
    });
}