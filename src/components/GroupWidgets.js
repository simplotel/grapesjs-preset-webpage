export default (editor, config = {}) => {
    const domc = editor.DomComponents;

    function getDisableAllDataAttrs() {
        return 'data-gjs-copyable="false" data-gjs-draggable="false" data-gjs-droppable="false" data-gjs-editable="false" data-gjs-highlightable="false" data-gjs-hoverable="false" data-gjs-layerable="false" data-gjs-removable="false" data-gjs-resizable="false" data-gjs-selectable="false" data-gjs-stylable="false"';
    }

    function getHtmlElement(name, props, attrs) {
        var innerItem = document.createElement(name);
        for (var key in props) {
            innerItem[key] = props[key];
        }
        for (var key in attrs) {
            innerItem.setAttribute(key, attrs[key]);
        }
        return innerItem;
    }

    // function to highlight activated checkbox for configuration window
    let prevHighlightedCheckbox = null;
    function highlightActiveCheckbox(listItem) {
        if (prevHighlightedCheckbox) {
            prevHighlightedCheckbox.style.background = "unset";
        }
        listItem.style.background = "#d983a66e";
        prevHighlightedCheckbox = listItem;
    }

    // function for configuration button
    let prevConfigWindow = null;
    function configureBtnAction(configureBtn, listData, listItem, radioBtn) {
        let radioName = listData.itemName;
        let checkboxName = listData.text;
        let listEle = listData.list;
        let radioId = listData.id;
        let filterId = listData.tagId;
        // these are attribute names
        let filterIdName = listData.filterIdName;
        let radioIdName = listData.dataIdName;

        // initiate configuration card for every button (Different card for different filter)
        let configWindow = createConfigurationCard(listData, radioBtn);
        setTimeout(() => {
            listEle.parentElement.appendChild(configWindow);
        }, 0);
        configureBtn.addEventListener('click', function () {
            if (prevConfigWindow) {
                prevConfigWindow.style.display = "none";
            }
            configWindow.style.display = "block";
            highlightActiveCheckbox(listItem);
            prevConfigWindow = configWindow;
        })
    }

    function createListItem(listData, radioBtn) {
        let listStyle = "margin: 5px 0px;padding: 2px 10px 2px 0px;border: 1px solid #d3d3d375;border-radius: 3px;";
        let listItem = getHtmlElement('li', {}, { style: listStyle });

        let labelStyle = "width: 100%;display: flex;align-items: center;margin-bottom: 0px;";
        let label = getHtmlElement('label', {}, { style: labelStyle, class: "labelToggle" });
        const inputAttr = {
            name: listData.name,
            type: listData.type,
        }
        if (listData.isChecked) {
            inputAttr['checked'] = listData.isChecked;
        }
        inputAttr[`data-${listData.dataIdName}`] = listData.id;

        if (listData.type === "checkbox") {
            inputAttr[`data-${listData.filterIdName}`] = listData.tagId;
        }
        inputAttr['style'] = "margin-left: 10px; margin-top: 0px;";
        let inputBox = getHtmlElement('input', {}, inputAttr);
        label.appendChild(inputBox);
        label.appendChild(getHtmlElement('span', { innerText: listData.text }, { style: "padding-left: 10px;font-weight: normal;", class: "tagName" }));

        // extra for checkbox type which is filter
        if (listData.type === "checkbox") {
            let configureBtn = getHtmlElement('button', { "innerHTML": `<i class="fa fa-cogs" aria-hidden="true"></i> Configure` }, { style: `margin-left: auto;border: none; border-radius: 2px; background: rgb(217, 131, 166); color: white; font-size: 10px; display: ${listData.isChecked ? "inline" : "none"};` });
            label.appendChild(configureBtn);
            /* 
            configuration part for configuration button which is independant for each filter
            listData.itemName --> radio parent name
            listData.text --> checkbox name
            listData.list --> main ul element
            listData.id --> group id
            listData.tagId --> filter btn id (default type)
            listData.filterIdName --> filter attribute name
            listData.dataIdName --> radio attribute name
            */
            configureBtnAction(configureBtn, listData, listItem, radioBtn);
            inputBox.addEventListener('change', function (e) {
                if (inputBox.checked) {
                    configureBtn.style.display = "inline";
                } else {
                    configureBtn.style.display = "none";
                }
            });
        }

        /* 
        updating the status of the filter in final configuration object 
        so that it can be used in templating side 
        */
        label.addEventListener('click', function (e) {
            if (radioBtn && e.target.tagName === "INPUT") {
                updateRadioBtnConfigData(radioBtn, e.target.checked, "isActive", listData.tagId);
            }

            let radioTarget = document.querySelectorAll('.inner-container.box-config > ul > li > label > input');
            radioTarget.forEach((target) => {
                target.addEventListener("change", () => {
                    let targetDataVal = target.attributes["data-groupid"].value;
                    radioTarget.forEach((finalTarget) => {
                        let finalTargetDataVal = finalTarget.attributes["data-groupid"].value;
                        if (finalTargetDataVal !== targetDataVal) {
                            const parentElement = finalTarget.parentElement;
                            const disableBtn = parentElement.querySelector('.disable-btn');
                            const selectBtn = parentElement.querySelector('.select-btn');
                            const listDetials = parentElement.nextSibling;
                            if (disableBtn) {
                                disableBtn.classList.remove('open');
                            }
                            if (selectBtn) {
                                selectBtn.classList.remove('open');
                            }
                            if (listDetials) {
                                listDetials.classList.remove('open');
                            }
                        } else {
                            const parentElement = finalTarget.parentElement;
                            const disableBtn = parentElement.querySelector('.disable-btn');
                            const selectBtn = parentElement.querySelector('.select-btn');
                            const listDetials = parentElement.nextSibling;
                            if (disableBtn) {
                                disableBtn.classList.add('open');
                            }
                            if (selectBtn) {
                                selectBtn.classList.add('open');
                            }
                            if (listDetials) {
                                listDetials.classList.add('open');
                            }
                        }
                    });
                });
            });
        })
        listItem.appendChild(label);
        return listItem;
    }

    function configureBtns(btn, radioBtn, commonAttr, flag) {
        let allCheckbox = radioBtn.querySelectorAll(`input[type="checkbox"]${flag ? ":not(:checked)" : ":checked"}`);
        allCheckbox.forEach(function (box) {
            box.checked = flag;
            updateRadioBtnConfigData(radioBtn, flag, "isActive", box.getAttribute(`data-${commonAttr.filterIdName}`));
            box.dispatchEvent(new Event("change"));
        });
    }

    // Disable and Select buttons configuration
    function getActionBtns(radioBtn, commonAttr) {
        let disableAllBtn = getHtmlElement('button', { "innerText": "Disable all" }, { style: "margin: auto;border: 1px solid #d3d3d375; border-radius: 2px; background: transparent; font-size: 10px;", class: "disable-btn" });
        let selectAllBtn = getHtmlElement('button', { "innerText": "Select all" }, { style: "margin-left: auto;border: 1px solid #d3d3d375; border-radius: 2px; background: transparent; font-size: 10px;", class: "select-btn" });
        disableAllBtn.addEventListener('click', function (e) {
            e.stopPropagation();
            configureBtns(disableAllBtn, radioBtn, commonAttr, false);
        })

        selectAllBtn.addEventListener('click', function (e) {
            e.stopPropagation();
            configureBtns(selectAllBtn, radioBtn, commonAttr, true);
        })

        return { disableAllBtn, selectAllBtn };
    }

    // this is the default configuration for widgets if it's not configured
    function getDefaultConfig(id) {
        return {
            filterId: id,
            isActive: false,
            all: {
                name: "All",
                isChecked: true,
            },
            multi: false,
            type: "button"
        }
    }

    function getConfigurationObject(checkboxData) {
        let ATTRIBUTE_NAME = "data-summary_config";
        let attrs = editor.getSelected().getAttributes();
        /* 
        checkboxData.id --> radio id
        checkboxData.tagId --> filter id (checkbox)
        */
        let prevConfigObj = attrs[ATTRIBUTE_NAME] ? JSON.parse(attrs[ATTRIBUTE_NAME]) : null;
        let configObj = null;
        if (prevConfigObj && +prevConfigObj.groupId === +checkboxData.id && prevConfigObj.configs) {
            JSON.parse(prevConfigObj.configs).forEach(function (item) {
                /* 
                item is a config for one checkbox,
                checking custom configuration for filter
                */
                if (+checkboxData.tagId === +item.filterId) {
                    configObj = item;
                }
            })
        }

        // we'll have to get the default configuration for the filter if the configuration is not there
        if (!configObj) {
            configObj = getDefaultConfig(checkboxData.tagId);
        }
        return configObj;
    }

    /* 
    function to create list for radio btns and checkboxes (for filter groups and for filters)
    -- Main function
    */
    function createMultiLevelList(data, dataId, allFilterIds, dataIdName, filterIdName) {
        var list = getHtmlElement('ul', {}, { style: "list-style: none; padding-left: 0px;max-width: 500px;min-width: 30%;" });
        const commonAttr = {
            dataId,
            dataIdName,
            filterIdName
        }
        data.forEach(function (item) {
            let radioData = {
                ...commonAttr,
                id: item.id,
                name: "name" + dataIdName,
                text: item.name,
                type: "radio",
                isChecked: item.id === dataId,
            }
            let radioBtn = createListItem(radioData);

            // to disable all unused checkboxes
            radioBtn.addEventListener('click', function (e) {
                if (e.target.tagName === "INPUT") {
                    let allUnCheckedBoxes = list.querySelectorAll('input[type="radio"]:not(:checked)');
                    allUnCheckedBoxes.forEach(function (item) {
                        let disableBtn = item.parentElement.querySelector('.disable-btn');
                        if (disableBtn) {
                            disableBtn.click();
                        }
                    })
                }
            });

            /* 
            -we need data of all filters
            -we need different array for different group (radio btn is a group and checkbox is the filter)
            -we are taking configuration for each checkbox (it can be default or 
            custom, function "getConfigurationObject" will figure that out)
            */
            let summaryConfigArray = [];

            if (item.tags && item.tags.length) {
                // adding action buttons "disable and select all" to radio Btn
                let aBtns = getActionBtns(radioBtn, commonAttr);
                radioBtn.querySelector('label').appendChild(aBtns.disableAllBtn);
                radioBtn.querySelector('label').appendChild(aBtns.selectAllBtn);

                let tagList = getHtmlElement('ul', {}, { style: "list-style: none;", class: "ulToggle" });
                item.tags.forEach(function (tag) {
                    let checkboxData = {
                        id: item.id,
                        tagId: tag.id,
                        ...commonAttr,
                        name: "name" + dataIdName,
                        text: tag.name,
                        itemName: item.name,
                        type: "checkbox",
                        list: list, // the main ul element passing for configuration part
                        isChecked: radioData.isChecked && !!(Array.isArray(allFilterIds) ? allFilterIds.find(function (ele) { return +ele === tag.id }) : allFilterIds.split(",").find(function (ele) { return +ele === tag.id })),
                    };

                    /* 
                    before adding listItem in the child list we need to configure data for that
                    we'll have to pass "checkboxData" to get previously customized data for that filter
                    */
                    checkboxData["configData"] = getConfigurationObject(checkboxData);
                    checkboxData["configData"]["isActive"] = checkboxData.isChecked;
                    summaryConfigArray.push(checkboxData["configData"]);
                    tagList.appendChild(createListItem(checkboxData, radioBtn));
                });
                radioBtn.appendChild(tagList);

                /* 
                setting configuration object to radioBtn (Group).
                It's a Array of all the configurations of the filters in that group
                */
                radioBtn.setAttribute("data-summary_config", JSON.stringify(summaryConfigArray));
            }

            list.appendChild(radioBtn);
        });

        return list;
    }

    /* 
    function for "All" feature configuration
    - Custom name for All 
    */
    function getAllConfig(allObj, radioBtn, filterId) {
        let allConfig = getHtmlElement('label', {}, { style: "display: flex; align-items: center; width: fit-content; border-radius: 2px; border: 1px solid #d3d3d375; padding: 5px;" });
        let dataObj = { name: allObj.name };
        let checkAttr = { type: "checkbox", style: "width: 20px; height: 20px; margin: 0px;" };
        if (allObj.isChecked) {
            checkAttr['checked'] = true;
            dataObj["isChecked"] = true;
        }
        allConfig.appendChild(getHtmlElement('input', {}, checkAttr));
        let inputField = getHtmlElement('input', {}, { class: "form-control", type: "text", style: "margin-left: 5px; max-width: 40%; min-width: 200px; border-radius: 2px !important; padding: 5px; height: 25px; font-weight: normal;", placeholder: "name...", value: allObj.name });
        inputField.addEventListener('input', function (e) {
            e.stopPropagation();
            dataObj["name"] = e.target.value;
            updateRadioBtnConfigData(radioBtn, dataObj, "all", filterId);
        });
        inputField.addEventListener('click', function (e) {
            e.stopPropagation();
        });
        allConfig.addEventListener('click', function (e) {
            if (e.target.tagName === "INPUT") {
                dataObj["isChecked"] = e.target.checked;
                updateRadioBtnConfigData(radioBtn, dataObj, "all", filterId);
            }
        });
        allConfig.appendChild(inputField);
        return allConfig;
    }

    /*
    function for filter type configuration
    - Default "button"
    */
    function getTypeConfig(config, radioBtn, filterId) {
        let selectTypes = getHtmlElement('div', {}, { class: "s-select-types", style: "display: flex;" });
        let devices = ['desktop', 'mobile'];
        /* 
        Multiple options can be added here 
        - For example: Radio and Checkbox
        */
        let options = [
            {
                name: "Button",
                value: "button",
            },
            {
                name: "Dropdown",
                value: "select",
            }
        ];

        devices.forEach(device => {
            let typeConfig = getHtmlElement('div', { "innerText": `Select ${device} type` }, { style: "margin-right: 10px;" });
            let selectBox = getHtmlElement('select', {}, { class: "form-control", style: "border-radius: 2px !important; padding:5px;" });

            options.forEach(function (item) {
                let typeConfigObj = { value: item.value };
                if (
                    (device === 'desktop' && item.value === config.type)
                    ||
                    (device === 'mobile' && item.value === config.mobileType)
                ) {
                    typeConfigObj['selected'] = true;
                }
                selectBox.appendChild(getHtmlElement('option', { "innerText": item.name }, typeConfigObj));
            });
            selectBox.setAttribute('data-device', device);

            selectBox.addEventListener('change', function (e) {
                if (device === 'mobile') {
                    updateRadioBtnConfigData(radioBtn, e.target.value, "mobileType", filterId);
                } else {
                    updateRadioBtnConfigData(radioBtn, e.target.value, "type", filterId);
                    updateRadioBtnConfigData(radioBtn, e.target.value, "mobileType", filterId);
                    const mobileSelect = selectTypes.querySelector('[data-device="mobile"]');
                    mobileSelect.value = e.target.value;
                }
            });

            typeConfig.appendChild(selectBox);
            selectTypes.appendChild(typeConfig);
        });

        return selectTypes;
    }

    /*
    function to update configuration object of each group
    */
    function updateRadioBtnConfigData(radioBtn, data, propertyName, filterId) {
        // configObj is a array of configurations
        let configObj = JSON.parse(radioBtn.getAttribute('data-summary_config'));
        if (configObj) {
            configObj.forEach(function (filter) {
                if (+filter.filterId === +filterId) {
                    filter[propertyName] = data;
                }
            });
        }
        radioBtn.setAttribute("data-summary_config", JSON.stringify(configObj));
    }

    function createConfigurationCard(listData, radioBtn) {
        let radioName = listData.itemName;
        let checkboxName = listData.text;
        /* 
        listData also have config object
        - listData.configData --> default or custom config data
        - radioBtn --> radio element for config data
        */

        let configContainer = getHtmlElement('div', {}, { class: "configuration-card", style: "width: 70%;border: 1px solid #d3d3d375;margin: 5px 0px 15px 10px;border-radius: 3px; padding: 15px; display: none;" });
        let configWindowheading = getHtmlElement('h4', { "innerText": "Custom Configuration Window" }, { style: "margin-top: 0px; font-weight: bold;" });
        // close icon in configuration window
        let closeIcon = getHtmlElement('span', { "innerHTML": `<i class="fa fa-times-circle" aria-hidden="true"></i>` }, { style: "float: right; cursor: pointer;" });
        closeIcon.addEventListener('click', () => {
            configContainer.style.display = "none";
            if (prevHighlightedCheckbox) {
                prevHighlightedCheckbox.style.background = "unset";
                prevHighlightedCheckbox = null;
            }
        })
        configWindowheading.appendChild(closeIcon);
        configContainer.appendChild(configWindowheading);
        // for unique window
        configContainer.appendChild(getHtmlElement('span', { "innerText": radioName + " / " }, {}));
        configContainer.appendChild(getHtmlElement('span', { "innerText": checkboxName }, {}));
        // configContainer.appendChild(getHtmlElement('div', {"innerText": "Filter ID: " + listData.tagId}, {}));

        // for all configuration
        configContainer.appendChild(getAllConfig(listData.configData.all, radioBtn, listData.tagId));
        // for multiple select
        let multipleSelectCon = getHtmlElement('label', {}, { style: "display: flex; width: fit-content; padding: 5px; border: 1px solid #d3d3d375; border-radius: 2px;" });
        let multiAttr = { type: "checkbox", value: "multi-true", style: "width: 20px; height: 20px; margin: 0px;", };
        let multiData = false;
        if (listData.configData.multi) {
            multiAttr["checked"] = true;
            multiData = true;
            updateRadioBtnConfigData(radioBtn, multiData, "multi", listData.tagId);
        }

        multipleSelectCon.addEventListener('click', function (e) {
            if (e.target.tagName === "INPUT") {
                updateRadioBtnConfigData(radioBtn, e.target.checked, "multi", listData.tagId);
            }
        });
        multipleSelectCon.appendChild(getHtmlElement('input', {}, multiAttr));
        multipleSelectCon.appendChild(getHtmlElement('span', { "innerText": "Need multiple select?" }, { style: "margin-left: 5px; font-weight: normal;" }));
        /* 
        - To enable multiple select feature, uncomment below line of code.
        - Also need to write logic for it in templating side.
        */
        //configContainer.appendChild(multipleSelectCon);
        // for type select
        configContainer.appendChild(getTypeConfig(listData.configData, radioBtn, listData.tagId));
        return configContainer;
    }

    function showGroupWidgetPopup(modal, model, listDataUrl, htmlDataUrl, title, errorMessage, dataIdName, dataWidgetIdKey, dataWidgetVariantIdKey, filterIdName) {
        var content = '<div style="text-align: center;padding: 20px;min-height: 180px;"><i class="fa fa-refresh fa-spin fa-3x fa-fw" aria-hidden="true"></i></div>';

        function handleSave() {
            var object = modal.getContent();
            var attrs = model.getAttributes();
            var dataId = attrs[`data-${dataIdName}`] || '0';
            var filterId = attrs[`data-${filterIdName}`] || '0';
            var widgetId = attrs[`data-${dataWidgetIdKey}`] || '0';
            var widgetVariantId = attrs[`data-${dataWidgetVariantIdKey}`] || '0';

            let selectedRadio = object.querySelector('input[type="radio"]:checked');
            dataId = selectedRadio.getAttribute(`data-${dataIdName}`) || '0';
            let selectedCheckboxes = selectedRadio ? selectedRadio.closest('li').querySelectorAll('input[type="checkbox"]:checked') : ['0'];
            let allFilterIds = [];
            selectedCheckboxes.forEach(function (item) {
                allFilterIds.push(+item.getAttribute(`data-${filterIdName}`));
            });
            filterId = allFilterIds;

            /* 
            Final configuration object structure
            {
            groupId: 300385, // INT, STRING
            configs: [
            {
            filterId: 1, // INT, STRING
            isActive: true, // BOOL
            all: {
            name: "All", // STRING
            isChecked: true, // BOOL
            },
            multi: false, // BOOL
            type: "button", // STRING
            mobileType: "button", // STRING
            },
            ]
            }
            */

            // adding new filter data on summary widget
            let finalConfigurationObj = { groupId: dataId, configs: selectedRadio.closest('li').getAttribute('data-summary_config') };
            attrs[`data-summary_config`] = JSON.stringify(finalConfigurationObj);

            if (dataId && widgetVariantId && dataId !== '0' && widgetVariantId !== '0') {
                if (dataId === attrs[`data-${dataIdName}`] && filterId === attrs[`data-${filterIdName}`]) {
                    modal.close();
                } else {
                    $.ajax({
                        url: htmlDataUrl,
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify({
                            "id": dataId,
                            "widget_id": widgetId,
                            "widget_variant_id": widgetVariantId,
                        }),
                        beforeSend: function (xhr, settings) {
                            if (!csrfSafeMethod(settings.type)) {
                                xhr.setRequestHeader("X-CSRFToken", csrftoken);
                            }
                        },
                        success: function (data, textStatus, jqXHR) {
                            model.set('content', data.html_data);
                            attrs[`data-${dataIdName}`] = dataId;
                            attrs[`data-${filterIdName}`] = filterId;
                            model.setAttributes(attrs);
                            modal.close();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            model.set('content', "<h2 " + getDisableAllDataAttrs() + ">" + errorMessage + "</h2>");
                            $.growl.error({ message: "Sorry something went wrong, you might want to try again by refreshing the page!" });
                        }
                    });
                }
            } else {
                model.set('content', "<h2 " + getDisableAllDataAttrs() + ">" + errorMessage + "</h2>");
            }
        }

        modal.onceOpen(function () {
            $.ajax({
                url: listDataUrl,
                type: "GET",
                beforeSend: function (xhr, settings) {
                    if (!csrfSafeMethod(settings.type)) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                },
                success: function (data, textStatus, jqXHR) {
                    var modalHtml = getHtmlElement('table', {}, { style: 'margin: 10px 0 10px 15px; width: 100%;' });
                    var modelAttrs = model.getAttributes();
                    var parentItem = '', childItem = '';
                    var dataId = parseInt(modelAttrs[`data-${dataIdName}`]);
                    var filterId = parseInt(modelAttrs[`data-${filterIdName}`]);

                    const pfx = editor.getConfig('stylePrefix');
                    let modalHtmlContent = getHtmlElement('div', {}, {});
                    let innerContainer = getHtmlElement('div', {}, { class: "inner-container box-config", style: "display: flex;" });

                    // Creating Multilevel list in the modal
                    innerContainer.appendChild(createMultiLevelList(data.data, dataId, editor.getSelected().getAttributes()[`data-${filterIdName}`], dataIdName, filterIdName));

                    modalHtmlContent.appendChild(innerContainer);
                    modalHtmlContent.appendChild(getHtmlElement('button', { 'innerHTML': 'Add', 'className': `${pfx}btn-prim ${pfx}btn-add__file-attachment`, 'onclick': handleSave }, { style: 'float: right;' }));
                    modal.setContent(modalHtmlContent);
                    
                    // Keeping the selected radio button's contents visible
                    let afterClose = document.querySelectorAll('.gjs-mdl-container .gjs-mdl-content #gjs-mdl-c  .inner-container > ul  > li > label input');
                    afterClose.forEach(function(close){
                        if (close.checked === true){
                            close.parentElement.nextSibling.classList.add('open');
                        }
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $.growl.error({ message: "Sorry something went wrong, you might want to try again by refreshing the page!" });
                }
            });
        });
        modal.open({ title, content }).onceClose(function () {
            console.log('Summary Widget modal closed');
        });
    }

    const summaryWidgetType = domc.getType('summary-widget');
    var compDefaults = Object.assign({}, summaryWidgetType.model.prototype.defaults);
    compDefaults.type = 'summary-widget';
    compDefaults.tagName = 'div';

    domc.addType('summary-widget', {
        model: summaryWidgetType.model.extend({
            defaults: compDefaults
        }),
        view: summaryWidgetType.view.extend({
            init({ model }) {
                this.listenTo(model, 'change:content', this.handleModelContentChange);
            },
            // Bind events
            events: {
                // If you want to bind the event to children elements
                // 'click .someChildrenClass': 'methodName',
                dblclick: 'handleClick'
            },
            render() {
                this.renderAttributes();
                if (this.modelOpt.temporary) return this;
                this.renderChildren();
                this.updateScript();
                this.postRender();
                var attributes = this.model.getAttributes();
                attributes['data-gjs-type'] = 'summary-widget';
                attributes['data-gjs-droppable'] = 'false';
                this.model.setAttributes(attributes);

                return this;
            },
            handleModelContentChange: function () {
                this.el.querySelectorAll('.summary-widget').forEach(summaryWidget => {
                    summaryWidget.classList.remove('col-md-4');
                    summaryWidget.classList.add('col-sm-4');
                });
            },
            handleClick: function (e) {
                const modal = editor.Modal;
                var model = this.model;
                showGroupWidgetPopup(modal, model, config.summaryWidgetListDataUrl, config.summaryWidgetHtmlDataUrl, 'Summary Widget Groups', 'Double click here to choose summary group', 'groupid', 'simpwidget', 'simpwidget-variant', 'filtertagid');
            }
        })
    });

    const photoGalleryType = domc.getType('image-group');
    var compDefaults = Object.assign({}, photoGalleryType.model.prototype.defaults);
    compDefaults.type = 'image-group';
    compDefaults.tagName = 'div';

    domc.addType('image-group', {
        model: photoGalleryType.model.extend({
            defaults: compDefaults
        }),
        view: photoGalleryType.view.extend({
            // Bind events
            events: {
                // If you want to bind the event to children elements
                // 'click .someChildrenClass': 'methodName',
                dblclick: 'handleClick'
            },
            render() {
                this.renderAttributes();
                if (this.modelOpt.temporary) return this;
                this.renderChildren();
                this.updateScript();
                this.postRender();
                var attributes = this.model.getAttributes();
                attributes['data-gjs-type'] = 'image-group';
                attributes['data-gjs-droppable'] = 'false';
                this.model.setAttributes(attributes);
                return this;
            },
            handleClick: function (e) {
                const modal = editor.Modal;
                var model = this.model;
                showGroupWidgetPopup(modal, model, config.photoGalleryWidgetUrl, config.photoGalleryWidgetUrl, 'Photo Gallery Groups', 'Double click here to choose gallery group', 'groupid', 'simpwidget', 'simpwidget-variant');
            }
        })
    });

    const roomAmenitiesType = domc.getType('room-amenities');
    var compDefaults = Object.assign({}, roomAmenitiesType.model.prototype.defaults);
    compDefaults.type = 'room-amenities';
    compDefaults.tagName = 'div';

    domc.addType('room-amenities', {
        model: roomAmenitiesType.model.extend({
            defaults: compDefaults
        }),
        view: roomAmenitiesType.view.extend({
            // Bind events
            events: {
                // If you want to bind the event to children elements
                // 'click .someChildrenClass': 'methodName',
                dblclick: 'handleClick'
            },
            render() {
                this.renderAttributes();
                if (this.modelOpt.temporary) return this;
                this.renderChildren();
                this.updateScript();
                this.postRender();
                var attributes = this.model.getAttributes();
                attributes['data-gjs-type'] = 'room-amenities';
                attributes['data-gjs-droppable'] = 'false';
                this.model.setAttributes(attributes);
                return this;
            },
            handleClick: function (e) {
                const modal = editor.Modal;
                var model = this.model;
                showGroupWidgetPopup(modal, model, config.roomInfoWidgetUrl, config.roomInfoWidgetUrl, 'Room Amenities', 'Double click here to choose room', 'pageid', 'simpwidget', 'simpwidget-variant');
            }
        })
    });

    const roomImageSliderType = domc.getType('room-image-slider');
    var compDefaults = Object.assign({}, roomImageSliderType.model.prototype.defaults);
    compDefaults.type = 'room-image-slider';
    compDefaults.tagName = 'div';

    domc.addType('room-image-slider', {
        model: roomImageSliderType.model.extend({
            defaults: compDefaults
        }),
        view: roomImageSliderType.view.extend({
            // Bind events
            events: {
                // If you want to bind the event to children elements
                // 'click .someChildrenClass': 'methodName',
                dblclick: 'handleClick'
            },
            render() {
                this.renderAttributes();
                if (this.modelOpt.temporary) return this;
                this.renderChildren();
                this.updateScript();
                this.postRender();
                var attributes = this.model.getAttributes();
                attributes['data-gjs-type'] = 'room-image-slider';
                attributes['data-gjs-droppable'] = 'false';
                this.model.setAttributes(attributes);
                return this;
            },
            handleClick: function (e) {
                const modal = editor.Modal;
                var model = this.model;
                showGroupWidgetPopup(modal, model, config.roomInfoWidgetUrl, config.roomInfoWidgetUrl, 'Room Image Slider', 'Double click here to choose room', 'pageid', 'simpwidget', 'simpwidget-variant');
            }
        })
    });

    const formWidgetType = domc.getType('form-widget');
    var compDefaults = Object.assign({}, formWidgetType.model.prototype.defaults);
    compDefaults.type = 'form-widget';
    compDefaults.tagName = 'div';

    domc.addType('form-widget', {
        model: formWidgetType.model.extend({
            defaults: compDefaults
        }),
        view: formWidgetType.view.extend({
            // Bind events
            events: {
                // If you want to bind the event to children elements
                // 'click .someChildrenClass': 'methodName',
                dblclick: 'handleClick'
            },
            render() {
                this.renderAttributes();
                if (this.modelOpt.temporary) return this;
                this.renderChildren();
                this.updateScript();
                this.postRender();
                var attributes = this.model.getAttributes();
                attributes['data-gjs-type'] = 'form-widget';
                attributes['data-gjs-droppable'] = 'false';
                this.model.setAttributes(attributes);
                return this;
            },
            handleClick: function (e) {
                const modal = editor.Modal;
                var model = this.model;
                showGroupWidgetPopup(modal, model, config.formWidgetListDataUrl, config.summaryWidgetHtmlDataUrl, 'Forms', 'Double click here to choose a Form', 'formid', 'simpwidget', 'simpwidget-variant');
            }
        })
    });

    editor.on('block:drag:stop', model => {
        if (model && model.attributes) {
            if (model.attributes.type == 'summary-widget') {
                showGroupWidgetPopup(modal, model, config.summaryWidgetListDataUrl, config.summaryWidgetHtmlDataUrl, 'Summary Widget Groups', 'Double click here to choose summary group', 'groupid', 'simpwidget', 'simpwidget-variant', 'filtertagid');
            } else if (model.attributes.type == 'image-group') {
                showGroupWidgetPopup(modal, model, config.photoGalleryWidgetUrl, config.photoGalleryWidgetUrl, 'Photo Gallery Groups', 'Double click here to choose gallery group', 'groupid', 'simpwidget', 'simpwidget-variant');
            } else if (model.attributes.type == 'room-amenities') {
                showGroupWidgetPopup(modal, model, config.roomInfoWidgetUrl, config.roomInfoWidgetUrl, 'Room Amenities', 'Double click here to choose room', 'pageid', 'simpwidget', 'simpwidget-variant');
            } else if (model.attributes.type == 'room-image-slider') {
                showGroupWidgetPopup(modal, model, config.roomInfoWidgetUrl, config.roomInfoWidgetUrl, 'Room Image Slider', 'Double click here to choose room', 'pageid', 'simpwidget', 'simpwidget-variant');
            } else if (model.attributes.type == 'form-widget') {
                showGroupWidgetPopup(modal, model, config.formWidgetListDataUrl, config.summaryWidgetHtmlDataUrl, 'Forms', 'Double click here to choose a Form', 'formid', 'simpwidget', 'simpwidget-variant');
            }
        }
    });
}
