import {
  cmdImport,
  cmdDeviceDesktop,
  cmdDeviceTablet,
  cmdDeviceMobile,
  cmdDeviceMobileLandscape,
  cmdClear,
  PAGE_PARENT
} from './../consts';

export function validateOnSave(editor) {
  if(sessionStorage.getItem('defaultHTML') !== JSON.stringify(editor.getHtml()) || sessionStorage.getItem('defaultCSS') !== JSON.stringify(editor.getCss()) || sessionStorage.getItem('defaultJS') !== JSON.stringify(editor.getJs()) ) {
    return true;
  } 
  return false;
}

export default (editor, config) => {
  const pn = editor.Panels;
  const eConfig = editor.getConfig();
  const prv = 'preview';
  const ful = 'fullscreen';
  const obl = 'open-blocks';
  const osm = 'open-sm';
  const otm = 'open-tm';
  const ola = 'open-layers';
  const expt = 'export-template';
  eConfig.showDevices = 0;

  pn.getPanels().reset([{
    id: 'commands',
    buttons: [{}],
  },{
    id: 'options',
    buttons: [{
      id: expt,
      className: config.faIconType + ' fa-code',
      command: e => e.runCommand(expt),
    },{
      id: 'undo',
      className: config.faIconType + ' fa-undo',
      command: e => e.runCommand('core:undo'),
    },{
      id: 'redo',
      className: config.faIconType + ' fa-redo',
      command: e => e.runCommand('core:redo'),
    }],
  },{
    id: 'views',
    buttons  : [{
      id: osm,
      command: osm,
      active: true,
      className: config.faIconType + ' fa-paint-brush',
    },{
      id: otm,
      command: otm,
      disable: true,
      className: config.faIconType + ' fa-cog traits-mgr-btn',
    },{
      id: ola,
      command: ola,
      className: config.faIconType + ' fa-bars',
    }],
  }]);

  //Page Heading Panel
  const pageNamePanel = pn.addPanel({id: 'page-name'});
  pageNamePanel.get("buttons").add([
    {
      id: "custom-panel",
      label: `${config.hotelName} (${config.pageName})`,
      className: "pageName js-page-navigator",
      disable: true,
      attributes: {
        "data-toggle": "tooltip",
        "data-placement": "bottom",
        title: `${config.hotelName} (${config.pageName})`,
      },
    },
  ]);

 

  // Add devices buttons
  const panelDevices = pn.addPanel({id: 'devices-c'});
  panelDevices.get('buttons').add([{
    id: cmdDeviceDesktop,
    command: cmdDeviceDesktop,
    className: config.faIconType + ' fa-desktop',
    active: 1,
  },{
    id: cmdDeviceTablet,
    command: cmdDeviceTablet,
    className: config.faIconType + ' fa-tablet-alt',
  },{
    id: cmdDeviceMobile,
    command: cmdDeviceMobile,
    className: config.faIconType + ' fa-mobile-alt',
  },{
    id: cmdDeviceMobileLandscape,
    command: cmdDeviceMobileLandscape,
    className: config.faIconType + ' fa-mobile-alt fa-rotate-270',
  }]);

  // Add and beautify tooltips
  [
    ['export-template', 'Export'], ['undo', 'Undo'], ['redo', 'Redo']
  ].forEach(function(item) {
    pn.getButton('options', item[0]).set('attributes', {title: item[1], 'data-tooltip-pos': 'bottom'});
  });

  [
    ['open-sm', 'Style Manager'], ['open-layers', 'Layers']
  ].forEach(function(item) {
    pn.getButton('views', item[0]).set('attributes', {title: item[1], 'data-tooltip-pos': 'bottom'});
  });

  [
    [cmdDeviceDesktop, 'Desktop view'], [cmdDeviceTablet, 'Tablet view'],
    [cmdDeviceMobile, 'Mobile portrait view'], [cmdDeviceMobileLandscape, 'Mobile landscape view']
  ].forEach(function(item) {
    pn.getButton('devices-c', item[0]).set('attributes', {title: item[1], 'data-tooltip-pos': 'bottom'});
  });

  function fetchAllPageAndRenderPageDropdown(){
    const hotelId = config.hotelId;
    const currentPageId = config.pageId;
    if(!hotelId) return;
     $.ajax({
      url: `/api/v1/hotel/${hotelId}/get_page_list`,
      type: "GET",
      beforeSend: function (xhr, settings) {
          if (config.csrftoken) {
              xhr.setRequestHeader("X-CSRFToken", config.csrftoken);
          }
      },
      success: function (data, textStatus, jqXHR) {
          const list = (data && data.data) || [];
          const navLinks = list.map((page) => {
            const navLink = `<a draggable="false" class="parent-nav dropdown-item ${currentPageId === String(page.pgid) ? 'active' : ''}" title="${page.text}" href="/cms/${hotelId}/edit/page/${page.pgid}">${page.text}</a>`;
            if (page.children && page.children.length) {
              const childNavLinks = page.children.map(
                (childNav) =>
                  `<a draggable="false" class="child-nav dropdown-item ${currentPageId === String(childNav.pgid) ? 'active' : ''}" title="${childNav.text}" href="/cms/${hotelId}/edit/page/${childNav.pgid}">${childNav.text}</a>`
              );
              return navLink + childNavLinks.join("");
            }
            return navLink;
          });
          renderInternalNavMenu(navLinks.join(""));
          hydrateExitButton(list);
          
      },
      error: function (jqXHR, textStatus, errorThrown) {
         console.log("Something went wrong");
      },
    });
  }

  function hydrateExitButton(list) {
    if(!list || (list && !list.length)) return;
    const hotelId = config.hotelId;
    const currentPageId = config.pageId;
    const pageParentType = config.pageParentType;
    const pageParentGroupId = config.pageParentGroupId;
    let currentPageObj = {};
    list.forEach((page) => {
      if(currentPageId === String(page.pgid)){
        currentPageObj = {...page, isParent: true};
      }
      if(page.children && page.children.length){
        page.children.forEach((childPage) => {
          if (currentPageId === String(childPage.pgid)) {
            currentPageObj = {...childPage, isParent: false};
          }
        });
      }
    });

    if(!currentPageObj) return;

    const exitUrlMap = {
      [PAGE_PARENT.HOME]: `/simp/${hotelId}/homepage/`,
      [PAGE_PARENT.ROOMS]: currentPageObj.isParent
        ? `/simp/${hotelId}/rooms_group/${pageParentGroupId}`
        : `/simp/${hotelId}/room_page/${currentPageObj.pgid}`,
      [PAGE_PARENT.LOCATIONS]: `/simp/${hotelId}/location_page_settings/`,
      [PAGE_PARENT.GALLERY]: `/simp/${hotelId}/gallery_page_settings/`,
      [PAGE_PARENT.PROMOTIONS]: currentPageObj.isParent
        ? `/simp/${hotelId}/promotion_group/${pageParentGroupId}`
        : `/simp/${hotelId}/promotion/${currentPageObj.pgid}`,
      [PAGE_PARENT.CUSTOM_PAGE]: currentPageObj.isParent
        ? `/simp/${hotelId}/custom_page_group/${pageParentGroupId}`
        : `/simp/${hotelId}/custom_page/${currentPageObj.pgid}`,
    };

    const exitUrl = exitUrlMap[pageParentType];
    const exitButton = document.querySelector(".bottom-bar .btn-cancel");
    if(!exitUrl || !exitButton) return;
    const newExitButtonParent = exitButton.cloneNode(true);
    exitButton.parentNode.replaceChild(newExitButtonParent, exitButton);
    newExitButtonParent.addEventListener("click", function () {
      window.location.replace(exitUrl);
    });
  }

  function renderInternalNavMenu(navLinks){
    const navLinksWrapper = document.querySelector("#nav-links-wrapper");
    if(!navLinksWrapper || !navLinks || (navLinks && !navLinks.length)) return;
    navLinksWrapper.innerHTML = navLinks;

     function initNavSearch() {
       const searchInput = document.querySelector("#gjs-nav-search");
       if (!searchInput) return;
       searchInput.addEventListener("input", function (event) {
         const value = event.target.value || "";
         const navLinks = document.querySelectorAll(".nav-menu-wrapper a");
         navLinks.forEach((link) => {
           const includesText = link.textContent
             .toLowerCase()
             .includes(value.toLowerCase().trim());
           if (includesText) {
             link.style.display = "block";
           } else {
             link.style.display = "none";
           }
         });
       });
     }
     initNavSearch();
  }

  function initInternalPageNavMenu(){
    const navMenuHead = document.querySelector(".js-page-navigator");
    if (!navMenuHead) return;
    navMenuHead.innerHTML = `
      <span class="page-title">${config.hotelName} (${config.pageName})</span>
      <i class="fa fa-caret-down caret-down-icon"></i>
    `;
    const navMenuContainer = document.createElement('div');
    navMenuContainer.classList.add("internal-nav-container");
    navMenuContainer.classList.add("dropdown-menu");
    // Preventing menu from closing on clicking the white spaces
    navMenuContainer.addEventListener("click", event => event.stopPropagation());
    navMenuHead.addEventListener("click", handleCaretDownRotation);
    navMenuHead.parentElement.classList.add("dropdown");
    navMenuHead.parentNode.appendChild(navMenuContainer);
    navMenuHead.classList.add("dropdown-toggle");
    navMenuHead.dataset.toggle = "dropdown";

    navMenuContainer.innerHTML = `
          <div class="nav-menu-wrapper">
            <div class="search-bar-wrapper">
              <div class="search-bar">
                <i class="fas fa-search search-icon"></i>
                <input class="search-input" type="text" placeholder="Search Page" id="gjs-nav-search" />
              </div>
            </div>
            <div id="nav-links-wrapper"></div>
          </div>
    `;

    function handleCaretDownRotation() {
      const caretDownIcon = document.querySelector(".js-page-navigator .caret-down-icon");
      if (!caretDownIcon) return;
      if (navMenuHead.ariaExpanded === "true") {
        caretDownIcon.classList.add("expand");
      } else {
        caretDownIcon.classList.remove("expand");
      }
    }

    const gjsFrame = document.querySelector(".gjs-frame");
    if(gjsFrame){
      gjsFrame.contentWindow.document.body.onclick = function (event) {
        if (!navMenuContainer.contains(event.target) && navMenuHead.parentElement.classList.contains("open")) {
            navMenuHead.parentElement.classList.remove("open");
            navMenuHead.ariaExpanded = "false";
            handleCaretDownRotation();
        }
      };
    }
    document.addEventListener("click", function (event) {
      // Check if the clicked element is not the div itself or a descendant of the div
      if (!navMenuContainer.contains(event.target)) {
        handleCaretDownRotation();
      }
    });

    fetchAllPageAndRenderPageDropdown()
  }

  

  editor.on('load', function() {
    // Load and show settings and style manager
    var openTmBtn = pn.getButton('views', 'open-tm');
    openTmBtn && openTmBtn.set('active', 1);
    var openSm = pn.getButton('views', 'open-sm');
    openSm && openSm.set('active', 1);

    // Add Settings Sector
    var traitsSector = $('<div class="gjs-sm-sector gjs-sm-traits no-select">'+
    '<div class="gjs-sm-title"><i id="gjs-sm-caret" class="' + config.faIconType + ' fa-caret-right">' +
    '</i>&nbsp;<span class="icon-settings ' + config.faIconType + ' fa-cog"></span> Settings</div>' +
    '<div class="gjs-sm-properties" style="display: none;"></div></div>');
    var traitsProps = traitsSector.find('.gjs-sm-properties');
    traitsProps.append($('.gjs-trt-traits'));
    $('.gjs-sm-sectors').before(traitsSector);

    traitsSector.find('.gjs-sm-title').on('click', function(){
      var traitStyle = traitsProps.get(0).style;
      var hidden = traitStyle.display == 'none';
      if (hidden) {
        traitStyle.display = 'block';
        $(this).children().first().attr('class', config.faIconType + ' fa-caret-down');
      } else {
        traitStyle.display = 'none';
        $(this).children().first().attr('class', config.faIconType + ' fa-caret-right');
      }
    });

    initInternalPageNavMenu();
  });

  // On component change show the Style Manager
  config.showStylesOnChange && editor.on('component:selected', () => {
    const openSmBtn = pn.getButton('views', osm);
    const openLayersBtn = pn.getButton('views', ola);

    // Don't switch when the Layer Manager is on or
    // there is no selected component
    if ((!openLayersBtn || !openLayersBtn.get('active')) && editor.getSelected()) {
      openSmBtn && openSmBtn.set('active', 1);
    }
  });
  
  //Removing Devices buttons and Preview button from the panel
  if(!config.showOlderPhoenix){
    const removeDevicesPanel  = pn.removePanel('devices-c');
    const removePreviewButton = pn.removeButton('options', 'preview');
  }
}