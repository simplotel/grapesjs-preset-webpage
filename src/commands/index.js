import openImport from './openImport';
import {
    cmdImport,
    cmdDeviceDesktop,
    cmdDeviceTablet,
    cmdDeviceMobile,
    cmdDeviceMobileLandscape,
    cmdClear,
    cmdEditSortDelete,
    defaultAspectRatios,
    cmdCloneWidget,
    cmdOpenTextGroupConfig
} from './../consts';
import { textEditor } from './../components/TextEditor.js';
import { CreateCropModelUi, CreateSortableAndDeleteEditModelUi } from './modelData.js';
import { getHtmlElement } from '../utils/utils';
import {TEXT_GROUP_BLOCK, TEXT_GROUP_HEADING, generateCheckboxToggle, generatePlusMinusCounter } from "../utils/htmlHelpers";


/**
 * @param {String} - text
 * @param {String} - type
 *
 * It's a alert which will show the text with given type (success, error or wraning)
 * and it will disappear after 3 sec.
*/

function toggleAlert(text, type) {
    const ele = document.createElement('div');
    const style = 'position: fixed; top: 15px; left: 50%; transform: translateX(-50%); z-index: 9999;';
    ele.setAttribute('style', style);
    ele.setAttribute('class', `c-toast alert alert-${type ? type === 'error' ? 'danger' : type : 'success'}`);
    ele.setAttribute('role', 'alert');
    ele.innerHTML = text;
    document.body.appendChild(ele);

    setTimeout(() => {
        ele.remove();
    }, 3000);
}

function isValidUrl(url) {
    let pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(url);
}

/**
 * @param {String} - mobileUrl
 * @return {Object} - object with mobile ratio, width and height
*/

function findAspectRatio(url) {
    // let params = url.slice(url.indexOf('x_'), url.indexOf('r_')).split(',');
    // params.length = 2; // we need width and height only
    // let [width, height] = params.join().replace('w_', '').replace('h_', '').split(',');
    
    //extracting cropped dimensions from url along with original image dimensions in order to use them to calculate the cropped dimensions of the image. 
    let [width, height, , croppedWidth, croppedHeight] = url.slice(url.indexOf('w_')).split(',').join().replaceAll('w_', '').replaceAll('h_', '').split(',');
    height = height.slice(0, height.indexOf('/')); // eg: changes 2500/x_5000 to 2500.

    const finalWidth = croppedWidth ? croppedWidth : width,
          finalHeight = croppedHeight ? croppedHeight : height;
    
    // We are using cropped dimensions when present, these dimensions are directly added to x1 and y1 to get x2 and y2.
    return {
        ratio: +(finalWidth / finalHeight).toFixed(3),
        width: finalWidth,
        height: finalHeight
    };
}

/**
 * @param {String} - mobileUrl
 * @return {Object} - object with crop data of mobile
*/

function extractUrlData(url) {
    if (!url) {
        return null;
    }

    let params = url.slice(url.indexOf('x_')).split(',');
    params.length = 2;
    let [x, y] = params.join().replace('x_', '').replace('y_', '').split(',');

    let ratioDetails = findAspectRatio(url);
    return {
        x1: +x,
        y1: +y,
        x2: +x + +ratioDetails.width,
        y2: +y + +ratioDetails.height,
        ratio: ratioDetails.ratio,
        width: +ratioDetails.width,
        height: +ratioDetails.height
    };
}

/**
 * @param {Object} - comp (Grapesjs object) or selected element
 * Function will return new url
 * @return {String} - new url
*/

function constructNewUrl(config, comp, data = this.state.cropValues, isAsset = false) {

    const { x1, x2, y1, y2, width, height } = data;
    const ratio = this.state.selectedRatio ? this.state.selectedRatio : data.ratio;
    const orgUrl = data && data.orgUrl ? data.orgUrl : this.state.imgUrl;

    if (width === 0 || height === 0) {
        alert("Aspect ratio can't be 0!");
        return;
    }

    let originalImgUrl = orgUrl.split('/q_30/');
    const PRECISION_VALUE = this.PRECISION_VALUE;
    let cropParams = {
        params: `/x_${Math.round(x1)},y_${Math.round(y1)},w_${Math.round(width)},h_${Math.round(height)},r_0,c_crop`,
        cropedAspectRatio: +ratio ? parseFloat(ratio).toFixed(PRECISION_VALUE) : parseFloat(width / height).toFixed(PRECISION_VALUE),
    };
    let resizedParams = config.resizeImageParams(comp, cropParams);

    const DESKTOP_URL = `${originalImgUrl[0]}${resizedParams.dResizedparams}${originalImgUrl[originalImgUrl.length - 1]}`;
    const MOBILE_URL = `${originalImgUrl[0]}${resizedParams.mResizedparams}${originalImgUrl[originalImgUrl.length - 1]}`;
    if (isAsset) {
        return {
            desktop: DESKTOP_URL,
            tablet: DESKTOP_URL,
            mobile: MOBILE_URL,
        }
    }

    if (this.state.cropByDevice.device === 'desktop' || this.state.cropByDevice.device === 'tablet') {
        return DESKTOP_URL;
    }

    return MOBILE_URL;
}

/**
 * @param {Number} - imgAspect
 * This function will update data on Ui.
 * it will also calculate co-ordinates and will update it.
*/

function getOnSelectCallback(imgAspect, updateJcropApi, isCroppingEnable = true) {
    let attrs = isCroppingEnable ? this.data.selectedEle.getAttributes() : {};
    return (attrsParam) => {
        if (attrsParam) {
            attrs = attrsParam;
        }
        let imgWidth = parseInt(attrs['data-imgwidth']),
            imgHeight = parseInt(attrs['data-imgheight']),
            imgX = 0,
            imgY = 0,
            imgX2 = imgWidth,
            imgY2 = imgHeight,
            radius = 0,
            diff = 0;
        let height = imgWidth / imgAspect;

        if (height <= imgHeight) {
            diff = (imgHeight - height) / 2;
            imgX = 0;
            imgY = Math.round(diff);
            imgX2 = imgWidth;
            imgY2 = Math.round(height + diff);
        } else {
            let width = imgHeight * imgAspect;
            diff = (imgWidth - width) / 2;
            imgX = Math.round(diff);
            imgY = 0;
            imgX2 = Math.round(width + diff);
            imgY2 = imgHeight;
        }

        const newData = {
            x1: imgX,
            y1: imgY,
            x2: imgX2,
            y2: imgY2,
            width: imgWidth,
            height: imgHeight,
            ratio: imgAspect
        };

        if (isCroppingEnable) {
            this.state.selectedRatio = imgAspect;
            updateJcropApi(newData);
        }

        return newData;
    }
}

function InitCropping(edtior, config) {

    this.PRECISION_VALUE = 3;

    this.data = {
        selectedEle: editor.getSelected(),
        isWidgetCrop: "Image" === editor.getSelected().getName() ? false : true,
        title: "Image Crop",
        content: "",
        widgetCropEnabled: [
            "widget-heroimage-slider",
            "widget-image-slider",
            "widget-imagetext-slider",
            "widget-img-txt-link"
        ],
        isWidgetCroppingEnable: function () {
            const checkWidgetName = (ele) => {
                if (!ele) {
                    return false
                } else if (this.widgetCropEnabled.includes(ele.getName().toLowerCase())) {
                    return true
                }
                return checkWidgetName(ele.parent());
            }
            return checkWidgetName(editor.getSelected()) && !this.isWidgetCrop;
        },
        getWidget: function () {
            return this.isWidgetCrop ? editor.getSelected() : null;
        }
    }

    /**
     * @readonly {Object} - state
     * Used as a centralise store to maintain data flow
    */

    this.state = {
        isActive: true,
        imgUrl: "",
        cropApi: {},
        cropOptions: {},
        cropByDevice: {
            device: 'desktop',
            cropForAll: true,
        },
        selectedRatio: 1.777,
        cropValues: {
            x1: 0,
            y1: 0,
            x2: 0,
            y2: 0,
            width: 0,
            height: 0,
        },
        customValues: {
            width: {
                desktop: 0,
                tablet: 0,
                mobile: 0
            },
            height: {
                desktop: 0,
                tablet: 0,
                mobile: 0
            }
        },
        urls: {
            desktop: "",
            tablet: "",
            mobile: ""
        },
        mobileDimensions: {},
        tabletDimensions: {},
        desktopDimensions: {}
    };

    this.captionState = {
        isActive: false,
        caption: "",
        position: "center-center",
        imgLink: "",
        linkType: "caption",
        newWindow: false,
        default: {
            imgLink: ""
        }
    };

    /**
     * @param {String} - newUrl
     *
     * It will update new url to the state
    */
    const updateUrlToState = (newUrl) => {
        if (this.state.cropByDevice.cropForAll) {
            this.state.urls['desktop'] = newUrl;
            this.state.urls['tablet'] = newUrl;
            this.state.urls['mobile'] = newUrl;
        } else {
            const device = this.state.cropByDevice.device;
            this.state.urls[device] = newUrl;
        }
    }

    const getAllAttributes = (ele) => {
        const attrs = {};
        for (let obj of ele.attributes) {
            attrs[obj.nodeName] = obj.nodeValue;
        }
        return attrs;
    }

    const getCropDataOfImages = (ratio) => {
        let comp = this.data.getWidget();
        let allImages = comp.find('img');
        const state = this.state;

        const data = [];
        let newRatio = 0; // storing aspect ratio for manual cropping
        const newConstructNewUrl = constructNewUrl.bind(this);

        allImages.forEach((image, i) => {
            let attrs = getAllAttributes(image.getEl());
            let newValues = null;

            if (i === 0) { // considering cropped data for first image
                newRatio = +(state.cropValues.width / state.cropValues.height).toFixed(3);
                newValues = {
                    ...state.cropValues,
                    ratio: newRatio
                };
            } else {
                newValues = getOnSelectCallback.call(this, ratio ? ratio : newRatio, updateJcropApi)(attrs);
                newValues = {
                    ...newValues,
                    width: newValues.x2 - newValues.x1,
                    height: newValues.y2 - newValues.y1,
                };
            }
            data.push({
                ...newValues,
                image: image,
                url: newConstructNewUrl(config, image, { ...newValues, ratio: newRatio, orgUrl: attrs['data-orgimgsrc'] })
            });
        });

        return data;
    }

    const updateAttributes = (data) => {
        const { x1, y1, x2, y2, width, height, url, image } = data;
        let attrs = getAllAttributes(image.getEl());

        if (this.state.cropByDevice.device === 'desktop') {
            attrs['data-cropimgx'] = Math.round(x1);
            attrs['data-cropimgy'] = Math.round(y1);
            attrs['data-cropimgx2'] = Math.round(x2);
            attrs['data-cropimgy2'] = Math.round(y2);
            attrs['data-imgaspect'] = this.state.selectedRatio;
            attrs['src'] = url;
        } else if (this.state.cropByDevice.device === 'tablet') {
            attrs['data-tabaspect'] = this.state.selectedRatio;
            attrs['data-tabimgsrc'] = url;
        } else if (this.state.cropByDevice.device === 'mobile') {
            attrs['data-mobaspect'] = this.state.selectedRatio;
            attrs['data-mobimgsrc'] = url;
        }

        if (this.state.cropByDevice.cropForAll) {
            // for URL
            attrs['data-tabimgsrc'] = url;
            attrs['data-mobimgsrc'] = url;
            attrs['src'] = url;
            // for aspect ratio
            attrs['data-imgaspect'] = this.state.selectedRatio;
            attrs['data-tabaspect'] = this.state.selectedRatio;
            attrs['data-mobaspect'] = this.state.selectedRatio;
            // for custom width
            const desktopWidth = this.state.customValues.width['desktop'];
            const desktopHeight = this.state.customValues.height['desktop'];
            attrs['data-customwidth'] = desktopWidth ? `${desktopWidth},${desktopWidth},${desktopWidth}` : '';
            attrs['data-customheight'] = desktopHeight ? `${desktopHeight},${desktopHeight},${desktopHeight}` : '';
        } else {
            attrs['data-customwidth'] = `${this.state.customValues.width['desktop'] ? this.state.customValues.width['desktop'] : ''},${this.state.customValues.width['tablet'] ? this.state.customValues.width['tablet'] : ''},${this.state.customValues.width['mobile'] ? this.state.customValues.width['mobile'] : ''}`;
            attrs['data-customheight'] = `${this.state.customValues.height['desktop'] ? this.state.customValues.height['desktop'] : ''},${this.state.customValues.height['tablet'] ? this.state.customValues.height['tablet'] : ''},${this.state.customValues.height['mobile'] ? this.state.customValues.height['mobile'] : ''}`;
        }

        image.setAttributes(attrs);
        image.set('src', attrs['src']);
    }

    /**
     * Function will get new url and data, then it will store it on selected element
    */

    const handleSave = () => {
        let comp = this.data.selectedEle;

        if (this.data.isWidgetCrop) {
            let cropData = getCropDataOfImages(this.state.selectedRatio);
            cropData.forEach(data => {
                updateAttributes(data);
            });

            toggleAlert(`Cropping applied to all the images for ${this.state.cropByDevice.cropForAll ? 'all devices' : this.state.cropByDevice.device}`);
            editor.Modal.close();
            return;
        }

        let attrs = comp.getAttributes();

        // for caption part
        if (this.captionState.isActive) {
            const ckeditorInstance = this.captionState.ckeditorInstance ? this.captionState.ckeditorInstance : null;
            if (!this.captionState.imgLink) {
                if (!confirm("URL is empty, do you want to save?")) {
                    return;
                }
            } else if (this.captionState.imgLink.charAt(0) != '#' && !isValidUrl(this.captionState.imgLink)) {
                toggleAlert("Please use valid URL! It must contain http or https", "error");
                return;
            }

            if (ckeditorInstance) {
                const data = ckeditorInstance.getData();
                const captionState = this.captionState;
                comp.setAttributes({
                    ...attrs,
                    "data-imgcaption": data,
                    "data-captionpos": captionState.position,
                    "data-href": captionState.imgLink ? captionState.imgLink : "",
                    "data-linktype": captionState.linkType,
                    "data-newwindow": `${captionState.newWindow}`
                });

                toggleAlert(`Caption ${captionState.imgLink ? "and link" : ""} applied Successfully!`);
                ckeditorInstance.destroy(true);
            } else {
                toggleAlert("CKEditor not found!", 'error');
            }
            editor.Modal.close();
            return;
        }

        // for cropping part

        let { x1, x2, y1, y2, width, height } = this.state.cropValues;

        if (width === 0 || height === 0) {
            toggleAlert("Aspect ratio can't be 0!", 'danger');
            return;
        }

        if (this.state.cropByDevice.cropForAll) {
            if (!confirm('Cropping will apply for all devices')) {
                return;
            }
        }

        // It will create and update new urls for the device
        const newUrl = constructNewUrl.call(this, config, comp);
        updateUrlToState(newUrl);

        // Updating state with new cropping values
        const allDevices = ['desktop', 'tablet', 'mobile'];
        if (this.state.cropByDevice.cropForAll) {
            const state = this.state;
            allDevices.forEach(item => {
                state[item + 'Dimensions'] = {
                    ...state[item + 'Dimensions'],
                    x1: Math.round(x1),
                    y1: Math.round(y1),
                    x2: Math.round(x2),
                    y2: Math.round(y2),
                    ratio: state.selectedRatio
                }
            });

            attrs['data-tabaspect'] = state.selectedRatio;
            attrs['data-mobaspect'] = state.selectedRatio;
            attrs['data-imgaspect'] = state.selectedRatio;

        } else {
            const state = this.state;
            state[state.cropByDevice.device + 'Dimensions'] = {
                ...state[state.cropByDevice.device + 'Dimensions'],
                x1: Math.round(x1),
                y1: Math.round(y1),
                x2: Math.round(x2),
                y2: Math.round(y2),
                ratio: state.selectedRatio
            }
        }

        updateAttributes({ x1, x2, y1, y2, width, height, image: comp, url: newUrl });

        // Showing alert on success
        toggleAlert(`Cropping applied for ${this.state.cropByDevice.cropForAll ? 'all devices' : this.state.cropByDevice.device}`);

        // Closing modal if cropping is applied for all the devices
        if (this.state.cropByDevice.cropForAll) {
            editor.Modal.close();
        }
    }

    /**
     * @param {Object} - data
    */
    const updateJcropApi = (data) => {
        this.state.cropApi.setOptions({ 'aspectRatio': data.ratio });
        this.state.cropApi.setOptions({ 'trueSize': [data.width, data.height] });
        this.state.cropApi.setSelect([data.x1, data.y1, data.x2, data.y2]);
    }

    const setJcropApiObject = (objectValue) => {
        this.state.cropApi = objectValue;
    }

    /**
     * @param {Object} - attrs
     * @param {Object} - mobileDimensions
     *
     * Updating default state of the cropping modal
    */

    const udateInitialState = (attrs, mobileDimensions, tabletDimensions) => {

        this.state.imgUrl = attrs['data-orgimgsrc'];
        this.state.selectedRatio = +attrs['data-imgaspect'];
        this.state.cropValues = {
            x1: +attrs['data-cropimgx'],
            y1: +attrs['data-cropimgy'],
            x2: +attrs['data-cropimgx2'],
            y2: +attrs['data-cropimgy2'],
            width: +attrs['data-imgwidth'],
            height: +attrs['data-imgheight']
        };

        const customWidthArr = attrs['data-customwidth'] ? attrs['data-customwidth'].split(',') : [];
        const customHeightArr = attrs['data-customheight'] ? attrs['data-customheight'].split(',') : [];

        this.state.customValues = {
            width: customWidthArr.length
                ? {
                    desktop: customWidthArr[0],
                    tablet: customWidthArr[1],
                    mobile: customWidthArr[2]
                }
                : {},
            height: customHeightArr.length
                ? {
                    desktop: customHeightArr[0],
                    tablet: customHeightArr[1],
                    mobile: customHeightArr[2]
                }
                : {},
        }

        this.state.urls = {
            desktop: attrs['src'],
            tablet: attrs['data-tabimgsrc'],
            mobile: attrs['data-mobimgsrc']
        };

        this.state.mobileDimensions = {
            ...mobileDimensions,
            width: +attrs['data-imgwidth'],
            height: +attrs['data-imgheight'],
            ratio: attrs['data-mobaspect']
                ? +attrs['data-mobaspect']
                : mobileDimensions.ratio ? mobileDimensions.ratio : 1.777
        };

        this.state.tabletDimensions = {
            ...tabletDimensions,
            width: +attrs['data-imgwidth'],
            height: +attrs['data-imgheight'],
            ratio: attrs['data-tabaspect'] ? +attrs['data-tabaspect'] : 1.777
        };

        this.state.desktopDimensions = {
            ...this.state.cropValues,
            ratio: +attrs['data-imgaspect']
        };

    }

    const updateCaptionState = (attrs) => {
        this.captionState.caption = attrs["data-imgcaption"] ? attrs["data-imgcaption"] : "";

        // adding center-center as a default position for the caption
        this.captionState.position = attrs["data-captionpos"] ? attrs["data-captionpos"] : this.captionState.position;
        this.captionState.imgLink = attrs["data-href"] ? attrs["data-href"] : '';
        this.captionState.linkType = attrs["data-linktype"] ? attrs["data-linktype"] : "caption";
        this.captionState.newWindow = attrs["data-newwindow"] ? attrs["data-newwindow"] : false;

        this.captionState.default.imgLink = attrs["data-href"] ? attrs["data-href"] : '';
    }

    /**
     * Triggering actions on model open
    */

    this.run = () => {
        editor.Modal.onceOpen(() => {
            let comp = this.data.selectedEle;
            const imgEle = this.data.isWidgetCrop && comp.find('img')[0] ? comp.find('img')[0] : comp;
            this.data.selectedEle = imgEle;
            let attrs = imgEle.getAttributes();

            if (!attrs['data-orgimgsrc']) {
                editor.Modal.close();
                return;
            }

            /**
               * Updating default and current state for Mobile and Tablet
            */
            const mobileDimensions = extractUrlData(attrs['data-mobimgsrc']);
            const tabletDimensions = extractUrlData(attrs['data-tabimgsrc']);
            udateInitialState(attrs, mobileDimensions, tabletDimensions);
            updateCaptionState(attrs);

            const state = this.state;
            const captionState = this.captionState;
            const isWidgetCrop = this.data.isWidgetCrop;
            const isWidgetCroppingEnable = this.data.isWidgetCroppingEnable();
            const newGetOnSelectCallback = getOnSelectCallback.bind(this);

            // configuation options for croping
            let jcropOpts = {
                onSelect: function (c) {
                    let { w: width, h: height } = c;
                    width = Math.ceil(width) ? Math.ceil(width) : 0;
                    height = Math.ceil(height) ? Math.ceil(height) : 0;

                    state.cropValues.x1 = c.x;
                    state.cropValues.y1 = c.y;
                    state.cropValues.x2 = c.x2;
                    state.cropValues.y2 = c.y2;
                    state.cropValues.width = width;
                    state.cropValues.height = height;
                },
                trueSize: [state.cropValues.width, state.cropValues.height],
                bgColor: 'black',
                setSelect: [state.cropValues.x1, state.cropValues.y1, state.cropValues.x2, state.cropValues.y2],
                boxWidth: 1200,
                boxHeight: 370,
                aspectRatio: state.selectedRatio,
            };

            state.cropOptions = jcropOpts; // adding crop Options in state for later use

            // Creating an instance of new crop Ui
            let cropUi = new CreateCropModelUi();

            // getting Html code of the crop model from an instance of model.
            let modalHtml = cropUi.getHtml({
                updateJcropApi: updateJcropApi,
                getOnSelectCallback: newGetOnSelectCallback,
                modal: editor.Modal,
                handleSave: handleSave,
                state: state,
                isWidgetCrop: isWidgetCrop,
                isWidgetCropEnable: isWidgetCroppingEnable,
                captionState: captionState,
                setJcropApiObject: setJcropApiObject,
                initJcropElement: config.initJcropElement
            });

            /**
            * Appending crop model in modal
            */
            editor.Modal.setContent(modalHtml);

            // removing load message element from Crop model
            if (cropUi.publicActionEle.loadMsg !== null) {
                cropUi.publicActionEle.loadMsg.remove();
            }
            comp.removeClass('gjs-plh-image');

            // enabling Jcrop on image
            config.initJcropElement(cropUi.publicActionEle.imgEle, jcropOpts, setJcropApiObject);
        });

        editor.Modal.open({ title: this.data.title, content: this.data.content }).onceClose(function () {
            console.log('Image Crop modal closed');
        });
    }

}

const commands = (editor, config) => {
    const cm = editor.Commands;
    const txtConfirm = config.textCleanCanvas;

    cm.add(cmdImport, openImport(editor, config));
    cm.add(cmdDeviceDesktop, e => e.setDevice('Desktop'));
    cm.add(cmdDeviceTablet, e => e.setDevice('Tablet'));
    cm.add(cmdDeviceMobile, e => e.setDevice('Mobile portrait'));
    cm.add(cmdDeviceMobileLandscape, e => e.setDevice('Mobile landscape'));
    cm.add(cmdClear, e => confirm(txtConfirm) && e.runCommand('core:canvas-clear'));

    cm.add('save', (editor, sender, options = {}) => {
            // To update the HTML and CSS and JS data in session storage
            
            sessionStorage.setItem('defaultHTML', JSON.stringify(editor.getHtml()));
            sessionStorage.setItem('defaultCSS', JSON.stringify(editor.getCss()));
            sessionStorage.setItem('defaultJS', JSON.stringify(editor.getJs()));
            
            if (!saveInprogress) {
              $.ajax({
                url: config.saveApiUrl,
                type: "POST",
                data: {'html_data': editor.getHtml(), 'css_data': editor.getCss(), 'js_data': editor.getJs()},
                beforeSend: function (xhr, settings) {
                  saveInprogress = true;
  
                  if (config.csrftoken) {
                    xhr.setRequestHeader("X-CSRFToken", config.csrftoken);
                  }
                },
                success: function (data, textStatus, jqXHR) {
                  data = JSON.parse(data);
                  saveInprogress = false;
                  if (data.status) {
                    $.growl.notice({ title: "Status", message: "Content saved successfully!", size: "large", duration: 4800 });
                    if(options && options.onSaveComplete && typeof options.onSaveComplete === 'function'){
                        options.onSaveComplete();
                    }
                  } else {
                    $.growl.error({ message: "Sorry something went wrong, you might want to try again by refreshing the page!" });
                  }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                  $.growl.error({ message: "Sorry something went wrong, you might want to try again by refreshing the page!" });
                  saveInprogress = false;
                }
              });
            }
        }
    );

    // simplotel CMS customizations
    const openAssetsCommand = cm.get('open-assets');

    if (openAssetsCommand) {
        cm.add('open-assets', {
            run(editor, sender, opts = {}) {
                if (opts.target.attributes.type === "image") {
                    return this;
                }
                return openAssetsCommand.run(editor, sender, opts);
            }
        });
    }

    if (config.initJcropElement) {
        cm.add(config.imageEditCommandId, {
            run(editor, sender, opts = {}) {
                const croppingInstance = new InitCropping(editor, config);
                croppingInstance.run();
            }
        });
    }

    cm.add(cmdCloneWidget, {
        run(editor, sender, opts = {}){
            const modal = editor.Modal;
            const title = "Select the pages where you'd like the widget to be copied";
            let selectedPages = {};
            const pagesOrder = [];
            let cachedPagesData = {};

            function renderSelectedPagesReview(){
                const reviewContainer = document.querySelector("#clone-review");
                reviewContainer.style.display = 'block';
                reviewContainer.innerHTML = "";
                let pageCount = 0;
                pagesOrder.forEach((hotelId) => {
                    if(!selectedPages[hotelId]) return;
                    const hotel = selectedPages[hotelId];
                    const pageWrapperElement = getHtmlElement('div', {}, {style: 'margin-bottom: 20px;'});
                    const hotelTitleElement = getHtmlElement('div', {innerText: hotel.hotelName}, { class: 'hotel-name' });
                    pageWrapperElement.appendChild(hotelTitleElement);
                    const pages = (hotel && hotel.pages) || {};
                    const pagesKeys = Object.keys(pages);
                    const pageListElement = getHtmlElement("ul", {}, {});
                    pagesKeys.forEach((pageId) => {
                        const page = pages[pageId];
                        const childPages = (page && page.children) || {};
                        const childPagesKeys = Object.keys(childPages);
                        if (page.isSelected) {
                            const pageTitle = getHtmlElement('li', {innerText: page.pageText}, {});
                            pageListElement.appendChild(pageTitle);
                            pageCount += 1;
                        } else if(!page.isSelected && childPagesKeys.length) {
                            const pageTitle = getHtmlElement('li', {innerText: page.pageText}, { class: 'disabled' });
                            pageListElement.appendChild(pageTitle);
                        }
                        const childPageElement = getHtmlElement('ul', {}, {});
                        childPagesKeys.forEach((childPageId) => {
                            const childPage = childPages[childPageId];
                            if(childPage.isSelected){
                                const childPageTitle = getHtmlElement('li', {innerText: childPage.pageText}, {});
                                childPageElement.appendChild(childPageTitle);
                                pageCount += 1;
                                return;
                            }
                        });
                        if(childPageElement.childElementCount){
                            pageListElement.appendChild(childPageElement);
                        }
                    });
                    if(pageListElement.childElementCount){
                        pageWrapperElement.appendChild(pageListElement);
                        reviewContainer.appendChild(pageWrapperElement);
                    }
                });
                if(reviewContainer.childElementCount){
                    const text = `No of page(s) selected - ${pageCount}`;
                    const pagesCountElement = getHtmlElement('p', {innerText : text}, {});
                    reviewContainer.prepend(pagesCountElement);
                }
            }

            function onSelectPage(event, parentPageListItem) {
                event.stopPropagation();
                const checked = event.target.checked;
                const { hotelId, pageId, parentPageId, pageText, hotelName } = event.target.dataset;
                const data = (selectedPages && selectedPages[hotelId]) || {};
                const pagesData = (data && data.pages) || {};
                let unSelectedParentPageData = {};
                if(parentPageListItem && parentPageId){
                    const parentElement = parentPageListItem.querySelector(`input[data-page-id="${parentPageId}"]`);
                    unSelectedParentPageData = parentElement ? parentElement.dataset : {};
                }
                if(checked){
                    const currentPageData = {
                        parentPageId: parentPageId || null,
                        pageText,
                        pageId,
                        isSelected: true
                    };
                   if(!parentPageId){
                        const currentPageChildrenData = (pagesData && pagesData[pageId] && pagesData[pageId].children) || {};
                        selectedPages[hotelId] = {
                            hotelName,
                            hotelId,
                            pages: {
                                ...pagesData,
                                [pageId]: {
                                    ...currentPageData,
                                    children: {
                                        ...currentPageChildrenData
                                    }
                                }
                            }
                        };
                   } else {
                        let parentData = {};
                        const parentPageData = (pagesData && pagesData[parentPageId]) || {};
                        if(Object.keys(parentPageData).length){
                            parentData = parentPageData;
                        } else if(Object.keys(unSelectedParentPageData).length){
                            parentData = {
                                parentPageId: unSelectedParentPageData.parentPageId,
                                pageText: unSelectedParentPageData.pageText,
                                pageId: unSelectedParentPageData.pageId,
                                isSelected: false
                            }
                        }
                        const parentPageChildrenData = (parentPageData && parentPageData.children) || {};
                        selectedPages[hotelId] = {
                            hotelName,
                            hotelId,
                            pages: {
                                ...pagesData,
                                [parentPageId]: {
                                    ...parentData,
                                    children: {
                                        ...parentPageChildrenData,
                                        [pageId]: currentPageData
                                    }
                                }
                            }
                        };
                    }
                } else {
                    if(!parentPageId){
                        pagesData[pageId].isSelected = false;
                        
                    }else {
                        delete pagesData[parentPageId].children[pageId];
                    }
                }
                renderSelectedPagesReview();
            }

            /**
             * Recursively render a nested page list to the DOM.
             * @param {Array} pageList - The list of pages to render.
             * @param {Number} currentIndex - The current index in the pageList.
             * @param {HTMLUListElement} baseUList - The base HTML list element to append to.
             * @param {HTMLUListElement} parentPageList - The parent page's HTML list element.
             * @param {HTMLLIElement} parentPageListItem - The parent page's list item element.
             * @param {string | null} parentPageId - The ID of the parent page.
             * @param {string} hotelName - The name of the hotel.
            */
            function generatePageList(
                pageList,
                currentIndex,
                baseUList,
                parentPageList = null,
                parentPageListItem = null,
                parentPageId = null,
                hotelName
            ) {
                if (!pageList || currentIndex >= pageList.length) {
                    return;
                }

                const currentPageData = pageList[currentIndex];
                const currentPageListItem = getHtmlElement("li", {}, {});
                const currentPageLabel = getHtmlElement("label", { innerText: currentPageData.text }, {});
                const isChecked =
                  (
                    selectedPages[currentPageData.hotelId] &&
                    selectedPages[currentPageData.hotelId].pages &&
                    selectedPages[currentPageData.hotelId].pages[currentPageData.pgid] &&
                    selectedPages[currentPageData.hotelId].pages[currentPageData.pgid].isSelected) ||
                  (
                    parentPageId &&
                    selectedPages[currentPageData.hotelId] &&
                    selectedPages[currentPageData.hotelId].pages &&
                    selectedPages[currentPageData.hotelId].pages[parentPageId] &&
                    selectedPages[currentPageData.hotelId].pages[parentPageId].children &&
                    selectedPages[currentPageData.hotelId].pages[parentPageId].children[currentPageData.pgid] &&
                    selectedPages[currentPageData.hotelId].pages[parentPageId].children[currentPageData.pgid].isSelected
                  );
                const currentPageCheckboxAttrs = {
                    "type": "checkbox",
                    "data-hotel-id": currentPageData.hotelId,
                    "data-page-id": currentPageData.pgid,
                    "data-page-text": currentPageData.text,
                    "data-hotel-name": hotelName,
                };
                if (parentPageId) {
                    currentPageCheckboxAttrs["data-parent-page-id"] = parentPageId;
                }
                const currentPageCheckbox = getHtmlElement("input", {
                        onchange: (event) => onSelectPage(event, parentPageListItem),
                        checked: Boolean(isChecked)
                    }, currentPageCheckboxAttrs);
                currentPageLabel.prepend(currentPageCheckbox);
                currentPageListItem.appendChild(currentPageLabel);

                // If the current page has sub-pages, recursively generate nested pages
                if (currentPageData.children) {
                    const nestedPageList = getHtmlElement("ul", {}, {});
                    generatePageList(
                        currentPageData.children,
                        0,
                        baseUList,
                        nestedPageList,
                        currentPageListItem,
                        currentPageData.pgid,
                        hotelName
                    );
                    currentPageListItem.appendChild(nestedPageList);
                }
                if (parentPageList) {
                    parentPageList.appendChild(currentPageListItem);
                } else {
                    baseUList.appendChild(currentPageListItem);
                }
                generatePageList(
                    pageList,
                    currentIndex + 1,
                    baseUList,
                    parentPageList,
                    parentPageListItem,
                    parentPageId,
                    hotelName
                );
            }


            /**
             * Renders a list of pages in a specified container element.
             *
             * @param {HTMLElement} pagesContainerElement - The HTML element in which the pages list will be rendered.
             * @param {Array} pagesList - An array of page data objects to render.
             * @param {string} hotelName - The name of the hotel associated with the pages.
             */
            function renderPageList(pagesContainerElement, pagesList = [], hotelName){
                const pagesListElement = getHtmlElement('ul', {}, { class: "pages-list" });
                generatePageList(pagesList, 0, pagesListElement, null, null, null, hotelName);
                pagesContainerElement.innerHTML = "";
                pagesContainerElement.appendChild(pagesListElement);
            }

            function renderPageListLoader(pagesContainerElement){
                if(!pagesContainerElement) return;
                const list = getHtmlElement('ul', {}, {class: 'skeleton-page-list'});
                const NO_OF_LIST_ITEMS = 10;
                const NO_OF_NESTED_LIST_ITEMS = 3;
                const APPEND_NESTED_LIST_INTERVAL = 3;
                const LIST_ITEM_TITLE_WIDTH = 40;
                for(let i=0; i<NO_OF_LIST_ITEMS-1; i++){
                    const listItem = getHtmlElement('li', {}, {class: 'skeleton-page-item skeleton-page'});
                    const skeletonCheckbox = getHtmlElement('div', {}, {class: 'skeleton-checkbox shimmer-skeleton'});
                    const skeletonLabel = getHtmlElement("div", {}, {
                        class: "skeleton-label shimmer-skeleton",
                        style: `width: ${((1 + (i % APPEND_NESTED_LIST_INTERVAL)) * LIST_ITEM_TITLE_WIDTH)}px`,
                    });
                    listItem.appendChild(skeletonCheckbox);
                    listItem.appendChild(skeletonLabel);
                    if(i>0 && i%APPEND_NESTED_LIST_INTERVAL === 0){
                        const nestedList = getHtmlElement('ul', {}, {class: 'skeleton-page-list'});
                        for(let j=0; j<NO_OF_NESTED_LIST_ITEMS-1; j++){
                            const nestedListItem = getHtmlElement('li', {}, {class: 'skeleton-page-item skeleton-page'});
                            const nestedSkeletonCheckbox = getHtmlElement('div', {}, {class: 'skeleton-checkbox shimmer-skeleton'});
                            const nestedSkeletonLabel = getHtmlElement("div", {}, {
                                class: "skeleton-label shimmer-skeleton",
                                style: `width: ${(1 + j) * LIST_ITEM_TITLE_WIDTH}px`,
                            });
                            nestedListItem.appendChild(nestedSkeletonCheckbox);
                            nestedListItem.appendChild(nestedSkeletonLabel);
                            nestedList.appendChild(nestedListItem);
                        }
                        list.appendChild(nestedList);
                    }
                    list.appendChild(listItem);
                }
                pagesContainerElement.innerHTML = "";
                pagesContainerElement.appendChild(list);
            }

            function fetchAndRenderPageList(hotelId, pagesContainerElement, hotelName){
                if(!hotelId) return;

                if (
                  cachedPagesData &&
                  cachedPagesData[hotelId] &&
                  cachedPagesData[hotelId].length
                ) {
                  renderPageList(pagesContainerElement, cachedPagesData[hotelId], hotelName);
                  return;
                }

                $.ajax({
                    url: `/api/v1/hotel/${hotelId}/get_page_list`,
                    type: "GET",
                    beforeSend: function (xhr, settings) {
                        if (config.csrftoken) {
                            xhr.setRequestHeader("X-CSRFToken", config.csrftoken);
                        }
                        renderPageListLoader(pagesContainerElement);
                    },
                    success: function (data, textStatus, jqXHR) {
                        const list = (data && data.data) || [];
                        // Store page list in cachedPagesData to avoid API calls
                        cachedPagesData[hotelId] = list;
                        renderPageList(pagesContainerElement, list, hotelName);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $.growl.error({
                        message:
                            "Sorry something went wrong, you might want to try again by refreshing the page!",
                        });
                    },
                });
            }

            function onExpandHotel(parentElement, hotelName) {
                const dataSet = parentElement.dataset;
                const hotelId = dataSet.hotelId;
                const isExpanded = dataSet.isExpanded === "true";
                const pagesContainer = parentElement.querySelector(".pages-container");
                pagesContainer.style.display = !isExpanded ? "block" : "none";
                dataSet.isExpanded = !isExpanded;
                const expandIcon = parentElement.querySelector("i");
                expandIcon.classList.toggle("expand");
                fetchAndRenderPageList(hotelId, pagesContainer, hotelName);
            }

            function renderHotelListUtil(hotelList, hotelListElement){
                if(!hotelList || (hotelList && !hotelList.length)) return;
                hotelList.forEach((hotel) => {
                    pagesOrder.push(hotel.id);
                    const wrapper = getHtmlElement('li', {}, { class: 'hotel-wrapper' });
                    const labelAndCheckBoxWrapper = getHtmlElement('div', {}, {class: 'hotel-label-wrapper'})
                    const labelAndPagesContainer = getHtmlElement("div", {}, {
                        "data-hotel-id": hotel.id,
                        "data-is-expanded": false,
                    });
                    const hotelLabel = getHtmlElement(
                        "label",
                        {
                            innerText: hotel.text,
                            onclick: () => onExpandHotel(labelAndPagesContainer, hotel.text),
                        },
                        {
                            class: "hotel-label",
                        }
                    );
                    const expandIcon = getHtmlElement('i', {}, { class: 'fa fa-caret-right expandable-icon' });
                    const pagesContainer = getHtmlElement('div', {}, { class: 'pages-container', style: 'display: none' });
                    hotelLabel.prepend(expandIcon);
                    labelAndPagesContainer.appendChild(hotelLabel);
                    labelAndPagesContainer.appendChild(pagesContainer);
                    labelAndCheckBoxWrapper.appendChild(labelAndPagesContainer);
                    wrapper.appendChild(labelAndCheckBoxWrapper);
                    hotelListElement.appendChild(wrapper);
                    if(hotel.children && hotel.children.length){
                        const childHotelListElement = getHtmlElement('ul', {}, { class: 'hotel-child-list' });
                        renderHotelListUtil(hotel.children, childHotelListElement);
                        wrapper.appendChild(childHotelListElement);
                    }
                });
            }

            function renderHotelList(allHotels){
                const container = document.querySelector('#clone-on-different-hotel');
                if(!container || !allHotels || (allHotels && !allHotels.length)) return;
                container.style.gridColumn = 'span 1';
                container.innerHTML = "";
                const hotelListElement = getHtmlElement('ul', {}, { class: 'hotel-list' });
                renderHotelListUtil(allHotels, hotelListElement);
                container.appendChild(hotelListElement);
                const hotelId = config.hotelId;
                const currentHotelElement = container.querySelector(`div[data-hotel-id="${hotelId}"] > label`);
                if(currentHotelElement){
                    currentHotelElement.click();
                }
            }

            function renderHotelListLoader(){
                const container = document.querySelector('#clone-on-different-hotel');
                if(!container) return;
                const loadingContainer = getHtmlElement('div', {}, { class: 'hotel-list-loading-container' });
                const spinner = getHtmlElement('i', {}, {class: 'fa fa-refresh spinner'});
                const loadingText = getHtmlElement('span', {innerText: 'Fetching Hotels...'}, {});
                loadingContainer.appendChild(spinner);
                loadingContainer.appendChild(loadingText);
                container.style.gridColumn = 'span 2';
                container.appendChild(loadingContainer);
            }
       
            function fetchAndRenderPhoenixHotelList(){
                const selectedComponent = editor.getSelected();
                const componentAttributes = selectedComponent.getAttributes();
                const widgetVariantId =
                    componentAttributes['data-simpwidget-variant'] ||
                    componentAttributes['data-simp-type-variant'] ||
                    componentAttributes['data-simp-container-widget-variant'];
                const hotelId = config.hotelId;

                if(!widgetVariantId || !hotelId) return;

                $.ajax({
                    url: `/cms/${hotelId}/hotellist/${widgetVariantId}?all_hotels=True`,
                    type: "GET",
                    beforeSend: function (xhr, settings) {
                        if (config.csrftoken) {
                            xhr.setRequestHeader("X-CSRFToken", config.csrftoken);
                        }
                        renderHotelListLoader();
                    },
                    success: function (data, textStatus, jqXHR) {
                        const list = (data && data.data) || [];
                        let foundHotel = false;
                        // Find index of current hotel and move it to 1st place in array
                        for(let i=0; i<list.length; i++){
                            const hotel = list[i];
                            if(hotel && hotel.id === hotelId){
                                [list[0], list[i]] = [list[i], list[0]];
                                break;
                            }
                            if(hotel.children && hotel.children.length){
                                const childHotels = hotel.children;
                                for(let j=0; j<childHotels.length; j++){
                                    if(childHotels[j].id === hotelId){
                                        [childHotels[0], childHotels[j]] = [childHotels[j], childHotels[0]];
                                        [list[0], list[i]] = [list[i], list[0]];
                                        foundHotel = true;
                                        break;
                                    }
                                }
                            }
                            if(foundHotel) break;
                        }
                        renderHotelList(list);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $.growl.error({
                        message:
                            "Sorry something went wrong, you might want to try again by refreshing the page!",
                        });
                    },
                });
            }


             function cloneOnSelectedPage(saveButton){
                const activeHotelId = config.hotelId;
                const activePageId = config.pageId;
                const selectedComponent = editor.getSelected();
                const componentAttributes = selectedComponent.getAttributes();
                const widgetVariantId =
                    componentAttributes["data-simpwidget-variant"] ||
                    componentAttributes["data-simp-type-variant"] ||
                    componentAttributes["data-simp-container-widget-variant"];
                const widgetId = componentAttributes.id;
                
                if (!widgetId || !widgetVariantId) {
                    return;
                }
                
                const pagesData = [];
                let indexOfCurrentHotel = -1;
                Object.keys(selectedPages).forEach((hotelId) => {
                    const hotelSelectedPages = [];
                    const pages = (selectedPages[hotelId] && selectedPages[hotelId].pages) || {};
                    Object.keys(pages).forEach((pageId) => {
                        if(pages[pageId] && pages[pageId].isSelected){
                            hotelSelectedPages.push(pageId);
                        }
                        const childPages = (pages[pageId] && pages[pageId].children) || {};
                        Object.keys(childPages).forEach((childPageId) => {
                            if(childPages[childPageId] && childPages[childPageId].isSelected){
                                hotelSelectedPages.push(childPageId);
                            }
                        });
                    });
                    if(hotelSelectedPages.length){
                        pagesData.push({
                            hotel_id: hotelId,
                            pages: hotelSelectedPages,
                        });
                        indexOfCurrentHotel = activeHotelId === hotelId ? pagesData.length - 1 : indexOfCurrentHotel;
                    }
                });

                if (!pagesData.length) {
                    return;
                }
                // Updating clone button text and disabling it
                saveButton.innerText = "Copying...";
                saveButton.disabled = true;

                // Logic to find widget id and clone widget on the same page
                if(indexOfCurrentHotel !== -1){
                    let hasCloneWidgetToCurrentPage = false;
                    const currentHotelPages = (
                        pagesData[indexOfCurrentHotel] &&
                        pagesData[indexOfCurrentHotel].pages) || [];
                    const updatedHotelPagesData = [];
                    currentHotelPages.forEach((pageId) => {
                        if(pageId === activePageId){
                            editor.runCommand("tlb-clone");
                            hasCloneWidgetToCurrentPage = true;
                        } else {
                            updatedHotelPagesData.push(pageId);
                        }
                    });
                    if(updatedHotelPagesData.length){
                        pagesData[indexOfCurrentHotel] = {
                          hotel_id: activeHotelId,
                          pages: updatedHotelPagesData,
                        };
                    } else if(pagesData.length === 1 && !updatedHotelPagesData.length && hasCloneWidgetToCurrentPage) {
                        saveButton.disabled = true;
                        saveButton.innerText = "Create Copy";
                        modal.close();
                        return;
                    }
                }

                const payload = {
                    widget_id: widgetId,
                    widget_variant_id: widgetVariantId,
                    hotels: pagesData,
                };
                if(!activeHotelId || !activePageId) return;
                $.ajax({
                    url: `/cms/${activeHotelId}/clonewidget/${activePageId}`,
                    contentType: "application/json; charset=utf-8",
                    type: "POST",
                    data: JSON.stringify({data: payload}),
                    beforeSend: function (xhr, settings) {
                        if (config.csrftoken) {
                        xhr.setRequestHeader("X-CSRFToken", config.csrftoken);
                        }
                    },
                    success: function (data, textStatus, jqXHR) {
                        saveButton.disabled = true;
                        saveButton.innerText = "Create Copy";
                        modal.close();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $.growl.error({
                        message:
                            "Sorry something went wrong, you might want to try again by refreshing the page!",
                        });
                    },
                });
            }

            function handleSave(event){
                const saveButton = event.target;
                editor.runCommand('save', {
                    onSaveComplete: () => {
                        cloneOnSelectedPage(saveButton);
                    }
                });
            }

            function getContent(){
                const wrapper = getHtmlElement('div', {}, {});
                const root = getHtmlElement("div", {}, { id: "clone-widget" });
                const pfx = editor.getConfig("stylePrefix");
                const actionButtonWrapper = getHtmlElement("div", {}, {class: "action-buttons-wrapper"})
                const cancelButton = getHtmlElement(
                    "button",
                    {
                        innerHTML: "Cancel",
                        onclick: () => modal.close(),
                    },
                    {
                        class: "btn btn-default"
                    }
                );
                const actionButton = getHtmlElement(
                    "button",
                    {
                        innerHTML: "Create Copy",
                        onclick: handleSave,
                    },
                    {
                        class: `btn btn-primary`,
                    }
                );
                actionButtonWrapper.appendChild(cancelButton);
                actionButtonWrapper.appendChild(actionButton);
                const hotelListElement = getHtmlElement('div', {}, {id: 'clone-on-different-hotel'});
                const reviewContainer = getHtmlElement('div', {}, {id: 'clone-review', style: 'display: none'});
                root.appendChild(hotelListElement);
                root.appendChild(reviewContainer);
                wrapper.appendChild(root);
                wrapper.appendChild(actionButtonWrapper);
                return wrapper;
            }

            const content = getContent();
            modal.onceOpen(()=>{
                  fetchAndRenderPhoenixHotelList();
            });
            modal.open({ title, content }).onceClose(function () {
                cachedPagesData = {};
                selectedPages = {};
            });

            return this;
        }
    });

    cm.add('show-widget-variants', {
        run(editor, sender, opts = {}) {
            const modal = editor.Modal;
            const dataIdName = 'variantid';

            var title = 'Select Variant', content = '<div style="text-align: center;padding: 20px;min-height: 180px;"><i class="fa fa-refresh fa-spin fa-3x fa-fw" aria-hidden="true"></i></div>';

            function handleSave() {
                var object = modal.getContent();
                var comp = editor.getSelected();
                var attrs = comp.getAttributes();
                var currentVariantId = '0';
                var selectedVariantId = '0';
                var widgetCategory = 0;

                if (attrs['data-simpwidget']) {
                    currentVariantId = attrs['data-simpwidget-variant'] || '0';
                    widgetCategory = 1;
                } else if (attrs['data-simp-container-widget']) {
                    currentVariantId = attrs['data-simp-container-widget-variant'] || '0';
                    widgetCategory = 3;
                } else {
                    currentVariantId = attrs['data-simp-type-variant'] || '0';
                }

                if (object) {
                    let radio_obj = object.querySelector('input[type="radio"]:checked');
                    if (radio_obj) {
                        selectedVariantId = radio_obj.dataset['' + dataIdName];
                    }
                }

                if (currentVariantId && currentVariantId !== '0' && selectedVariantId && selectedVariantId !== '0') {
                    $.ajax({
                        url: config.WidgetVariantHtmlDataUrl,
                        type: "POST",
                        data: { 'html_data': comp.toHTML(), 'selected_variant_id': selectedVariantId, 'widget_category': widgetCategory },
                        beforeSend: function (xhr, settings) {
                            if (config.csrftoken) {
                                xhr.setRequestHeader("X-CSRFToken", config.csrftoken);
                            }
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (data.html_data) {
                                var newcomp = comp.replaceWith(data.html_data);
                                editor.select(newcomp);
                            } else {
                                $.growl.error({ message: "Sorry something went wrong, you might want to try again by refreshing the page!" });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $.growl.error({ message: "Sorry something went wrong, you might want to try again by refreshing the page!" });
                        }
                    });

                }
                modal.close();
            }

            function getHtmlElement(name, props, attrs) {
                var innerItem = document.createElement(name);
                for (var key in props) {
                    innerItem[key] = props[key];
                }
                for (var key in attrs) {
                    innerItem.setAttribute(key, attrs[key]);
                }
                return innerItem;
            }

            function createVariantList(widget_variants, widgetVariantId) {
                let list = getHtmlElement('ul', {}, { style: "list-style: none; padding-left: 0px; max-width: 500px; min-width: 30%;" });
                widget_variants.forEach(item => {
                    var attrs = {
                        type: 'radio',
                        name: 'name' + dataIdName,
                        style: 'margin-top: 0;'
                    };
                    attrs[`data-${dataIdName}`] = item.id;

                    if (item.id.toString() === widgetVariantId) {
                        attrs['checked'] = true;
                    }

                    let listStyle = "border: 1px solid #a8a8a850; margin: 5px 0px; padding: 2px 10px 2px 10px; border-radius: 5px;";
                    let listItem = getHtmlElement('li', {}, { style: listStyle });

                    let labelStyle = "width: 100%; display: flex; align-items: center; margin-bottom: 0px;";
                    let label = getHtmlElement('label', {}, { style: labelStyle });
                    let inputBox = getHtmlElement('input', {}, attrs);
                    label.appendChild(inputBox);
                    label.appendChild(getHtmlElement('span', { innerText: item.name }, { style: "padding-left: 10px; font-weight: normal;" }));

                    listItem.appendChild(label);
                    list.appendChild(listItem);
                });
                return list;
            }

            modal.onceOpen(function () {
                var comp = editor.getSelected();
                var attrs = comp.getAttributes();
                var widgetId = attrs['data-simp-type'];
                var widgetVariantId = attrs['data-simp-type-variant'];
                var model = this.model;

                if (widgetId === undefined) {
                    widgetId = attrs['data-simpwidget'];
                    widgetVariantId = attrs['data-simpwidget-variant'];
                }

                if (widgetId === undefined) {
                    widgetId = attrs['data-simp-container-widget'];
                    widgetVariantId = attrs['data-simp-container-widget-variant'];
                }

                $.ajax({
                    url: config.cmsBaseUrl + 'widget-variants/' + widgetId,
                    type: "GET",
                    beforeSend: function (xhr, settings) {
                        if (!csrfSafeMethod(settings.type)) {
                            xhr.setRequestHeader("X-CSRFToken", csrftoken);
                        }
                    },
                    success: function (data, textStatus, jqXHR) {
                        let modalHtmlContent = getHtmlElement('div', {}, { id: 'variantList' });

                        var attr = {
                            class: 'form-control custom-width-input search-variant-list',
                            placeholder: 'Search variant name',
                            name: 'search',
                        }
                        let searchStyle = "margin: 10px auto; width: 30%;";
                        let search = getHtmlElement('input', {}, { style: searchStyle, ...attr });
                        modalHtmlContent.appendChild(search);

                        let innerContainer = getHtmlElement('div', {}, { style: "display: flex; max-height: 450px; overflow-y: auto; margin-bottom: 20px;" });
                        var dataId = widgetId;

                        const pfx = editor.getConfig('stylePrefix');
                        innerContainer.appendChild(createVariantList(data.widget_variants, widgetVariantId));

                        modalHtmlContent.appendChild(innerContainer);
                        modalHtmlContent.appendChild(getHtmlElement('button', { 'innerHTML': 'OK', 'className': `${pfx}btn-prim ${pfx}btn-add__file-attachment`, 'onclick': handleSave }, { style: 'float: right;' }));
                        modal.setContent(modalHtmlContent);

                        const variantList = document.querySelector('#variantList');
                        const searchVariant = document.querySelector('.search-variant-list');
                        searchVariant.addEventListener('keyup', function () {
                            filterVariantByName(this.value, variantList);
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $.growl.error({ message: "Sorry something went wrong, you might want to try again by refreshing the page!" });
                    }
                });
            });

            function debounce(fn, delay = 150) {
                let timer = null;
                return (...args) => {
                    clearTimeout(timer);
                    timer = setTimeout(() => {
                        fn(...args);
                    }, delay)
                }
            }

            const filterVariantByName = debounce((inputVal, variantList) => {
                const newInputVal = inputVal.trim().toUpperCase();
                variantList.querySelectorAll('li').forEach((item, i) => {
                    if (item.querySelector('label input + span').innerText.toUpperCase().includes(newInputVal)) {
                        item.style.display = "";
                    } else {
                        item.style.display = "none";
                    }
                });
            });

            modal.open({ title, content }).onceClose(function () {
                console.log('Widget Variants modal closed');
            });

            return this;
        }
    });

    cm.add('vAddHeadText', {
        run(editor, sender, opts = {}) {
            textEditor(editor, config)(editor.Modal, 'variable-head', "Text Editor for Heading of Variable Slider");
        }
    });

    cm.add(cmdOpenTextGroupConfig, {
        run(editor, sender, opts = {}) {
           
            const modal = editor.Modal;
            const title = "Text Widget Configuration";
            const errorMessage = "Double click here to choose a text group";
            let userConsent = false;

            // modal, model, htmlDataUrl, title, errorMessage,
            var content = '<div style="text-align: center;padding: 20px;min-height: 180px;"><i class="fa fa-refresh fa-spin fa-3x fa-fw" aria-hidden="true"></i></div>';
            function getHtmlElement(name, props, attrs) {
                var innerItem = document.createElement(name);
                for (var key in props) {
                    innerItem[key] = props[key];
                }
                for (var key in attrs) {
                    innerItem.setAttribute(key, attrs[key]);
                }
                return innerItem;
            }
    
            var comp = editor.getSelected();
            var attrs = comp.getAttributes();
            
            let noOfGroupOld = parseInt(attrs[`data-noofgroup`], 10);
            let noOfBlockInGroupOld = parseInt(attrs[`data-noofblockingroup`], 10);
            let headingOld = attrs[`data-heading`] === 'true';

            function handleClose() {
                modal.close();
            }

            function handleSave() {
                let noOfGroup = parseInt($('#noOfGroup').val(), 10);
                let noOfBlockInGroup = parseInt($('#noOfBlockInGroup').val(), 10);
                let heading = $('#heading').is(':checked');
                if(!noOfGroup) {
                    const pmErrorMsg = document.querySelector(`#errorMsg-noOfGroup`);
                    pmErrorMsg.innerText = "This field should not be empty.";
                    setTimeout(function(){
                        pmErrorMsg.innerText = '';
                    },5000);
                    return;
                } else if(!noOfBlockInGroup) {
                    const pmErrorMsg = document.querySelector(`#errorMsg-noOfBlockInGroup`);
                    pmErrorMsg.innerText = "This field should not be empty.";
                    setTimeout(function(){
                        pmErrorMsg.innerText = '';
                    },5000);
                    return;
                }
                console.log(typeof(noOfGroup), noOfBlockInGroup, heading);
                attrs[`data-noofgroup`] = noOfGroup;
                attrs[`data-noofblockingroup`] = noOfBlockInGroup;
                attrs[`data-heading`] = String(Boolean(heading));
                comp.setAttributes(attrs);

                const selComp = editor.getSelected();
                const titleBlock = selComp.find(".title-block");
                const contentBlockArray = selComp.find(".content-block");
                if(contentBlockArray && contentBlockArray.length){
                    const contentBlock = contentBlockArray[0];
                    if(noOfGroup > noOfGroupOld){
                        const noOfGroupsToAdd = noOfGroup - noOfGroupOld;
                        const textBlocks = contentBlock.find(".text-content");
                        if(textBlocks && textBlocks.length){
                            for(let i = 0; i < noOfGroupsToAdd; i++){
                                const lastClonedTextBlock = textBlocks[textBlocks.length - 1].clone();
                                contentBlock.append(lastClonedTextBlock);
                            }
                        }
                    }else if (noOfGroup < noOfGroupOld) {
                      userConsent = confirm(
                        "You have removed certain text elements. If you save, you will lose the data in these elements. If you do not wish to save, please change the number of elements or hit cancel."
                      );
                      if (!userConsent) return;
                      const noOfGroupsToRemove = noOfGroupOld - noOfGroup;
                      const groupsArray = contentBlock.find(".text-content");
                      let groupsLength = groupsArray.length;
                      for (let i = 0; i < noOfGroupsToRemove; i++) {
                        groupsArray[groupsLength - 1].remove();
                        groupsLength -= 1;
                      }
                    }

                    if(noOfBlockInGroup > noOfBlockInGroupOld){
                        const noOfBlocksToAdd = noOfBlockInGroup - noOfBlockInGroupOld;
                        const groupsArray = contentBlock.find(".text-content");
                        for(let i = 0; i < groupsArray.length; i++){
                            const group = groupsArray[i];
                            group.append(TEXT_GROUP_BLOCK.repeat(noOfBlocksToAdd));
                        }
                    }
                    else if(noOfBlockInGroup < noOfBlockInGroupOld) {
                        if(!userConsent && !confirm('You have removed certain text elements. If you save, you will lose the data in these elements. If you do not wish to save, please change the number of elements or hit cancel.')) {
                            return;
                        };
                        const noOfBlocksToRemove = noOfBlockInGroupOld - noOfBlockInGroup;
                        const groupsArray = contentBlock.find(".text-content");
                        for(let i = 0; i < groupsArray.length; i++){
                            const group = groupsArray[i];
                            const textContent = group.find('.text-content-wrapper');
                            let textContentLength = textContent.length;
                            for(let j = 0; j < noOfBlocksToRemove; j++){
                                textContent[textContentLength - 1].remove();
                                textContentLength -= 1;
                            }
                        }
                    }
                }
                if (heading && titleBlock && titleBlock.length === 0) {
                    selComp.append(TEXT_GROUP_HEADING, { at: 0 });
                } else if(!heading && titleBlock && titleBlock.length) {
                    titleBlock[0].remove();
                }
                modal.close();
            }
    
            function increment(element, max, pmErrorMsg) {
                let count = parseInt(element.value);
                if(isNaN(count)){
                    element.value = 1;
                    return;
                }
                if (count < max) {
                    count++;
                    element.value = count;
                } else {
                    pmErrorMsg.innerText = `Max value should be ${max}`;
                    setTimeout(function(){
                        pmErrorMsg.innerText = '';
                    },5000);
                }
            }
                
            function decrement(element, min, pmErrorMsg) {
                let count = parseInt(element.value);
                if (count > min) {
                    count--;
                    element.value = count;
                } else {
                    pmErrorMsg.innerText = `Min value should be ${min}`;
                    setTimeout(function(){
                        pmErrorMsg.innerText = '';
                    },5000);
                }
            }
                
            function validateCount(input, min, max, pmErrorMsg) {
                let count = parseInt(input.value);
                if (isNaN(count)) {
                    input.value = '';
                } else if (count < min) {
                    pmErrorMsg.innerText = `Min value should be ${min}`;
                    setTimeout(function(){
                        pmErrorMsg.innerText = '';
                    },5000);
                    input.value = min;
                } else if (count > max) {
                    pmErrorMsg.innerText = `Max value should be ${max}`;
                    setTimeout(function(){
                        pmErrorMsg.innerText = '';
                    },5000);
                    input.value = max;
                }
            }
    
            function hydratePlusMinusCounter(min, max, id){
                const minusBtn = document.querySelector(`#minus-${id}`);
                const plusBtn = document.querySelector(`#plus-${id}`);
                const counterInput = document.querySelector(`#${id}`);
                const pmErrorMsg = document.querySelector(`#errorMsg-${id}`);
                minusBtn.addEventListener('click', ()=>decrement(counterInput, min, pmErrorMsg));
                plusBtn.addEventListener('click', ()=>increment(counterInput, max, pmErrorMsg));
                counterInput.addEventListener('input', ()=>validateCount(counterInput, min, max, pmErrorMsg));
            }
    
            function checkBoolean(value) {
                if(typeof value === 'string'){
                    return value.toLowerCase() === 'true';
                }
                return value;
            }
    
            function hydrateHeading(id,checked) {
                const heading = document.querySelector(`#${id}`);
                heading.checked = checkBoolean(checked);
            }
    
            modal.onceOpen(function (event) {
                const comp = editor.getSelected();
                let noOfGroup = 1;
                let noOfBlockInGroup = 1;
                let showHeading = false;
                if(comp){
                    const attrs = comp.getAttributes();
                    noOfGroup = attrs[`data-noofgroup`];
                    noOfBlockInGroup = attrs[`data-noofblockingroup`];
                    showHeading = checkBoolean(attrs[`data-heading`]);
                }
                    const pfx = editor.getConfig('stylePrefix');
                    let modalHtmlContent = getHtmlElement('div', {}, {});
                    let innerContainer = getHtmlElement('div', {}, {class: "inner-container box-config", style: "display: flex; flex-direction: column;"});
                    let actionBtnContainer = getHtmlElement('div', {}, {class: "action-btns", style: "display: flex; justify-content: flex-end"});
                    innerContainer.innerHTML = `
                        ${generateCheckboxToggle('toggleElement', 'Show heading', 'heading')}
                        ${generatePlusMinusCounter('counterElement', 'noOfGroup', 'No. of text blocks',noOfGroup)}
                        ${generatePlusMinusCounter('counterElement', 'noOfBlockInGroup', 'No. of text elements in each text block',noOfBlockInGroup)}
                    `
                    modalHtmlContent.appendChild(innerContainer);
                    actionBtnContainer.appendChild(getHtmlElement('button', { 'innerHTML': 'Cancel', 'className': `btn btn-default`, 'onclick': handleClose }, { style: "margin-right: 1%"}));
                    actionBtnContainer.appendChild(getHtmlElement('button', { 'innerHTML': 'Apply Changes', 'className': `btn btn-primary`, 'onclick': handleSave }, {}));
                    modalHtmlContent.appendChild(actionBtnContainer);
                    modal.setContent(modalHtmlContent);
                    hydrateHeading('heading',showHeading);
                    hydratePlusMinusCounter(1, 1000, 'noOfGroup',);
                    hydratePlusMinusCounter(1, 25, 'noOfBlockInGroup');
            });
            modal.open({ title, content }).onceClose(function () {
                console.log('Custom group widget modal closed');
            });
        }
    });

    cm.add(cmdEditSortDelete, {
        run(editor, sender, opts = {}) {
            const modal = editor.Modal;
            let modalConfig = {};

            /**
            * @param {Object} - state
            * Function updates the changes in DOM with given state
            */

            function handleSave(state) {
                const checker = new Set();
                let rearrangeFlag = false;
                const wrapper = state.selector.wrapper ? state.comp.find(state.selector.wrapper)[0] : state.comp;
                state.currentIndexPos.forEach((index, i) => {
                    const textContent = state.allTextContent[index - 1];
                    if (index - 1 !== i) {
                        textContent.remove({ temporary: true });
                        wrapper.append(textContent, { at: i });
                        rearrangeFlag = true;
                    }
                    checker.add(index - 1);
                });

                state.allTextContent.forEach((textContent, i) => {
                    if (!checker.has(i)) {
                        textContent.remove();
                    }
                });

                // for Text-Group-Widget to update the noOfGroup Value
                if (state.allTextContent.length !== state.currentIndexPos.length && state.comp.getName() === 'Widget-text-group') {
                    var attrs = state.comp.getAttributes();
                    attrs[`data-noofgroup`] = parseInt(state.currentIndexPos.length, 10);
                    state.comp.setAttributes(attrs);
                }

                if (state.allTextContent.length !== state.currentIndexPos.length) {
                    toggleAlert(`${state.widgetName === 'widget-text-group' ? 'Text block(s)' : 'Images and videos'} ${rearrangeFlag ? "Rearranged and " : ""}deleted successfully.`);
                } else {
                    toggleAlert(rearrangeFlag ? `${state.widgetName === 'widget-text-group' ? "Text block(s) rearranged successfully." : "Images and videos rearranged successfully."}` : "There were no changes to be made.");
                }
                const selectedComponent = editor.getSelected();
                if (
                  selectedComponent &&
                  selectedComponent.view &&
                  selectedComponent.view.render &&
                  typeof selectedComponent.view.render === "function"
                ) {
                  selectedComponent.view.render();
                }
            }

            /**
            * @param {Object} - comp (GrapesJS Object)
            * @return {Object} - selector
            * Function returns the selector and wrappers selector of the slider widgets.
            */

            function getSelector(comp) {
                const name = comp.getName().toLowerCase();
                let wrapper = "";
                if ((name === "widget-image-slider") || (name === "widget-heroimage-slider")) {
                    wrapper = ".sliders-jssor";
                    return {
                        wrapper: wrapper,
                        textContent: `${wrapper} .item`
                    };
                } else if (name === "widget-variable-imagetext-slider") {
                    wrapper = ".wt-variable-imagetext-slider";
                    return {
                        wrapper: wrapper,
                        textContent: `${wrapper} .imagetext-container`
                    };
                } else if (name === "widget-img-txt-link") {
                    wrapper = ".img-content";
                } else if (name === "widget-text-group") {
                    wrapper = ".content-block";
                    return {
                        wrapper: wrapper,
                        textContent: `${wrapper} .text-content`
                    }
                }

                return {
                    wrapper: wrapper,
                    textContent: `${wrapper} .text-content`
                };
            }

            /**
            * It executes the callback function once the modal is open.
            */

            modal.onceOpen(function () {
                const comp = editor.getSelected();
                const selector = getSelector(comp);

                const state = {
                    comp: comp,
                    selector: selector,
                    allTextContent: comp.find(selector.textContent),
                    currentIndexPos: [],
                    widgetName: comp.getName().toLowerCase(),
                }

                // Creating the instance of the edit modal UI

                const editInstance = new CreateSortableAndDeleteEditModelUi();

                modalConfig = editInstance.getHtml({
                    state: state,
                    handleSave: handleSave,
                    modal: modal,
                    toggleAlert: toggleAlert,
                });

                // Updating HTML content to the modal
                modal.setContent(modalConfig.html);
            });

            modal.open({ title: "Rearrange/Delete", content: "" }).onceClose(function () {
                modalConfig.confirmModal.remove();
            });
        }
    });
}

const exportItems = {
    commands: commands,
    constructNewUrl: constructNewUrl,
    getOnSelectCallback: getOnSelectCallback,
}

export default exportItems;
