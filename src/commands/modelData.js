import { defaultAspectRatios } from './../consts';
import { initiateCKeditor } from "./../components/TextEditor.js";

/**
 * @param {Object} - config
 * Function will create Nodes from String type HTML code
 * @return {Node} - all the HTML content
*/

function createNewHtml(config, getHtmlContent) {
	const _helperEle = document.createElement('template');
	_helperEle.innerHTML = getHtmlContent(config, config.state).trim();

	return _helperEle.content.firstChild;
}

function CreateCropModelUi() {

	function getCropTabHtml(state) {
		const iconSize = 24;
		return `
			<button class="btn btn-default advance-croping-head" type="button" data-toggle="collapse" data-target="#advance-croping-body" aria-expanded="false" aria-controls="advance-croping-body" style="margin-bottom: -1px;">
				<h4>Crop by device</h4>
			  	<div class="down-icon">
			  		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
						<path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
					</svg>
			  	</div>
			</button>
			<div class="collapse" id="advance-croping-body">
			  	<div class="well">
				    <div class="c-advance-body">
						<div class="device-icons">
							<button class="btn btn-default icon desktop-icon active" data-value="desktop" data-toggle="tooltip" data-placement="bottom" title="Desktop">
								<svg xmlns="http://www.w3.org/2000/svg" width="${iconSize}" height="${iconSize}" fill="currentColor" class="bi bi-laptop" viewBox="0 0 16 16">
								  <path d="M13.5 3a.5.5 0 0 1 .5.5V11H2V3.5a.5.5 0 0 1 .5-.5h11zm-11-1A1.5 1.5 0 0 0 1 3.5V12h14V3.5A1.5 1.5 0 0 0 13.5 2h-11zM0 12.5h16a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 12.5z"/>
								</svg>
							</button>
							<button class="btn btn-default icon tablet-icon" data-value="tablet" data-toggle="tooltip" data-placement="bottom" title="Tablet">
								<svg xmlns="http://www.w3.org/2000/svg" width="${iconSize}" height="${iconSize}" fill="currentColor" class="bi bi-tablet" viewBox="0 0 16 16">
								  <path d="M12 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h8zM4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4z"/>
								  <path d="M8 14a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
								</svg>
							</button>
							<button class="btn btn-default icon mobile-icon" data-value="mobile" data-toggle="tooltip" data-placement="bottom" title="Mobile">
								<svg xmlns="http://www.w3.org/2000/svg" width="${iconSize}" height="${iconSize}" fill="currentColor" class="bi bi-phone" viewBox="0 0 16 16">
								  <path d="M11 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h6zM5 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H5z"/>
								  <path d="M8 14a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
								</svg>
							</button>
						</div>
						<div class="all-devices-btn">
							<a href="#" id="use-d-all">Use desktop crop for all devices</a>
						</div>
					</div>
			  	</div>
			</div>
		`;
	}

	function getRatioAndCustomCropHtml(config) {
		return `
			<div class="c-custom-ratios">
				<div class="selected-crop-btns">
					<h4>Standard aspect ratios</h4>
					<div class="map-defiend-ratios"></div>
				</div>
				<div class="custom-crop-container">
					<div class="custom-crop-head">
						<h4>Crop with custom ratio</h4>
					</div>
					<div class="custom-crop-body" style="display: flex; align-items: end; justify-content: space-between;">
						<div class="form-group" style="margin-bottom: 0px; margin-right: 2%">
							<label for="custom-width" style="font-size: 13px; font-weight: normal;">Width</label>
							<input type="number" class="form-control custom-width-input" id="custom-width" ${config.isWidgetCropEnable ? "disabled" : ""}/>
						</div>
						<div class="form-group" style="margin-bottom: 0px; margin-right: 2%">
							<label for="custom-height" style="font-size: 13px; font-weight: normal;">Height</label>
							<input type="number" class="form-control custom-height-input" id="custom-height" ${config.isWidgetCropEnable ? "disabled" : ""}/>
						</div>
						<button class="btn btn-primary gjs-cutom-btn custom-crop-btn" id="crop-custom-btn" ${config.isWidgetCropEnable ? "disabled" : ""}>Crop</button>
					</div>
				</div>
			</div>
		`;
	}

	/**
	 * @param {Object} - config
	 * @param {Object} - state
	 * @return {String} - HTML content with given data
	*/
	function getHtmlContent(config, state) {
		const deviceListStyle = "max-width: 200px; border-radius: 3px; padding: 3px; border: 1px solid #5697E350; display: flex; padding-left: 4%; margin: 1% 0px; align-items: center;";

		return `
			<div class="clearfix imgcropparent">
				<div class="d-flex flex-row flex-column" style="display: flex;">
					<div class="col-md-8 c-image-container" style="padding-left: 0px;">
						<div id="cropImgContainer" class="crop-pic col-md-9" style="padding: 0px;">
	                		<div id="loadMsg">
	                  			<p>Loading...Please wait!</p>
	                  			<i class="fa fa-spinner fa-spin"></i>
	                		</div>
		                	<img id="widgetCropPic" src="${state.imgUrl}" />
		              	</div>
					</div>
					<div class="col-md-4 c-tabs tab-container" style="padding: 0px">
						<ul class="nav nav-tabs c-tab-heads" role="tablist">
						    <li role="presentation" class="active tab-head"><a href="#crop-image-tab" aria-controls="crop-image-tab" role="tab" data-toggle="tab" data-value="crop">Crop Image</a></li>
							${config.isWidgetCrop ? "" : `<li role="presentation" class="tab-head"><a href="#add-links-tab" aria-controls="add-links-tab" role="tab" data-toggle="tab" data-value="caption">Add Caption & Links</a></li>`}
						</ul>
						<div class="tab-content c-tab-bodies">
						    <div role="tabpanel tab-body" class="tab-pane fade in active" id="crop-image-tab">
						    	${getCropTabHtml(config.state)}
								${getRatioAndCustomCropHtml(config)}
						    </div>
						    <div role="tabpanel tab-body" class="tab-pane" id="add-links-tab">
						    	<h4>Add Caption</h4>
						    	<textarea id="caption-field"></textarea>

						    	<div class="form-group" style="margin-top: 10px;">
						    		<label for="cap-place">Caption Placement</label>
						    		<select class="form-control" id="cap-place" style="border-radius: 4px !important;">

						    			<option value="top-left">Top-Left</option>
						    			<option value="top-center">Top-Center</option>
						    			<option value="top-right">Top-Right</option>

						    			<option value="center-left">Center-Left</option>
						    			<option value="center-center">Center-Center</option>
						    			<option value="center-right">Center-Right</option>

						    			<option value="bottom-left">Bottom-Left</option>
						    			<option value="bottom-center">Bottom-Center</option>
						    			<option value="bottom-right">Bottom-Right</option>

						    		</select>
						    	</div>

						    	<div class="link-type" id="link-type" style="display: flex; margin-bottom: 10px; width: 100%;">
						    		<div style="display: flex; justify-content: flex-start; align-items: center; width: 25%; padding-left: 20px;">
						    			<input type="radio" name="link-type-field" id="link-caption-input" data-value="caption" style="margin: 0px;" ${config.captionState.linkType === "caption" ? "checked" : ""}/>
						    			<label for="link-caption-input" style="margin: 0px; margin-left: 5px;">Link Caption</label>
						    		</div>
						    		<div style="display: flex; justify-content: flex-start; align-items: center; width: 25%; padding-left: 20px;">
						    			<input type="radio" name="link-type-field" id="link-image-input" data-value="image" style="margin: 0px;" ${config.captionState.linkType === "image" ? "checked" : ""}/>
						    			<label for="link-image-input" style="margin: 0px; margin-left: 5px;">Link Image</label>
						    		</div>
						    	</div>

						    	<div class="link-field c-tabs">
						    		<ul class="nav nav-tabs c-tab-heads" role="tablist" style="margin-bottom: 10px;">
						    		    <li class="tab-head active" style="width: 25%; display: flex; justify-content: flex-start; align-items: center; padding-left: 9px;">
						    		    	<input type="radio" name="links" id="inter-link" data-tab_body="#internal-link" style="margin:0px;">
						    		    	<label for="inter-link" style="margin: 0px; margin-left: 5px; font-size: 14px;">Internal Link</label>
						    		    </li>
						    		    <li class="tab-head" style="width: 25%; display: flex; justify-content: flex-start; align-items: center; padding-left: 15px;">
						    		    	<input type="radio" name="links" id="exter-link" data-tab_body="#external-link" style="margin:0px;">
						    		    	<label for="exter-link" style="margin: 0px; margin-left: 5px; font-size: 14px;">External Link</label>
						    		    </li>
						    		</ul>

						    		<div class="tab-content">
						    		    <div role="tabpanel" class="tab-pane active" id="internal-link">
						    		    	<select class="form-control" style="border-radius: 4px !important;"></select>
						    		    </div>
						    		    <div role="tabpanel" class="tab-pane" id="external-link">
						    		    	<input type="text" class="form-control" style="border-radius: 4px !important; height: 28px;" placeholder="link..." name="external-link"/>
						    		    </div>
										<div class="unlink-container">
                                            <div class="btn btn-sm" id="unlink-btn"><i class="fas fa-times"></i></div>
                                        </div>
						    		</div>

						    		<div class="new-window-container" style="display: flex; align-items: center; justify-content: space-between; margin-top: 10px;">
						    			<div class="new-window-wrapper new-window-main" style="margin-top: 10px;">
							    			<input type="checkbox" id="new-window" style="margin:0px;" ${config.captionState.newWindow && config.captionState.newWindow === "true" ? "checked" : ""}/>
							    			<label for="new-window" style="margin:0px;">Open In New Window</label>
							    		</div>
						    		</div>

						    	</div>

						    </div>
						</div>
					</div>

	          	</div>

	          	<button class="btn btn-primary" id="crop-save-btn" data-loading-text="Applying..." style="float: right; margin-top: 2%;">Apply Changes</button>
	          	<button class="btn btn-default" id="close-modal" style="float: right; margin-top: 2%; margin-right: 1%;">Cancel</button>
	        </div>
		`;
	}

	/**
	 * @param {Object} - config
	 * @return {Boolean} - Function will check the ratio for all the devices
	*/
	function isRatioSameForAll(config) {
		const allDevices = ['desktop', 'tablet', 'mobile'];
		let allRatios = allDevices.map((dev) => config.state[dev+'Dimensions'].ratio);
		if(!config.state.urls['tablet']) {
			return true;
		}
		return allRatios.every((r) => r === allRatios[0]);
	}

	/**
	 * @param {Object} - actions
	 * Function will disable the active standard aspect ratio button
	*/
	function disableActiveCropBtn(actions) {
		let activeBtn = actions.standardBtns.querySelector('button.active');
		if(activeBtn) {
			activeBtn.classList.remove('active');
		}
	}

	/**
	 * @param {Object} - actions
	 * @param {Object} - config
	 * @param {Number} - width
	 * @param {Number} - height
	 *
	 * Function will crop the image with given data and it will also update the state
	*/
	function cropImage(actions, config, width, height) {
		const ratio = +(width / height).toFixed(3);
		config.getOnSelectCallback(ratio, config.updateJcropApi)();
		disableActiveCropBtn(actions);

		const allDevices = ['desktop', 'tablet', 'mobile'];
		if(config.state.cropByDevice.cropForAll) {

			allDevices.forEach(item => {
				config.state.customValues.width[item] = width;
				config.state.customValues.height[item] = height;
			});
		} else {
			config.state.customValues.width[config.state.cropByDevice.device] = width;
			config.state.customValues.height[config.state.cropByDevice.device] = height;
		}
	}

	/**
	 * @param {Object} - actions
	 * @param {Object} - config
	 *
	 * Function will crop the image as par given width and height
	*/

	function cropWithCustomRatio(actions, config) {

		actions.customHeight.addEventListener('keypress', (e) => {
			if(e.keyCode === 13) {
				cropImage(actions, config, +actions.customWidth.value, +actions.customHeight.value);
			}
		});

		actions.customCropBtn.addEventListener('click', () => {
			cropImage(actions, config, +actions.customWidth.value, +actions.customHeight.value);
		})
	}

	/**
	 * @param {Object} - config
	 * @return {Object - [Node]} - All actionable HTML elements
	 * This function will expose elements publically for later use
	*/

	function getActionable(config) {
		return {
			standardBtns: config.html.querySelector('.map-defiend-ratios'),
			saveBtn: config.html.querySelector('#crop-save-btn'),
			closeBtn: config.html.querySelector('#close-modal'),
			loadMsg: config.html.querySelector('#loadMsg'),
			imgEle: config.html.querySelector('#widgetCropPic'),
			customWidth: config.html.querySelector('.custom-crop-body #custom-width'),
			customHeight: config.html.querySelector('.custom-crop-body #custom-height'),
			customCropBtn: config.html.querySelector('.custom-crop-body #crop-custom-btn'),
			allIcons: config.html.querySelectorAll('#advance-croping-body .icon'),
			useAll: config.html.querySelector('#use-d-all'),
			adCropHead: config.html.querySelector('.advance-croping-head'),
			adCropBody:config.html.querySelector('#advance-croping-body'),
			captionField: config.html.querySelector('#caption-field'),
			tabHeads: config.html.querySelectorAll('.c-tabs .c-tab-heads a[data-toggle="tab"]'),
			captionPlace: config.html.querySelector('#cap-place'),
			interLinksSelect: config.html.querySelector('#internal-link select'),
			externalLinkInput: config.html.querySelector('#external-link input'),
			linkHeads: config.html.querySelectorAll('.link-field li.tab-head input'),
			linkContent: config.html.querySelector('.link-field .tab-content'),
			linkType: config.html.querySelector('#link-type'),
			newWindowEle: config.html.querySelector('.new-window-main input[type="checkbox"]'),
			unlinkBtn: config.html.querySelector('#unlink-btn'),
		}
	}

	/**
	 * @param {String} - device
	 * @param {Object} - actions
	 * @param {Object} - config
	 * @param {Boolean} - isInitial
	 *
	 * Function will update the cropping data on the UI
	*/

	function updateDefaultOnUi(device, actions, config, isInitial) {
		let allRatioBtns = actions.standardBtns.querySelectorAll('button');
		const defaultRatio = config.state[device + 'Dimensions'].ratio;

		actions.customWidth.value = "";
		actions.customHeight.value = "";

		let flag = true;
		if(!isInitial) {
			disableActiveCropBtn(actions);
			allRatioBtns.forEach(btn => {
				let btnRatio = +btn.getAttribute('data-ratio-value');
				if(btnRatio === defaultRatio) {
					btn.click();
					flag = false;
				}
			});
		}

		if(flag && !actions.standardBtns.querySelector('button.active')) {
			actions.customWidth.value = !!+config.state.customValues.width[device] ? +config.state.customValues.width[device] : "";
			actions.customHeight.value = !!+config.state.customValues.height[device] ? +config.state.customValues.height[device] : "";
			setTimeout(() => actions.customWidth.focus(), 10);
		}
	}

	/**
	 * @param {Object} - config
	 * @param {Boolean} - flag
	 * Function will update the state to decide whether the crop should apply for all devices or not
	*/

	function updateCropForAll(config, flag) {
		if(flag) {
			config.state.cropByDevice.cropForAll = true;
		} else {
			config.state.cropByDevice.cropForAll = false;
		}
	}

	/**
	 * @param {Object} - config
	 * @param {Object - [Node]} - actions
	 * This function will add event listeners on the elements
	*/

	let prevSelectedDevice = null;
	function addListeners(config, actions) {

		// for the device change
		actions.allIcons.forEach((icon, i) => {
			if(i === 0) {
				prevSelectedDevice = icon;
			}
			icon.addEventListener('click', (e) => {
				if(icon.classList.contains('active')) {
					return;
				}
				const selectedDevice = icon.getAttribute('data-value');
				config.state.cropByDevice.device = selectedDevice;
				config.state.selectedRatio = config.state[`${selectedDevice}Dimensions`].ratio;
				icon.classList.add('active');

				if(prevSelectedDevice) {
					prevSelectedDevice.classList.remove('active');
				}
				prevSelectedDevice = icon;

				updateDefaultOnUi(selectedDevice, actions, config);
				config.updateJcropApi(config.state[`${selectedDevice}Dimensions`]);
			});
		});

		// for apply btn
		actions.saveBtn.addEventListener('click', (e) => {
			config.handleSave();
		});

		// for close btn
		actions.closeBtn.addEventListener('click', () => config.modal.close());

		// adding event listener for custom cropping
		cropWithCustomRatio(actions, config);

		// to use desktop for all
		actions.useAll.addEventListener('click', (e) => {
			e.preventDefault();
			e.stopPropagation();
			if(confirm('Cropping will be the same for all devices!')) {
				$(actions.adCropBody).collapse('hide');
			}
		});

		// Bootstrap event listener after opening cropping by device
		$(actions.adCropBody).on('show.bs.collapse', function() {
			updateCropForAll(config, false);
		});

		// Bootstrap event listener after hiding cropping by device
		$(actions.adCropBody).on('hidden.bs.collapse', function() {
			actions.allIcons[0].click(); // setting desktop as default
			updateCropForAll(config, true);
		});

		actions.tabHeads.forEach(head => {
			$(head).on('shown.bs.tab', function (e) {
			  	const value = e.target.getAttribute('data-value');
			  	if(value === 'caption') {
			  		config.captionState.isActive = true;
			  		config.state.isActive = false;
			  		//config.state.cropApi.destroy();
			  	} else {
			  		config.captionState.isActive = false;
			  		config.state.isActive = true;
			  		//config.initJcropElement(actions.imgEle, config.state.cropOptions, config.setJcropApiObject);
			  	}
			});
		});

		// setting default value to select box
		actions.captionPlace.value = config.captionState.position;
		actions.captionPlace.addEventListener('change', (e) => {
			config.captionState.position = actions.captionPlace.value;
		});

		// adding listener on link type

		actions.linkType.addEventListener('click', (e) => {
			let linkTypeRadio = actions.linkType.querySelector('input:checked');
			config.captionState.linkType = linkTypeRadio.getAttribute('data-value');
		});

		actions.newWindowEle.addEventListener('click', function(e) {
			if(this.checked) {
				config.captionState.newWindow = true;
			} else {
				config.captionState.newWindow = false;
			}
		});

	}

	/**
	 * @param {Object} - item
	 * @param {Object} - config
	 * @param {Object} - actions
	 *
	 * Function will create a standard aspect ratio button.
	*/

	let prevActiveBtn = null;
	function createStandardBtn(item, config, actions) {
		let button = document.createElement('button');
		button.innerHTML = item.name;
		button.className = `btn btn-default s-ratio-btn`;
		button.setAttribute('data-ratio-value', item.value);
		if(config.isWidgetCropEnable) {
			button.setAttribute("disabled", true);
		}

		button.addEventListener('click', function() {
			config.getOnSelectCallback(item.value, config.updateJcropApi)();

			actions.customWidth.value = ""; // Removing values from custom input fields
			actions.customHeight.value = "";
			if(config.state.cropByDevice.cropForAll) {
				for(let device of Object.keys(config.state.customValues.width)) {
					config.state.customValues.width[device] = "";
				}
				for(let device of Object.keys(config.state.customValues.height)) {
					config.state.customValues.height[device] = "";
				}
			} else {
				config.state.customValues.width[config.state.cropByDevice.device] = "";
				config.state.customValues.height[config.state.cropByDevice.device] = "";
			}

			if(prevActiveBtn) {
				prevActiveBtn.classList.remove('active');
			}
			button.classList.add('active');
			prevActiveBtn = button;
		});

		if(!prevActiveBtn && ((+item.value === +config.state.selectedRatio) || (!config.state.customValues.width["desktop"] && +item.value === 0))) {
			button.classList.add('active');
			prevActiveBtn = button;
		}

		return button;
	}

	/**
	 * @param {Object} - actions
	 * @param {Object} - config
	 *
	 * It will add all the standard aspect ratio button in the UI
	*/
	function mapStandardRatios(actions, config) {
		defaultAspectRatios.forEach(item => {
			actions.standardBtns.appendChild(createStandardBtn(item, config, actions));
		});
	}

	function getInternalLinks() {
		const links = internalLinks;
		const newLinks = [];

		let i = 0;
		let j = 1;

		while(i < j && i < links.length) {
			if(links[i].hotelId) {
				const newPage = {...links[i], level: 0};

				newPage.children = [{...newPage, text: "Homepage", level: 1}];

				while(j < links.length && newPage.hotelId === links[j].hotelId) {
					const newChildPage = {...links[j], level: 1};
					newPage.children.push(newChildPage);
					if(newChildPage.children) {
						newChildPage.children.forEach((cLink) => {
							newPage.children.push({...cLink, level: 2});
						});
						newChildPage.children = [];
					}
					j++;
				}

				newLinks.push(newPage);
				i = j;
				j = i + 1;
			} else {
				i++;
				j++;
			}
		}

		return newLinks;
	}

	function isInternalLink(config) {
		return config.captionState.default.imgLink && config.captionState.default.imgLink.charAt(0) === '#';
	}

	function updateInternalLinks(actions, config, internalLinks) {
		const select2Instance = $(actions.interLinksSelect).select2({
			data: getInternalLinks(),
			width: '100%',
			placeholder: "Select a Link",
			templateResult: function(node) {
				return $('<span style="padding-left:' + (20 * node.level) + 'px;">' + node.text + '</span>');
			},
			formatSelection: function(item) {
		      return item.text
		    },
		    formatResult: function(item) {
		      return item.text
		    },
		});

		// to set default values
		if(!isInternalLink(config)) {
			if(!config.captionState.default.imgLink) {
				select2Instance.val(null).trigger("change");
			} else {
				actions.externalLinkInput.value = config.captionState.default.imgLink;
				actions.unlinkBtn.classList.add('show');
			}
		} else {
			select2Instance.val(config.captionState.default.imgLink).trigger("change");
			actions.unlinkBtn.classList.add('show');
		}

		$(actions.interLinksSelect).on("select2:select", function(e) {
			config.captionState.imgLink = e.params.data.id;
			if(config.captionState.imgLink === '') {
                actions.unlinkBtn.classList.remove('show');
            } else {
                actions.unlinkBtn.classList.add('show');
            }
		});

		actions.externalLinkInput.addEventListener("input", function(e) {
			if(e.target.value.trim()) {
				config.captionState.imgLink = e.target.value.trim();
				if(config.captionState.imgLink === '') {
                    actions.unlinkBtn.classList.remove('show');
                } else {
                    actions.unlinkBtn.classList.add('show');
                }
			}
		});

		actions.unlinkBtn.addEventListener("click", function() {
			select2Instance.val(null).trigger("change");
			actions.externalLinkInput.value = "";
			config.captionState.imgLink = "";
			config.captionState.default = "";
			this.classList.remove("show");
		});
	}

	function addTabFeatureForLinks(actions, config) {
		let prevSelectedLinkType = null; //actions.linkContent.querySelector(actions.linkHeads[0].getAttribute('data-tab_body'))
		actions.linkHeads.forEach((head) => {
			const targetSelector = head.getAttribute('data-tab_body');
			head.addEventListener('click', (e) => {
				const targetEle = actions.linkContent.querySelector(targetSelector);
				if(targetEle.classList.contains('active')) {
					return;
				}
				targetEle.classList.add('active');
				/*if(head.getAttribute('id') === 'exter-link') {
					config.captionState.imgLink = config.captionState.default.imgLink;
				} else {
					config.captionState.imgLink = config.captionState.default.imgLink;
				}*/
				if(isInternalLink(config) && head.getAttribute('id') !== 'exter-link') { //checking for default link
					$(actions.interLinksSelect).val(config.captionState.default.imgLink).trigger('change');
				} else if(!isInternalLink(config) && head.getAttribute('id') === 'exter-link') {
					$(actions.interLinksSelect).val(null).trigger('change');
					actions.externalLinkInput.value = config.captionState.default.imgLink;
				}
				config.captionState.imgLink = config.captionState.default.imgLink;
				if(prevSelectedLinkType) {
					prevSelectedLinkType.classList.remove('active');
				}
				prevSelectedLinkType = targetEle;
				e.stopPropagation();
			});
		});

		prevSelectedLinkType = actions.linkContent.querySelector(actions.linkHeads[0].getAttribute('data-tab_body'));
		if(!config.captionState.imgLink || isInternalLink(config)) {
			actions.linkHeads[0].click();
		} else {
			actions.linkHeads[1].click();
		}
	}

	/**
	 * This will allow to use actionable elements publically
	*/

	this.publicActionEle = {};

	/**
	 * @param {Object} - config
	 * This function will return HTML code with all the event listener added
	 * @return {Node} - html element
	*/
	this.getHtml = function(config) {

		if(!config.state.imgUrl) {
			alert('Image url is required!');
			return null;
		}
		config.html = createNewHtml(config, getHtmlContent);

		const actions = getActionable(config); // Getting all actionable elements

		addListeners(config, actions); // Adding event listeners on actionable elements
		mapStandardRatios(actions, config); // Add standard aspect ratio buttons
		updateDefaultOnUi('desktop', actions, config, true); // Updating default state on the UI
		updateInternalLinks(actions, config, internalLinks); // Updating internal Links
		addTabFeatureForLinks(actions, config); // Change between internal and external links
		const ckeditorInstance = initiateCKeditor(actions.captionField);
		// above instance can be use to set data in editor.
		ckeditorInstance.setData(config.captionState.caption);
		config.captionState.ckeditorInstance = ckeditorInstance;
		// Opening cropping by device section if aspect ratio for all the device is different
		if(!isRatioSameForAll(config)) {
			setTimeout(() => actions.adCropHead.click(), 5);
		}

		this.publicActionEle = actions;

		return config.html;
	}

}

function CreateSortableAndDeleteEditModelUi() {

	/**
	 * @param {Object} - config
	 * @param {Object} - state
	 * @return {String} - HTML content with given data
	*/

	function getHtmlContent(config) {
		return `
			<div class="clearfix setting-modal">
				<div class="d-flex flex-row flex-column">
					<div class="${config.state.widgetName === 'widget-text-group' ? 'sortable-container rearrange-text-blocks' :  'sortable-container rearrange-images'}" style="display: flex; flex-wrap: wrap;">

					</div>
                    ${config.state.allTextContent.length
                        ? `<div class="action-btns">
                                <button class="btn btn-default close-setting-modal">Cancel</button>
                                <button class="btn btn-primary apply-changes">Apply Changes</button>
                           </div>`
                        : ""
                    }

				</div>
			</div>
		`;
	}

    /**
    * @param {Object} - attrs
    * @param {Number} - i (index)
    * @return {Function} - callback
    * Function returns the template for preview image of the slider/text of the Text widget in a modal.
    */

	function createClonedContent(config, attrs, i, isContent) {
		if(config.state.widgetName === 'widget-text-group') {
			const textContent = `
				<div class="cloned-text-wrapper" data-index="${i + 1}">
					<div class="cloned-text-content-wrapper">
						<div class="cloned-text-data-wrapper">${isContent.view.$el.html()}</div>
						<div class="cloned-text-data-counter">
							<div class="text-data" style="width: 60%;">
								<span class="text-index">${i + 1}</span>
							</div>
							<button type="button" class="delete-btn" data-index="${i + 1}" data-toggle="modal" data-target=".alert-modal">
								<span class="fa fa-trash-o" aria-hidden="true"></span>
							</button>
						</div>
					</div>
				</div>
			`;

			return () => {
				return textContent;
			};
		} else {
			const imageContent = `
				<div class="cloned-image-wrapper" data-index="${i + 1}">
					<div class="cloned-image-content-wrapper">
						<${isContent ? "img" : "video"} src="${attrs["src"]}" alt="${attrs["alt"]}" ${attrs["poster"] ? `poster="${attrs["poster"]}"` : ""} class="cloned-slider-image" ${isContent ? "/>" : "></video>"}
						<div class="cloned-image-data-wrapper">
							<div class="image-data" style="width: 60%;">
								<span class="image-index">${i + 1}</span>
								${isContent ? "" : `<span class="show-video" style="margin-left: 2%;">Video</span>`}
							</div>
							<button type="button" class="delete-btn" data-index="${i + 1}" data-toggle="modal" data-target=".alert-modal">
								<span class="fa fa-trash-o" aria-hidden="true"></span>
							</button>
						</div>
					</div>
					${config.state.comp.getName().toLowerCase() !== "widget-image-slider"
						?
							`<div class="text-message">
								Includes a text of ${i + 1}${[,'st','nd','rd'][(i + 1)/10%10^1&&(i + 1)%10]||'th'} ${isContent ? "image" : "video"}.
							</div>`
						: ""
					}
				</div>
			`;
	
			return () => {
				return imageContent;
			};
		}
	}

    /**
    * @return {Function} - callback
    * Function returns a template for confirmation modal
    */

	function createConfirmModal(config) {
		const modal = `
			<div class="modal fade alert-modal" tabindex="-1" role="dialog" aria-labelledby="alertModal">
			  	<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<i class="fa fa-close" aria-hidden="true"></i>
							</button>
							<h4 class="modal-title">Delete ${config.state.widgetName === 'widget-text-group' ? 'Text Block' : 'Image/Video'}</h4>
						</div>
						<div class="modal-body">
							<p>${config.state.widgetName === 'widget-text-group' ? 'This text block will be deleted.' : 'This image or video will be deleted.'}</p>
						</div>
						<div class="modal-footer" style="display: flex; flex-direction: column; justify-content: flex-end;">
							<label style="display: flex; align-items: center; justify-content: flex-end; margin-bottom: 10px;">
								<input type="checkbox" class="dont-ask-check" style="margin: 0px 10px 0px 0px;"/>
								<span style="font-weight: normal;">Don't ask me about this again</span>
							</label>
							<div class="modal-footer-action-buttons">
								<button type="button" class="btn btn-default close-modal" data-dismiss="modal">Cancel</button>
								<button type="button" class="btn btn-primary delete-permanently" data-dismiss="modal">Ok</button>
							</div>
						</div>
					</div>
			  	</div>
			</div>
		`;

		return () => {
			return modal;
		}
	}

    /**
    * @param {Object} - config
    * @return {Object} - actions
    * Function return the actionable elements from the generated html.
    */

	function getActionable(config) {
		return {
			sortContainer: config.html.querySelector(".sortable-container"),
            applyChanges: config.html.querySelector(".action-btns .apply-changes"),
            closeSettingModal: config.html.querySelector(".action-btns .close-setting-modal"),
			confirmModal: config.confirmModal,
			dontAsk: config.confirmModal.querySelector(".dont-ask-check"),
			modalDeleteBtn: config.confirmModal.querySelector(".delete-permanently"),
            modalCloseBtn: config.confirmModal.querySelector(".close-modal"),
			getAllDelBtns: function() {
				return this.sortContainer.querySelectorAll(".delete-btn");
			}
		};
	}

    /**
    * @param {Object} - config
    * @param {Node} - ele
    * Function remove the given image/video or Text from modal
    */

	function deleteThisContent(config, ele) {
		setTimeout(() => {
            if(config.state.currentIndexPos.length === 1) {
				const message = config.state.widgetName === 'widget-text-group' ? "The text block must have at least one block." : "The slider must have at least one image or video.";
                config.toggleAlert(message, "error");
            } else {
				if(config.state.widgetName === 'widget-text-group') {
					ele.closest(".cloned-text-wrapper").remove();
				} else {
					ele.closest(".cloned-image-wrapper").remove();
				}
                config.allClonedSortableContainer.trigger("sortupdate");
                if(config.state.currentIndexPos.length <= 0) {
                    updateNoContentStatus(config);
                }
            }
        }, 0);
	}

    /**
    * @param {Object} - config
    * Function adds an events on action elements.
    */

	function applyEvents(config) {
		let targetContent = null;
		config.dontAskFlag = false;
        config.deleteTriggerFlag = false;

		config.actions.dontAsk.addEventListener("click", function(e) {
			if(config.actions.dontAsk.checked) {
				config.dontAskFlag = true;
			} else {
				config.dontAskFlag = false;
			}
		});

		config.actions.modalDeleteBtn.addEventListener("click", function(e) {
			$(config.actions.confirmModal).modal('hide');
            config.deleteTriggerFlag = true;

			if(config.dontAskFlag) {
				const allDelBtns = config.actions.getAllDelBtns();
				allDelBtns.forEach(btn => {
					btn.removeAttribute("data-toggle");
					btn.removeAttribute("data-target");
				});
			}
			deleteThisContent(config, targetContent);
		});

		$(config.actions.confirmModal).on("shown.bs.modal", function(e) {
			targetContent = e.relatedTarget;
		});

        $(config.actions.confirmModal).on("hidden.bs.modal", function(e) {
            if(config.actions.dontAsk.checked && !config.deleteTriggerFlag) {
                config.dontAskFlag = false;
                config.actions.dontAsk.checked = false;
            }
		});

        config.actions.modalCloseBtn.addEventListener("click", function(e) {
            config.dontAskFlag = false;
            config.actions.dontAsk.checked = false;
        });

        config.actions.applyChanges && config.actions.applyChanges.addEventListener("click", function(e) {
            config.handleSave(config.state);
            config.modal.close();
        });

        config.actions.closeSettingModal && config.actions.closeSettingModal.addEventListener("click", function(e) {
            config.modal.close();
        });
	}

    /**
    * @param {Object} - config
    * @param {String} - text
    * @param {Boolean} - flag
    * @return {String} - HTML content with given data
    */
	function createMessage(config, text, flag = "danger") {
		return createNewHtml(config, () => {
			return `
				<div class="no-sortable-container alert alert-${flag}" role="alert">
					<h2 style="margin: 0px; font-weight: bold;">${text}</h2>
				</div>
			`;
		});
	}

    /**
    * @param {Object} - config
    * Function creates and updated message to DOM if there are no images/video or Text to perform action.
    */

	function updateNoContentStatus(config) {
		const message = config.state.widgetName === 'widget-text-group' ? "No text block(s) in this block!" : "No images and Videos in this slider!";
		config.actions.sortContainer.appendChild(createMessage(config, message));
	}

    /**
    * @param {Object} - config
    * Function renders all the preview images/video or Text in the modal
    */

	function addClonedContent(config) {
		const currentIndexPos = [];
		if(config.state.allTextContent.length <= 0) {
			updateNoContentStatus(config);
		}
		if(config.state.widgetName === 'widget-text-group') {
			config.state.allTextContent.forEach((textContent, i) => {
				const isText = textContent;
				const newText = createNewHtml(config, createClonedContent(config, {}, i, isText));
				if(newText) {
					newText.querySelector(".delete-btn").addEventListener('click', (e) => {
						if(config.dontAskFlag) {
							deleteThisContent(config, e.target);
						}
					});
					config.actions.sortContainer.appendChild(newText);
					currentIndexPos.push(parseInt(newText.getAttribute("data-index")));
				}
			});
		} else {
			config.state.allTextContent.forEach((textContent, i) => {
				const isImage = textContent.find("img").length !== 0;
				const attrs = textContent.find(isImage ? "img" : "video")[0].getAttributes();
				const newImg = createNewHtml(config, createClonedContent(config, attrs, i, isImage));
				if(newImg) {
					newImg.querySelector(".delete-btn").addEventListener('click', (e) => {
						if(config.dontAskFlag) {
							deleteThisContent(config, e.target);
						}
					});
					config.actions.sortContainer.appendChild(newImg);
					currentIndexPos.push(parseInt(newImg.getAttribute("data-index")));
				}
			});
		}

		config.state.currentIndexPos = currentIndexPos;
	}

    /**
    * @param {Object} - config
    * Function initiate a sortable API on preview images/video or Text and also add event
    */

	function applySortable(config) {
		setTimeout(() => {
			const allClonedSortableContainer = $(`${config.state.widgetName === 'widget-text-group' ? '.sortable-container.rearrange-text-blocks' : '.sortable-container.rearrange-images'}`);
			const sortableInstance = allClonedSortableContainer.sortable({});
			config.sortableInstance = sortableInstance;
			config.allClonedSortableContainer = allClonedSortableContainer;

			allClonedSortableContainer.on("sortupdate", function(event, ui) {
				const updatedPos = allClonedSortableContainer.sortable("toArray", {attribute: "data-index"});
				config.state.currentIndexPos = updatedPos.map((i) => parseInt(i));
			});
		}, 0);
	}

    /**
    * @param {Object} - config
    * @return {Object} - config
    * Init function for html code of the modal
    */

	this.getHtml = function(config) {
		config.html = createNewHtml(config, getHtmlContent);
		config.confirmModal = createNewHtml(config, createConfirmModal(config));
		document.body.appendChild(config.confirmModal); // adding confirm modal to body

		config.actions = getActionable(config);

		addClonedContent(config);

		applySortable(config);
		applyEvents(config);

		return config;
	}


}
export { CreateCropModelUi, CreateSortableAndDeleteEditModelUi }
