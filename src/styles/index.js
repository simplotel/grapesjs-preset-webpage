export default (editor, config) => {
  const sm = editor.StyleManager;
  const csm = config.customStyleManager;
  const propertyFileViewObj = editor.StyleManager.getType('file');

  if(config.showAssetsMethod) {
    editor.StyleManager.addType('file', {
      model: propertyFileViewObj.model,
      view: propertyFileViewObj.view.extend({
        init() {
          const em = this.em;
          this.modal = em.get('Modal');
          this.am = em.get('AssetManager');
          this.events['click #' + this.pfx + 'close'] = 'removeFile';
          this.events['click #' + this.pfx + 'images'] = 'openAssetManager';
          this.events['click #' + this.pfx + 'crop'] = 'openCropWindow';
          this.delegateEvents();
        },
        templateInput() {
          const pfx = this.pfx;
          const assetsLabel = this.config.assetsLabel || 'Images';
          return `
          <div class="${pfx}field ${pfx}file">
            <div id='${pfx}input-holder'>
              <div class="${pfx}btn-c">
                <button class="${pfx}btn" id="${pfx}images" type="button">
                  ${assetsLabel}
                </button>
              </div>
              <div style="clear:both;"></div>
            </div>
            <div id="${pfx}preview-box" style="padding-left: 0; padding-right: 0;">
              <div id="${pfx}preview-file"></div>
              <div id="${pfx}close">&Cross;</div>
              <button id="${pfx}crop" onclick="this.openCropWindow" style="display: none; margin-top: 6px; background-color: rgba(99, 99, 99, 0.68); color: #fff; border: none; border-radius: 2px; padding: 5px;">
                Crop
              </button>
            </div>
          </div>
          `;
        },
        openCropWindow(){
          function constructNewUrl(config, comp, data = null, isAsset = false) {
            if(!data) return;
            const { x1, y1, x2, y2, imgUrl } = data;
            const originalImgUrl = imgUrl.split('/q_30/');
            const width = x2-x1;
            const height = y2-y1;
            const PRECISION_VALUE = 3;
            const cropParams = {
                params: `/x_${Math.round(x1)},y_${Math.round(y1)},w_${Math.round(width)},h_${Math.round(height)},r_0,c_crop`,
                cropedAspectRatio: parseFloat(width / height).toFixed(PRECISION_VALUE),
            };
            const resizedParams = config.resizeImageParams(comp, cropParams);
            const URL = `${originalImgUrl[0]}${resizedParams.dResizedparams}${originalImgUrl[originalImgUrl.length - 1]}`;
            return URL;
          }
          function extractUrlData(url) {
            if (!url) {
                return {};
            }
            const data = {
              x1:0,
              y1:0,
              x2:0,
              y2:0,
              width:0,
              height:0
            }
          
            const splittedUrl = url.split('/');
            const TOTAL_ATTRIBUTES = 4;
            let noOfAttributesFound = 0;
            for(let i=0;i<splittedUrl.length;i++){
                if(noOfAttributesFound === TOTAL_ATTRIBUTES) break;
                const s = splittedUrl[i];
                if(s.includes('x_') && s.includes('y_') && s.includes('w_') && s.includes('h_')){
                    const attributes = s.split(',');
                    attributes.forEach((attr) => {
                        const value = parseInt(attr.split('_')[1]);
                        const parsedValue = isNaN(value) ? 0 : value;
                        if(attr.includes('x_')){
                            data.x1 = parsedValue;
                            noOfAttributesFound++;
                            return;
                        }
                        if(attr.includes('w_')){
                            data.x2 = parsedValue;
                            noOfAttributesFound++;
                            return;
                        }
                        if(attr.includes('y_')){
                            data.y1 = parsedValue;
                            noOfAttributesFound++;
                            return;
                        }
                        if(attr.includes('h_')){
                            data.y2 = parsedValue;
                            noOfAttributesFound++;
                            return;
                        }
                    });
                    continue;
                }
                if(s.includes('w_') && s.includes('h_')){
                    const attributes = s.split(',');
                    attributes.forEach((attr) => {
                        const value = parseInt(attr.split('_')[1]);
                        const parsedValue = isNaN(value) ? 0 : value;
                        if(attr.includes('w_')){
                            data.width = parsedValue;
                            noOfAttributesFound++;
                            return;
                        }
                        if(attr.includes('h_')){
                            data.height = parsedValue;
                            noOfAttributesFound++;
                            return;
                        }
                    });
                }
            }

            if(!data.x1 && !data.x2 && !data.y1 && !data.y2){
              data.x2 = data.width;
              data.y2 = data.height;
              return data;
            }
            
            if(data.x1 && data.x2){
                data.x2 = data.x1 + data.x2;
            }
            
            if(data.y1 && data.y2){
                data.y2 = data.y1 + data.y2;
            }
            return data;          
          }
          const generateOriginalImageUrl = (url) => {
            if(!url) return;
            const splittedUrl = url.split('/');
            const HEIGHT_WIDTH_PARAM_PATTERN = /^.*w_(\d+),h_(\d+)|h_(\d+),w_(\d+)/;
            const imageWidthAndHeight = splittedUrl.find((param) => param.match(HEIGHT_WIDTH_PARAM_PATTERN));
            if(!imageWidthAndHeight) return url;
            let imagePath = '';
            const shouldNotContain = [',','c_limit', 'q_'];
            for(let i=splittedUrl.length-1; i>=0; i--){
              const currentChunk = splittedUrl[i];
              if(shouldNotContain.some((param) => currentChunk.includes(param))) break;
              imagePath = imagePath === '' ? currentChunk : `${currentChunk}/${imagePath}`;
            }
            return `https://res.cloudinary.com/simplotel/image/upload/${imageWidthAndHeight}/q_30/${imagePath}`;
          }
          const state = {
            cropApi: {},
            imgUrl: "",
            croppedImageUrl: "",
            cropValues: {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 0,
              width: 0,
              height: 0,
            }
          };
          const self = this;
          this.modal.onceOpen(() => {
            const imageUrl = self.model.attributes.value;
          const originalUrl = self.model.attributes.originalImage || generateOriginalImageUrl(imageUrl);
          const urlData = extractUrlData(imageUrl);
          const content = `
          <div class="c-image-container" style="padding-left: 0px;">
          <div id="cropImgContainer" class="crop-pic" style="max-height: 400px; padding: 0px; display:flex; justify-content: center; align-items: center;">
          <img id="widgetCropPic" src="${originalUrl}" style="display: none;"/>
          </div>
          <div style="width: 100%; padding: 10px 0; display:flex; justify-content: flex-end; align-items: center; gap: 15px; margin-top: 10px;">
          <button id="cancel-cropping" class="btn btn-default">Cancel</button>
          <button id="save-cropped-image" class="btn btn-primary">Apply Change</button>
          </div>
          </div>
          `;
          self.modal.setContent(content);
          
          state.cropValues = urlData;
          state.imgUrl = originalUrl;
          const { x1, x2, y1,y2, width, height } = urlData;
            const applyChangeButton = document.querySelector('#save-cropped-image');
            const cancelButton = document.querySelector('#cancel-cropping');
            const modalContainer = document.querySelector('.gjs-mdl-dialog.gjs-one-bg.gjs-two-color');
            if(applyChangeButton){
              applyChangeButton.addEventListener('click', ()=>{
                const data = {
                  x1: state.cropValues.x1,
                  y1: state.cropValues.y1,
                  x2: state.cropValues.x2,
                  y2: state.cropValues.y2,
                  imgUrl: state.imgUrl
                }
                const url = constructNewUrl(config, self.model, data);
                self.spreadUrl(url);
                self.modal.close();
              });
            }
            if(cancelButton){
              cancelButton.addEventListener('click', () => self.modal.close());
            }
            if(modalContainer){
              modalContainer.style.width = '630px';
            }
            
            let jcropOpts = {
              onSelect: function (c) {
                  state.cropValues.x1 = c.x;
                  state.cropValues.y1 = c.y;
                  state.cropValues.x2 = c.x2;
                  state.cropValues.y2 = c.y2;
              },
              trueSize: [width, height],
              bgColor: 'black',
              setSelect: [x1, y1, x2, y2],
              boxWidth: 600,
              boxHeight: 370,
            };
            config.initJcropElement($('#widgetCropPic'), jcropOpts, (objectValue) => {
              state.cropApi = objectValue;
            });
          });
          this.modal.open({ title: 'Crop Background', attributes: { class:'my-custom-class', id:'my-custom-id' } });
        },
        storeOriginalImageUrl(url){
          this.model.set('originalImage', url);
        },
        setPreviewView(v) {
          const pv = this.$previewBox;
          pv && pv[v ? 'addClass' : 'removeClass'](`${this.pfx}show`);
          this.$el.find(`#${this.pfx}crop`).css({display: v ? 'block' : 'none'});
        },
        openAssetManager(e) {
          const { em, modal } = this;
          const editor = em ? em.get('Editor'): '';
          if (editor) {
            const callback = {
              onSelect: (asset, args) => {
                const imageUrl = args['data-orgimgsrc'];
                this.spreadUrl(imageUrl);
                this.storeOriginalImageUrl(imageUrl);
              }
            };
            config.showAssetsMethod({...this, exactDimensions: true}, callback.onSelect);
          }
        }
      })
    });
  }
  if (!config.showOlderPhoenix) {
    sm.getSectors().reset(csm && csm.length ? csm: [{
      name: config.textDecorations,
      open: false,
      buildProps: ['opacity', 'background-color', 'border-radius', 'border', 'box-shadow', 'background'],
      properties: [{
        type: 'slider',
        property: 'opacity',
        defaults: 1,
        step: 0.01,
        max: 1,
        min:0,
      },{
        property: 'border-radius',
        properties: [
          { name: 'Top', property: 'border-top-left-radius'},
          { name: 'Right', property: 'border-top-right-radius'},
          { name: 'Bottom', property: 'border-bottom-left-radius'},
          { name: 'Left', property: 'border-bottom-right-radius'}
        ],
      },{
        property: 'box-shadow',
        properties: [
          { name: 'X position', property: 'box-shadow-h'},
          { name: 'Y position', property: 'box-shadow-v'},
          { name: 'Blur', property: 'box-shadow-blur'},
          { name: 'Spread', property: 'box-shadow-spread'},
          { name: 'Color', property: 'box-shadow-color'},
          { name: 'Shadow type', property: 'box-shadow-type'}
        ],
      },{
        property: 'background',
        properties: [
          { name: 'Image', property: 'background-image'},
          { name: 'Repeat', property:   'background-repeat'},
          { name: 'Position', property: 'background-position'},
          { name: 'Attachment', property: 'background-attachment'},
          { name: 'Size', property: 'background-size'}
        ],
      }]
    }]); 
  }
  else {
  sm.getSectors().reset(csm && csm.length ? csm: [{
    name: config.textGeneral,
    open: false,
    buildProps: ['float', 'display', 'position', 'top', 'right', 'left', 'bottom'],
  },{
    name: config.textLayout,
    open: true,
    buildProps: ['width', 'height', 'max-width', 'min-height', 'margin', 'padding'],
    properties: [{
      property: 'margin',
      properties:[
        { name: 'Top', property: 'margin-top'},
        { name: 'Right', property: 'margin-right'},
        { name: 'Bottom', property: 'margin-bottom'},
        { name: 'Left', property: 'margin-left'}
      ],
    },{
      property: 'padding',
      properties:[
        { name: 'Top', property: 'padding-top'},
        { name: 'Right', property: 'padding-right'},
        { name: 'Bottom', property: 'padding-bottom'},
        { name: 'Left', property: 'padding-left'}
      ],
    }]
  },{
    name: config.textTypography,
    open: false,
    buildProps: ['font-family', 'font-size', 'font-weight', 'letter-spacing', 'color', 'line-height', 'text-align', 'text-decoration', 'text-shadow'],
    properties:[
      { name: 'Font', property: 'font-family'},
      { name: 'Weight', property: 'font-weight'},
      { name:  'Font color', property: 'color'},
      {
        property: 'text-align',
        type: 'radio',
        defaults: 'left',
        list: [
          { value: 'left',  name: 'Left',    className: 'fa fa-align-left'},
          { value: 'center',  name: 'Center',  className: 'fa fa-align-center' },
          { value: 'right',   name: 'Right',   className: 'fa fa-align-right'},
          { value: 'justify', name: 'Justify',   className: 'fa fa-align-justify'}
        ],
      },{
        property: 'text-decoration',
        type: 'radio',
        defaults: 'none',
        list: [
          { value: 'none', name: 'None', className: 'fa fa-times'},
          { value: 'underline', name: 'underline', className: 'fa fa-underline' },
          { value: 'line-through', name: 'Line-through', className: 'fa fa-strikethrough'}
        ],
      },{
        property: 'text-shadow',
        properties: [
          { name: 'X position', property: 'text-shadow-h'},
          { name: 'Y position', property: 'text-shadow-v'},
          { name: 'Blur', property: 'text-shadow-blur'},
          { name: 'Color', property: 'text-shadow-color'}
        ],
    }]
  },{
    name: config.textDecorations,
    open: false,
    buildProps: ['opacity', 'background-color', 'border-radius', 'border', 'box-shadow', 'background'],
    properties: [{
      type: 'slider',
      property: 'opacity',
      defaults: 1,
      step: 0.01,
      max: 1,
      min:0,
    },{
      property: 'border-radius',
      properties: [
        { name: 'Top', property: 'border-top-left-radius'},
        { name: 'Right', property: 'border-top-right-radius'},
        { name: 'Bottom', property: 'border-bottom-left-radius'},
        { name: 'Left', property: 'border-bottom-right-radius'}
      ],
    },{
      property: 'box-shadow',
      properties: [
        { name: 'X position', property: 'box-shadow-h'},
        { name: 'Y position', property: 'box-shadow-v'},
        { name: 'Blur', property: 'box-shadow-blur'},
        { name: 'Spread', property: 'box-shadow-spread'},
        { name: 'Color', property: 'box-shadow-color'},
        { name: 'Shadow type', property: 'box-shadow-type'}
      ],
    },{
      property: 'background',
      properties: [
        { name: 'Image', property: 'background-image'},
        { name: 'Repeat', property:   'background-repeat'},
        { name: 'Position', property: 'background-position'},
        { name: 'Attachment', property: 'background-attachment'},
        { name: 'Size', property: 'background-size'}
      ],
    }]
  },{
    name: config.textExtra,
    open: false,
    buildProps: ['transition', 'perspective', 'transform', 'filter'],
    properties: [{
      property: 'transition',
      properties:[
        { name: 'Property', property: 'transition-property'},
        { name: 'Duration', property: 'transition-duration'},
        { name: 'Easing', property: 'transition-timing-function'}
      ],
    },{
      property: 'transform',
      properties:[
        { name: 'Rotate X', property: 'transform-rotate-x'},
        { name: 'Rotate Y', property: 'transform-rotate-y'},
        { name: 'Rotate Z', property: 'transform-rotate-z'},
        { name: 'Scale X', property: 'transform-scale-x'},
        { name: 'Scale Y', property: 'transform-scale-y'},
        { name: 'Scale Z', property: 'transform-scale-z'}
      ],
    },{
        name: 'Filter',
        property: 'filter',
        type: 'filter',
        full: 1,
    }]
  },{
    name: 'Flex',
    open: false,
    properties: [{
      name: 'Flex Container',
      property: 'display',
      type: 'select',
      defaults: 'block',
      list: [
        { value: 'block', name: 'Disable'},
        { value: 'flex', name: 'Enable'}
      ],
    },{
      name: 'Flex Parent',
      property: 'label-parent-flex',
      type: 'integer',
      defaults: 0,
    },{
      name: 'Direction',
      property: 'flex-direction',
      type: 'radio',
      defaults: 'row',
      list: [{
        value: 'row',
        name: 'Row',
        className: 'icons-flex icon-dir-row',
        title: 'Row',
      },{
        value: 'row-reverse',
        name: 'Row reverse',
        className: 'icons-flex icon-dir-row-rev',
        title: 'Row reverse',
      },{
        value: 'column',
        name: 'Column',
        title: 'Column',
        className: 'icons-flex icon-dir-col',
      },{
        value: 'column-reverse',
        name: 'Column reverse',
        title: 'Column reverse',
        className: 'icons-flex icon-dir-col-rev',
      }],
    },{
      name: 'Wrap',
      property: 'flex-wrap',
      type: 'radio',
      defaults: 'nowrap',
      list: [
        {value: 'nowrap',  name: 'nowrap'},
        {value: 'wrap',  name: 'wrap'},
        {value: 'wrap-reverse',   name: 'wrap-reverse'}
      ],
    },{
      name: 'Justify',
      property: 'justify-content',
      type: 'radio',
      defaults: 'flex-start',
      list: [{
        value: 'flex-start',
        className: 'icons-flex icon-just-start',
        title: 'Start',
      },{
        value: 'flex-end',
        title: 'End',
        className: 'icons-flex icon-just-end',
      },{
        value: 'space-between',
        title: 'Space between',
        className: 'icons-flex icon-just-sp-bet',
      },{
        value: 'space-around',
        title: 'Space around',
        className: 'icons-flex icon-just-sp-ar',
      },{
        value: 'center',
        title: 'Center',
        className: 'icons-flex icon-just-sp-cent',
      }],
    },{
      name: 'Align',
      property: 'align-items',
      type: 'radio',
      defaults: 'center',
      list: [{
        value: 'flex-start',
        title: 'Start',
        className: 'icons-flex icon-al-start',
      },{
        value: 'flex-end',
        title: 'End',
        className: 'icons-flex icon-al-end',
      },{
        value: 'stretch',
        title: 'Stretch',
        className: 'icons-flex icon-al-str',
      },{
        value: 'center',
        title: 'Center',
        className: 'icons-flex icon-al-center',
      }],
    },{
      name: 'Flex Children',
      property: 'label-parent-flex',
      type: 'integer',
    },{
      name:     'Order',
      property:   'order',
      type:     'integer',
      defaults:  0,
      min: 0
    },{
      name: 'Flex',
      property: 'flex',
      type: 'composite',
      properties: [{
        name: 'Grow',
        property: 'flex-grow',
        type: 'integer',
        defaults: 0,
        min: 0
      },{
        name: 'Shrink',
        property: 'flex-shrink',
        type: 'integer',
        defaults: 0,
        min: 0
      },{
        name: 'Basis',
        property: 'flex-basis',
        type: 'integer',
        units: ['px','%',''],
        unit: '',
        defaults: 'auto',
      }],
    },{
      name: 'Align',
      property: 'align-self',
      type: 'radio',
      defaults: 'auto',
      list: [{
        value: 'auto',
        name: 'Auto',
      },{
        value: 'flex-start',
        title: 'Start',
        className: 'icons-flex icon-al-start',
      },{
        value: 'flex-end',
        title: 'End',
        className: 'icons-flex icon-al-end',
      },{
        value: 'stretch',
        title: 'Stretch',
        className: 'icons-flex icon-al-str',
      },{
        value: 'center',
        title: 'Center',
        className: 'icons-flex icon-al-center',
      }],
    }]
  }]);
}
}
