export function generatePlusMinusCounter(parentClass, dropdownId, label, value, errorMsg) {
    return `
        <div class="${parentClass}" style="margin-bottom: 15px;">
            <label style="margin-right: 15px; width: 150px">${label}</label>
            <span class="minus" id="minus-${dropdownId}">-</span>
            <input class="form-control count" type="number" id="${dropdownId}" value="${value}">
            <span class="plus" id="plus-${dropdownId}">+</span>
            <span class="pm-errorMsg" id="errorMsg-${dropdownId}"></span>
        </div>
    `
}

export function generateCheckboxToggle(parentClass, label, checkboxId) {
    return `
        <div class="${parentClass}" style="display: flex; align-items: center; margin-bottom: 15px">
            <label style="margin-right: 15px; width: 150px" for="${checkboxId}">${label}</label>
            <input id="${checkboxId}" type="checkbox" style="width: 15px; height: 15px; margin-bottom: 5px;"}>
        </div>
    `;
}

export const TEXT_GROUP_BLOCK = `
    <div data-gjs-type="default"draggable="true" data-highlightable="1" data-simp-content-parent="true" class="text-content-wrapper" data-gjs-copyable="false" data-gjs-draggable="false" data-gjs-removable="false" data-gjs-resizable="false" data-gjs-droppable="false">
        <p data-gjs-type="text" draggable="true" data-highlightable="1" data-simp-content="text" class="" data-gjs-copyable="false" data-gjs-draggable="false" data-gjs-removable="false" data-gjs-resizable="false" data-gjs-droppable="false">
            Lorem Ipsum
        </p>
    </div>
`;

export const getTextGroup = (blockCount) => {
  return `
        <div
            data-gjs-type="default"
            draggable="true"
            data-highlightable="1"
            class="col-sm-4 col-md-4 text-block-1 text-content"
            data-gjs-copyable="false"
            data-gjs-draggable="false"
            data-gjs-removable="false"
            data-gjs-resizable="false"
            data-gjs-droppable="false"
        >
            ${TEXT_GROUP_BLOCK.repeat(blockCount)}
        </div>
    `;
};

export const TEXT_GROUP_HEADING = `
    <div data-gjs-type="default" draggable="true" class="col-xs-12 one-col title-block" data-gjs-copyable="false" data-gjs-draggable="false" data-gjs-removable="false" data-gjs-resizable="false" data-gjs-droppable="false">
        <div data-gjs-type="default" draggable="true" data-simp-heading="true" data-simp-content-parent="true" class="text-content-wrapper" data-gjs-copyable="false" data-gjs-draggable="false" data-gjs-removable="false" data-gjs-resizable="false" data-gjs-droppable="false">
            <p data-gjs-type="text" draggable="true" data-simp-content="text" data-gjs-copyable="false" data-gjs-draggable="false" data-gjs-removable="false" data-gjs-resizable="false" data-gjs-droppable="false">
                Lorem Ipsum
            </p>
        </div>
    </div>
`;