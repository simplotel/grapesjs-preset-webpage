export function getHtmlElement(name, props, attrs) {
    var innerItem = document.createElement(name);
    for (var key in props) {
        innerItem[key] = props[key];
    }
    for (var key in attrs) {
        innerItem.setAttribute(key, attrs[key]);
    }
    return innerItem;
}
