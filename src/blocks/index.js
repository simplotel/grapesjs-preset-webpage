export default (editor, config) => {
  const bm = editor.BlockManager;
  // const toAdd = name => config.blocks.indexOf(name) >= 0;

  // simplotel CMS customizations
  if(config.showAssetsMethod) {
    function getSIUPopupOnSelectCallback(model) {
      return function(asset, args) {
        args['src'] = asset;
        var attrs = Object.assign(model.getAttributes(), args);
        model.setAttributes(attrs);
        model.set('src', asset);
      }
    }

    editor.on('block:drag:stop', model => {
      if(model && model.attributes) {
        if(model.attributes.type == 'image') {
          config.showAssetsMethod(model, getSIUPopupOnSelectCallback(model));
        } 
        editor.select(model);
      }
    });

    editor.on("component:selected", (comp) => {
      if (comp && comp.attributes) {
        var toolbar = comp.get("toolbar");

        const btnType = comp.attributes.type;
        toolbar.forEach((item) => {
          mapTitleToIcons(item);
        });
      }
    });
  }
}
