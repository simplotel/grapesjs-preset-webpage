export const
  cmdImport = 'gjs-open-import-webpage',
  cmdDeviceDesktop = 'set-device-desktop',
  cmdDeviceTablet = 'set-device-tablet',
  cmdDeviceMobile = 'set-device-mobile',
  cmdDeviceMobileLandscape = 'set-device-mobile-landscape',
  cmdClear = 'canvas-clear',
  cmdEditSortDelete = "edit-sort-delete",
  cmdCloneWidget = "clone-widget",
  cmdOpenTextGroupConfig = "open-text-group-config",
  allSliders = [
      "widget-image-slider",
      "widget-heroimage-slider",
      "widget-variable-imagetext-slider",
      "widget-img-txt-link",
      "widget-imagetext-slider"
  ],
  defaultAspectRatios = [{
	  	name: "16-9",
		value: 1.777,
	  },{
	  	name: "3-2",
		value: 1.5,
	  },{
	    name: "4-3",
	    value: 1.333,
	  },{
	    name: "2-1",
	    value: 2,
	  },{
	    name: "1-2",
	    value: 0.5,
	  },{
	    name: "1-1.25",
	    value: 0.80,
	  },{
	    name: "1-1.5",
	    value: 0.667,
	  },{
	    name: "1-1",
	    value: 1,
	  },{
	    name: "Manual",
	    value: 0,
	  }],
  PAGE_PARENT = {
		'HOME': 1,
		'ROOMS': 2,
		'LOCATIONS': 3,
		'GALLERY': 4,
		'PROMOTIONS': 5,
		'CUSTOM_PAGE': 6
	};
