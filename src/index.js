import grapesjs from 'grapesjs';
import pluginNavbar from 'grapesjs-navbar';
import pluginCountdown from 'grapesjs-component-countdown';
import pluginForms from 'grapesjs-plugin-forms';
import pluginExport from 'grapesjs-plugin-export';
import pluginTooltipComponent from 'grapesjs-tooltip';
import pluginTypedComponent from 'grapesjs-typed';
import pluginFilterTrait from 'grapesjs-style-filter';
import cmdItems from './commands';
import blocks from './blocks';
import components from './components';
import panels, {validateOnSave} from './panels';
import styles from './styles';

export default grapesjs.plugins.add('gjs-preset-webpage', (editor, opts = {}) => {
  let config = opts;

  let defaults = {
    // Which blocks to add
    blocks: ['link-block', 'quote', 'text-basic'],

    // Modal import title
    modalImportTitle: 'Import',

    // Modal import button text
    modalImportButton: 'Import',

    // Import description inside import modal
    modalImportLabel: '',

    // Default content to setup on import model open.
    // Could also be a function with a dynamic content return (must be a string)
    // eg. modalImportContent: editor => editor.getHtml(),
    modalImportContent: '',

    // Code viewer (eg. CodeMirror) options
    importViewerOptions: {},

    // Confirm text before cleaning the canvas
    textCleanCanvas: 'Are you sure to clean the canvas?',

    // Show the Style Manager on component change
    showStylesOnChange: 1,

    // Text for General sector in Style Manager
    textGeneral: 'General',

    // Text for Layout sector in Style Manager
    textLayout: 'Layout',

    // Text for Typography sector in Style Manager
    textTypography: 'Typography',

    // Text for Decorations sector in Style Manager
    textDecorations: 'Decorations',

    // Text for Extra sector in Style Manager
    textExtra: 'Extra',

    // Use custom set of sectors for the Style Manager
    customStyleManager: [],

    // `grapesjs-navbar` plugin options
    // By setting this option to `false` will avoid loading the plugin
    navbarOpts: {},

    // `grapesjs-component-countdown` plugin options
    // By setting this option to `false` will avoid loading the plugin
    countdownOpts: {
      // Countdown class prefix
      countdownClsPfx: 'countdown',

      // Countdown label
      labelCountdown: 'Countdown',

      // Countdown category label
      labelCountdownCategory: 'Extra',

      // Days label text used in component
      labelDays: 'days',

      // Hours label text used in component
      labelHours: 'hours',

      // Minutes label text used in component
      labelMinutes: 'minutes',

      // Seconds label text used in component
      labelSeconds: 'seconds'
    },

    // `grapesjs-plugin-forms` plugin options
    // By setting this option to `false` will avoid loading the plugin
    formsOpts: {},

    // `grapesjs-plugin-export` plugin options
    // By setting this option to `false` will avoid loading the plugin
    exportOpts: {},

    // `grapesjs-tooltip` plugin options
    tooltipCompOpts: {},

    // `grapesjs-typed` plugin options
    typedCompOpts: {},

    // `grapesjs-style-filter` plugin options
    filterTraitOpts: {},

    // SIU image select/crop popup
    faIconType: 'fa',
    showAssetsMethod: null,
    initJcropElement: null,
    resizeImageParams: null,
    imageEditCommandId: 'siu-image-editor'
  };

  // Load defaults
  for (let name in defaults) {
    if (!(name in config))
      config[name] = defaults[name];
  }

  const {
    navbarOpts,
    countdownOpts,
    formsOpts,
    exportOpts,
    tooltipCompOpts,
    typedCompOpts,
    filterTraitOpts
  } = config;

  // Load plugins
  navbarOpts && pluginNavbar(editor, navbarOpts);
  countdownOpts && pluginCountdown(editor, countdownOpts);
  formsOpts && pluginForms(editor, formsOpts);
  exportOpts && pluginExport(editor, exportOpts);
  tooltipCompOpts && pluginTooltipComponent(editor, tooltipCompOpts);
  typedCompOpts && pluginTypedComponent(editor, typedCompOpts);
  filterTraitOpts && pluginFilterTrait(editor, filterTraitOpts);

  // Load components
  components(editor, config);

  // Load blocks
  blocks(editor, config);

  // Load commands
  cmdItems.commands(editor, config);

  // Load panels
  panels(editor, config);

  // Load styles
  styles(editor, config);

  editor.on('component:mount', function(model) {
    model.attributes.classes.models.map(item => {
      let newItem = {...item};
      newItem.changed = {active: false};
      newItem.attributes.active = false;
      return newItem;
    })
    if(!config.showOlderPhoenix && model.attributes.type == 'widget-one-image') {
      const imageContentWrapper = model.find('.image-content-wrapper');
      if(imageContentWrapper && imageContentWrapper.length){
        const imageWrapper = imageContentWrapper[0];
        imageWrapper.removeClass('col-xs-12');
        imageWrapper.removeClass('one-col');
        imageWrapper.addClass('col-sm-12');
        imageWrapper.addClass('col-md-4');
        imageWrapper.addClass('center-block');
      }
    }

    // replace grid breakpoints for:
    // 1. two image
    // 2. two top image - bottom text 
    if(model.attributes.type == 'widget-two-image' || model.attributes.type == 'widget-two-timage-btext') {
      const contentWrapper = model.find('.text-content');

      if(contentWrapper && contentWrapper.length){
        contentWrapper.forEach(wrapper => {
          wrapper.removeClass('col-sm-12');
          wrapper.removeClass('col-md-6');
          wrapper.addClass('col-xs-12');
          wrapper.addClass('col-sm-6');
        });
      }
    }

    // replace grid breakpoints for:
    // 1. three image
    // 2. three top image - bottom text
    if(model.attributes.type == 'widget-three-image' || model.attributes.type == 'widget-three-timage-btext') {
      const contentWrapper = model.find('.text-content');

      if(contentWrapper && contentWrapper.length){
        contentWrapper.forEach(wrapper => {
          wrapper.removeClass('col-sm-12');
          wrapper.removeClass('col-md-4');
          wrapper.addClass('col-xs-12');
          wrapper.addClass('col-sm-4');
        });
      }
    }

    // replace grid breakpoints for:
    // 1. summary widget
    if(model.attributes.type == 'summary-widget') {
      const contentWrapper = model.find('.summary-widget');

      if(contentWrapper && contentWrapper.length){
        contentWrapper.forEach(wrapper => {
          wrapper.removeClass('col-md-4');
          wrapper.addClass('col-sm-4');
        });
      }
    }
  })

  editor.on('load', function() {
    // for State dropdown in SelectorManager
    if(!config.showOlderPhoenix) {
      document.querySelector('#gjs-clm-status-c').style.display = 'none';
    }
  })

  window.addEventListener('beforeunload', function (e) {
    // If page is inside iframe refrain showing unload popup
    if(window.self !== window.top) return;

    if(validateOnSave(editor)) {
      e.preventDefault();
      e.returnValue = true;
    }
  });

  editor.on("component:mount", function(model) {
    if(model.getTrait("id")) {
      model.removeTrait("id");
    }
    if(model.getTrait("title")) {
      model.removeTrait("title");
    }
  });

  // On load, for each image, construct new URL and resized params with current theme settings, set mobile URL on image
  editor.on("load", function() {
    // Skip, if editor inside iframe
    if (window.parent !== window) {
      return;
    }
    
    editor.getWrapper().find('img')
    .filter(image => image.closest('*[data-simp-type]'))
    .forEach(image => {
      const attrs = image.getAttributes();

      // calculate resized image X,Y values
      let imgWidth = parseInt(attrs['data-imgwidth']),
          imgHeight = parseInt(attrs['data-imgheight']),
          imgAspect = +attrs['data-imgaspect'],
          imgX = 0,
          imgY = 0,
          imgX2 = imgWidth,
          imgY2 = imgHeight,
          diff = 0;
      let width = imgWidth;
      let height = imgWidth / imgAspect;

      // Skip, if width or height is 0
      // (applies for the skeleton image)
      if (width === 0 || height === 0) {
        return;
      }
      // Obtaining the crop params from the existing mobile src of the image.
      const mobImgSrc = attrs['data-mobimgsrc'];
      let cropParamsArr = mobImgSrc.match(/x_(\d+),y_(\d+),w_(\d+),h_(\d+)/) || []; // Essentially gets us the pattern string which will be like "x_num,y_num,w_num,h_num", and individual crop parameter numbers as strings in the pattern.
      
      cropParamsArr.shift()// Removing the unnecessary 1st item, which will be a string with all the crop parameters. We will only use the individual crop parameter items.
      cropParamsArr = cropParamsArr.map(numAsStr => parseInt(numAsStr));
      const [x1, y1, croppedWidth, croppedHeight] = cropParamsArr;
      
      if (
        typeof x1 === "number" &&
        typeof y1 === "number" &&
        typeof croppedWidth === "number" &&
        typeof croppedHeight === "number"
      ) {
        imgX = x1;
        imgY = y1;
        imgX2 = x1 + croppedWidth;
        imgY2 = y1 + croppedHeight;
      } else if (height <= imgHeight) {
        diff = (imgHeight - height) / 2;
        imgX = 0;
        imgY = Math.round(diff);
        imgX2 = imgWidth;
        imgY2 = Math.round(height + diff);
      } else {
        width = imgHeight * imgAspect;
        diff = (imgWidth - width) / 2;
        imgX = Math.round(diff);
        imgY = 0;
        imgX2 = Math.round(width + diff);
        imgY2 = imgHeight;
      }
      
      const data = {
        width: croppedWidth ? croppedWidth : width,
        height: croppedHeight ? croppedHeight :height,
        x1: imgX,
        x2: imgX2,
        y1: imgY,
        y2: imgY2,
        ratio: attrs['data-imgaspect'],
        orgUrl: attrs['data-orgimgsrc'],
      };

      const thisObject = {
        PRECISION_VALUE: 3,
        state: {}
      }
      
      // construct resized URL params
      const constructNewUrl = cmdItems.constructNewUrl.bind(thisObject);
      const obj = constructNewUrl(config, image, data, true);
      
      // set image mobile src
      image.setAttributes({
        ...attrs,
        'data-mobimgsrc': obj.mobile,
      });
    });
      // for default canvas data storing in session before save
      sessionStorage.setItem('defaultHTML', JSON.stringify(editor.getHtml()));
      sessionStorage.setItem('defaultCSS', JSON.stringify(editor.getCss()));
      sessionStorage.setItem('defaultJS', JSON.stringify(editor.getJs()));
  });
});
